package model;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 * Class for defining tournament discounts.
 */
public class Discount {

	private final transient String name;
	private final transient BigDecimal amount;
	private final transient boolean dominant;

	private final transient ID DISCOUNT_ID;

	/**
	 * Constructor.
	 * 
	 * @param discount_id
	 *            discount id
	 * @param discountname
	 *            discount name
	 * @param discountamount
	 *            amount of money the discount reducesF
	 * @param dominantdiscount
	 *            dominant discount flag
	 */
	protected Discount(final ID discount_id, final String discountname,
			final BigDecimal discountamount, final boolean dominantdiscount) {
		this.DISCOUNT_ID = discount_id;
		this.name = discountname;
		this.amount = discountamount;
		this.dominant = dominantdiscount;
	}

	/**
	 * Returns discount id.
	 * 
	 * @return discount id
	 */
	public ID getId() {
		return this.DISCOUNT_ID;
	}

	/**
	 * Returns discount name.
	 * 
	 * @return discount name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns discount amount.
	 * 
	 * @return discount amount
	 */
	public BigDecimal getAmount() {
		return this.amount;
	}

	/**
	 * Checks whether discount is dominant.
	 * @return true if discount is dominant, otherwise false
	 */
	public boolean isDominant() {
		return this.dominant;
	}

	/**
	 * Checks whether discount equals to given discount.
	 * 
	 * @param other discount instance to compare to
	 */
	@Override
	public boolean equals(final Object other) {
		// ID object compared with itself
		if (this == other) {
			return true;
		}

		// Discount compared with non-Discont-object
		if (!(other instanceof Discount)) {
			return false;
		}
		// Discount compared with other Discount
		if (other instanceof Discount
				&& this.name.equals(((Discount) other).getName())
				&& this.amount.compareTo(((Discount) other).getAmount()) == 0
				&& this.dominant == ((Discount) other).isDominant()) {
			return true;
		}

		return false;
	}

	/**
	 * Returns a key for a hash map.
	 */
	@Override
	public int hashCode() {
		int hash = 1;
		hash = hash * 31 + this.name.hashCode();
		hash = hash * 31 * this.amount.hashCode();
		return hash;
	}

	/**
	 * Returns discount contents in a formatted string.
	 */
	@Override
	public String toString() {
		String discount_str = "ID:" + this.DISCOUNT_ID + '|' + this.name + '|'
				+ this.amount + '|';
		if (this.dominant) {
			discount_str += "dominant";
		} else {
			discount_str += "non-dominant";
		}

		return discount_str;
	}

	public static class DiscountComparator implements Comparator<Discount> {
		//sort by id
		public int compare(final Discount first_id, final Discount second_id) {
			return first_id.getId().compareTo(second_id.getId());
		}
		
	}

}
