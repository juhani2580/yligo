package model;

import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Tournament.EntryComparator.SortBy;
import model.exceptions.DuplicatePlayerException;
import model.exceptions.EntryNotFoundException;
import model.exceptions.GoException;
import model.exceptions.GoIOException;
import model.exceptions.PreEntryNotFoundException;
import model.exceptions.TournamentClosedException;
import model.exceptions.TournamentOpenException;
import model.tournament.TournamentDataModel;

import settings.Settings;

/**
 * Class Tournament is used to store all the information and entries
 * of a particular tournament.
 * 
 * The data structure of the tournament is based on the class 
 * model.tournament.TournamentDataModel.
 * 
 * Editing and creating a completely new tournament is handled via
 * the TournamentUpdater and TournamentInitializer objects.
 * 
 * A Tournament object also controls the reading and writing
 * of the XML data-, log- and export files of the tournament through
 * classes that extend the File class. 
 *
 */
public class Tournament extends TournamentDataModel {
	
	public static final String[] TOURNAMENT_HEADER_FIELDS = {
		"id",					"name",				"html",
		"defaultCountry",		"baseFee",			"date",
		"preregistrationOpen",	"tournamentOpen", 	"entryIdCounter",	
		"discountIdCounter", 	"discounts"};

	private transient HashMap<ID, Entry> entries = new HashMap<ID, Entry>();
	private transient HashMap<ID, Entry> pre_entries = new HashMap<ID, Entry>();
	private transient HashMap<ID, Entry> confirmed_pre_entries = new HashMap<ID, Entry>();

	private transient XMLFile xml_file;
	private transient ExportFile registration_export_file;
	private transient ExportFile pre_registration_export_file;
	private transient LogFile log_file;
		
	private transient ID entry_id_counter;
	private transient ID discount_id_counter;

	/**
	 * Constructor creates a Tournament object from an existing XML data file
	 * 
	 * @param existing_xml_file
	 * @throws GoException thrown if the XML data file cannot be read or parsed.
	 */
	protected Tournament(final XMLFile existing_xml_file) throws GoException { //DOMException on unchecked, älä lisää
		this(existing_xml_file.getTournamentId());
		//save XML data file for the tournament
		this.xml_file = existing_xml_file;
		//get tournament header data from XML document
		final TournamentInitializer initializer = this.xml_file.getTournamentInitializer();
		//set tournament header data
		this.setHeaderData(initializer);
		
		//update counters
		this.entry_id_counter = this.xml_file.getEntryIdCounter();
		this.discount_id_counter = this.xml_file.getDiscountIdCounter();
		//get entries from XML data file
		this.entries = createEntryMap(this.xml_file.getEntries(this));
		this.pre_entries = createEntryMap(this.xml_file.getPreEntries(this));
		this.confirmed_pre_entries = createEntryMap(this.xml_file.getConfirmedPreEntries(this));
		
	}

	/**
	 * Constructor creates a new Tournament object from a TournamentInitializer object.
	 * The tournament information is read from the initializer.
	 * 
	 * (the tournament created by this constructor has no entries)
	 * 
	 * @param initializer contains tournament information
	 * @throws GoException throw if the initializer cannot be read
	 */
	protected Tournament(final TournamentInitializer initializer) throws GoException {
		this(initializer.getId());	
		//set counters
		this.entry_id_counter = new ID(0);
		this.discount_id_counter = new ID(initializer.getDiscounts().length);
		//create an XML data file for the tournament with initializer data
		final String xml_file_name = Settings.TOURNAMENT_FILENAME_PREFIX+this.getId()+Settings.DATA_FILE_SUFFIX;
		this.xml_file = new XMLFile(Settings.getDataFolder(), xml_file_name, initializer);
		//set tournament data:
		this.setHeaderData(initializer);

		this.writeLogMessage("Tournament created "+this);
	}

	/**
	 * Private constructor, common for the two public constructors.
	 * 
	 * Creates the IO files used by the Tournament object.
	 * 
	 * @param tournament_id the ID of the tournament to be created
	 */
	private Tournament(final ID tournament_id) {
		super(tournament_id);		
		this.log_file = new LogFile(Settings.TOURNAMENT_FILENAME_PREFIX+this.getId()+Settings.LOG_FILE_SUFFIX);
		this.registration_export_file = new ExportFile(Settings.TOURNAMENT_FILENAME_PREFIX+this.getId()+Settings.REGISTRATION_EXPORT_FILE_SUFFIX);
		this.pre_registration_export_file = new ExportFile(Settings.TOURNAMENT_FILENAME_PREFIX+this.getId()+Settings.PRE_REGISTRATION_EXPORT_FILE_SUFFIX);
	}	

	/**
	 * Method is used by the constructor that creates a completely 
	 * new Tournament object to set the tournament information
	 * 
	 * @param initializer contains the tournament information
	 * @throws GoException thrown if the initializer cannot be read
	 */
	private void setHeaderData(final TournamentInitializer initializer) throws GoException {
		super.setName(initializer.getName());
		super.setHtml(initializer.getHtml());
		super.setDefaultCountry(initializer.getDefaultCountry());
		super.setDate(initializer.getDate());
		super.setBaseFee(initializer.getBaseFee());
		super.setDiscounts(initializer.getDiscounts());
		super.setPreregistrationOpen(initializer.isPreRegistrationOpen());
		super.setTournamentOpen(initializer.isTournamentOpen());
	}
		
	
	/**
	 * @return the combined sum of the total fees of all the registered entries in the tournament
	 */
	public BigDecimal getTotalFees() {
		BigDecimal total_fees = BigDecimal.ZERO;
		for (Entry e : this.getEntries())
			total_fees = total_fees.add(e.getTotalFee());
		return total_fees;
	}

	/** 
	 * @return the combined sum of the money paid of by the registered entries(i.e. players) in the tournament
	 */
	public BigDecimal getTotalPaid() {
		BigDecimal total_paid = BigDecimal.ZERO;
		for (Entry e : this.getEntries())
			total_paid = total_paid.add(e.getPaidSum());
		return total_paid;
	}

	/**
	 * Returns the no. of times a particular discount has been used
	 * in the registered entries of the tournament.
	 * 
	 * @param discount_id id of the discount
	 * @return no. of times the discount has been used
	 */
	public int getNumberOfDiscounts(final ID discount_id) {
		int amount = 0;
		for (Entry e : this.entries.values())
			if (e.hasDiscountId(discount_id)) {
				++amount;
			}
		return amount;
	}

	/**
	 * @return no. of registered entries in the tournament
	 */
	public int getNumberOfEntries() {
		return this.entries.size();
	}

	/**
	 * @return no. of pre-entries in the tournament
	 */
	public int getNumberOfPreEntries() {
		return this.pre_entries.size();
	}

	/**
	 * @return the name of the tournament log file
	 */
	public String getLogFileName() {
		return this.log_file.getName();
	}

	/**
	 * Moves the given pre-entry to the confirmed pre-entries list.
	 * 
	 * (does not create a new registered entry, this must be done separately!)
	 * 
	 * @param entry_id id of the pre-entry to confirm
	 * @throws PreEntryNotFoundException thrown if the pre-entry is not found
	 * @throws GoIOException thrown if the modification of the XML data file fails
	 */
	protected final void confirmPreEntry(final ID entry_id) 
	throws PreEntryNotFoundException, GoIOException {
		if(!this.pre_entries.containsKey(entry_id)) {
			throw new PreEntryNotFoundException(entry_id);
		}

		final Entry confirmed_pre_entry = this.pre_entries.remove(entry_id);
		this.confirmed_pre_entries.put(entry_id, confirmed_pre_entry);
		this.xml_file.confirmPreEntry(entry_id);
		
		this.writeLogMessage("Pre-entry confirmed (" + confirmed_pre_entry + ')');
	}

	/**
	 * Edits the general information and discounts of the tournament.
	 * 
	 * @param updater a TournamentUpdater object that contains the new tournament information
	 * @throws GoException thrown if the updater cannot be read or the XML file modification fails
	 */
	protected void editTournament(final TournamentUpdater updater) throws GoException {
		super.setName(updater.getName());
		super.setHtml(updater.getHtml());
		super.setDefaultCountry(updater.getDefaultCountry());
		super.setDate(updater.getDate());
		super.setBaseFee(updater.getBaseFee());
		super.setDiscounts(updater.getDiscounts());
		super.setPreregistrationOpen(updater.isPreRegistrationOpen());
		this.xml_file.updateHeader(updater);
		this.writeLogMessage("Tournament information edited ("+this+')');
	}

	/**
	 * Writes a file that can be read by the Go player pairing program McMahon.
	 * 
	 * @param from_pre_entries if true the export file is created from the pre-entries, else from the registered entries
	 * @return the name of the export file
	 * @throws GoException
	 */
	public final String writeExportFile(final boolean from_pre_entries) throws GoException {
		if (from_pre_entries) {
			this.pre_registration_export_file.write(this.getPreEntries());
			return this.pre_registration_export_file.getName();
		}
		this.registration_export_file.write(this.getEntries());
		return this.registration_export_file.getName();				
	}

	public final String getDataFilename() {
		return this.xml_file.getName();
	}


	/**
	 * Returns a given entry.
	 * 
	 * @param entry_id id of the entry to return
	 * @return the specified Entry object
	 * @throws EntryNotFoundException thrown if the entry cannot be found
	 */
	public Entry getEntry(final ID entry_id) 
	throws EntryNotFoundException {
			if(!this.entries.containsKey(entry_id)) {
				throw new EntryNotFoundException(entry_id);
			}
		return this.entries.get(entry_id);
	}

	/**
	 * Returns a given pre-entry.
	 * 
	 * @param entry_id id of the entry to return
	 * @return the specified Entry object
	 * @throws PreEntryNotFoundException thrown if the entry cannot be found
	 */
	public Entry getPreEntry(final ID entry_id) throws PreEntryNotFoundException {
		if(!this.pre_entries.containsKey(entry_id)) {
			throw new PreEntryNotFoundException(entry_id);
		}
		return this.pre_entries.get(entry_id);
	}

	/**
	 * Deletes a given entry.
	 * 
	 * (deleting a confirmed pre-entry is not allowed in order to 
	 * keep the historical data intact)
	 * 
	 * @param entry_id id of the entry to be deleted
	 * @throws EntryNotFoundException throw if the entry cannot be found
	 * @throws GoIOException thrown if the modification of the XML data file fails.
	 */
	protected void deleteEntry(final ID entry_id) throws EntryNotFoundException, GoIOException {
		if (this.entries.containsKey(entry_id)) {
			final Entry removed = this.entries.remove(entry_id);
			this.writeLogMessage("Entry deleted (" + removed + ')');
		} else if (this.pre_entries.containsKey(entry_id)) {
			final Entry removed = this.pre_entries.remove(entry_id);
			this.writeLogMessage("Pre-entry deleted (" + removed + ')');
		} else {
			throw new EntryNotFoundException(entry_id);
		}
		this.xml_file.deleteEntry(entry_id);
	}

	/**
	 * Used to get the next available entry ID for the tournament and update the counter by one.
	 * 
	 * @return next entry ID
	 * @throws GoIOException thrown if the XML data file modification fails
	 */
	protected ID getNextEntryId() throws GoIOException {
		return  this.getNextEntryId(1);
	}

	/**
	 * Used to get the next available entry ID for the tournament and update the counter.
	 * 
	 * The entry id counter can be incremented by more than one if need.
	 * This is used when importing multiple entries at the same time
	 * so that the XML file update operation does not have to be made multiple times.
	 * 
	 * @param increment the increment to be made
	 * @return next entry ID
	 * @throws GoIOException thrown if the XML data file modification fails
	 */
	protected ID getNextEntryId(final int increment) throws GoIOException {
		final ID next_entry_id = this.entry_id_counter;
		this.entry_id_counter = this.xml_file.incrementEntryIdCounter(increment);
		return next_entry_id;
	}

	/**
	 * Used to get the next available discount ID for the tournament and update the counter by one.
	 * 
	 * @return next discount ID
	 * @throws GoIOException thrown if the XML data file modification fails
	 */
	protected ID getNextDiscountId() throws GoIOException {
		final ID next_discount_id = this.discount_id_counter;
		this.discount_id_counter = this.xml_file.incrementDiscountIdCounter(1);
		return next_discount_id;
	}

	/**
	 * Edits the information of the entry.
	 * 
	 * 
	 * @param entry_id id of the entry to be edited
	 * @param updater EntryUpdater object containing the new information
	 * @throws GoException thrown if the updater cannot be read or the XML data file modification fails
	 */
	protected void editEntry(final ID entry_id, final EntryUpdater updater) 
	throws GoException {
		Entry entry;
		if (this.entries.containsKey(entry_id)) {
			entry = this.entries.get(entry_id);
		} else if (this.pre_entries.containsKey(entry_id)) {
			entry = this.pre_entries.get(entry_id);
		} else {
			throw new EntryNotFoundException(entry_id);
		}
		entry.editEntry(updater);
		this.xml_file.updateEntry(entry_id, updater);
		this.writeLogMessage("Entry edited (" + entry + ')');
	}
	
	/**
	 * Returns an EntryInitializer object that can be used to create
	 * a new entry for this tournament.
	 * 
	 * @return EntryInitializer object
	 * @throws GoException thrown if the necessary XML data file modification fails
	 */
	protected EntryInitializer getEntryInitializer() throws GoException {
		return new EntryInitializer(this, this.getNextEntryId(), false);
	}
	
	/**
	 * Returns an EntryInitializer object that can be used to create
	 * a new pre-entry for this tournament.
	 * 
	 * @return EntryInitializer object
	 * @throws GoException thrown if the necessary XML data file modification fails
	 */
	protected EntryInitializer getPreEntryInitializer() throws GoException {
		return new EntryInitializer(this, this.getNextEntryId(), true);
	}

	/**
	 * Returns an array EntryInitializer objects that can be used to create
	 * new pre-entries for this tournament.
	 * 
	 * Used when importing multiple pre-entries at the same time.
	 * 
	 * @param amount no of initializers
	 * @return EntryInitializer object
	 * @throws GoException thrown if the necessary XML data file modification fails
	 */
	protected EntryInitializer[] getPreEntryInitializer(final int amount) throws GoException {
		EntryInitializer[] initializers = new EntryInitializer[amount];
		ID array_id_counter = this.getNextEntryId(amount);
		for (int i = 0; i < initializers.length; ++i) {
			initializers[i] = new EntryInitializer(this, array_id_counter, false);
			array_id_counter = new ID(array_id_counter.getIdNumber() + 1);
		}
		return initializers;
	}
	
	/**
	 * Returns an EntryUpdater object that can be used to modify
	 * an entry of this tournament.
	 * 
	 * @return EntryUpdater object
	 * @throws GoException thrown if the necessary XML data file modification fails
	 */
	protected EntryUpdater getEntryUpdater(final ID target_entry_id) 
	throws GoException {
		Entry target_entry;
		if (this.entries.containsKey(target_entry_id)) {
			target_entry = this.entries.get(target_entry_id);
		} else {
			throw new EntryNotFoundException(target_entry_id);
		}
		return new EntryUpdater(this, target_entry);
	}

	/**
	 * Returns an EntryUpdater object that can be used to modify
	 * a pre-entry of this tournament.
	 * 
	 * @return EntryUpdater object
	 * @throws GoException thrown if the necessary XML data file modification fails
	 */
	protected EntryUpdater getPreEntryUpdater(final ID target_entry_id) 
	throws GoException {
		Entry target_entry;
		if (this.pre_entries.containsKey(target_entry_id)) {
			target_entry = this.pre_entries.get(target_entry_id);
		} else {
			throw new EntryNotFoundException(target_entry_id);
		}
		return new EntryUpdater(this, target_entry);
	}

	/**
	 * Returns a TournamentUpdater object that can be used to edit information
	 * and discounts of this tournament.
	 * 
	 * @return TournamentUpdater object
	 * @throws GoException
	 */
	protected TournamentUpdater getTournamentUpdater() throws GoException {
		return new TournamentUpdater(this);
	}

	/**
	 * @param entry_list an ArrayList of Entry objects
	 * @return a HashMap object with ID-Entry pairs
	 */
	private HashMap<ID, Entry> createEntryMap(final ArrayList<Entry> entry_list) {
		final HashMap<ID, Entry> entry_map = new HashMap<ID, Entry>(entry_list.size());
		for (Entry entry : entry_list)
			entry_map.put(entry.getId(), entry);
		return entry_map;

	}


	/**
	 * @return an array of all the registered entries in this tournament for which total fee != paid sum
	 */
	public Entry[] getUnpaidEntries() {
		return getUnpaidEntries(SortBy.LASTNAME);
	}

	/**
	 * @param sort_by an enum that defines how the return array is sorted
	 * @return an array of all the registered entries in this tournament for which total fee != paid sum
	 */	
	public Entry[] getUnpaidEntries(final SortBy sort_by) {
		final ArrayList<Entry> unpaid = new ArrayList<Entry>();
		for (Entry e : this.getEntries())
			if (e.getPaidSum().compareTo(e.getTotalFee()) != 0) {
				unpaid.add(e);
			}
		
		final Entry[] entry_array = unpaid.toArray(new Entry[0]);
		Arrays.sort(entry_array, new EntryComparator(sort_by));
		return entry_array;
	}

	/**
	 * @return an array of all the registered entries in this tournament
	 */
	public Entry[] getEntries() {
		return this.getEntries(SortBy.LASTNAME);
	}

	/**
	 * @param sort_by an enum that defines how the return array is sorted
	 * @return an array of all the registered entries in this tournament 
	 */	
	public Entry[] getEntries(final SortBy sort_by) {
		final Entry[] entry_array = this.entries.values().toArray(new Entry[0]);
		Arrays.sort(entry_array, new EntryComparator(sort_by));
		return entry_array;
	}

	/**
	 * @return an array of all the pre-entries in this tournament
	 */
	public Entry[] getPreEntries() {
		return this.getPreEntries(SortBy.LASTNAME);
	}

	/**
	 * @param sort_by an enum that defines how the return array is sorted
	 * @return an array of all the pre-entries in this tournament 
	 */	
	public Entry[] getPreEntries(final SortBy sort_by) {
		final Entry[] pre_entry_array = this.pre_entries.values().toArray(new Entry[0]);
		Arrays.sort(pre_entry_array, new EntryComparator(sort_by));
		return pre_entry_array;
	}

	/**
	 * @return an array of all the confirmed pre-entries in this tournament
	 */
	public Entry[] getConfirmedPreEntries() {
		return this.getConfirmedPreEntries(SortBy.LASTNAME);
	}

	/**
	 * @param sort_by an enum that defines how the return array is sorted
	 * @return an array of all the confirmed pre-entries in this tournament 
	 */	
	public Entry[] getConfirmedPreEntries(final SortBy sort_by) {
		final Entry[] confirmed_pre_entry_array = this.confirmed_pre_entries.values().toArray(new Entry[0]);
		Arrays.sort(confirmed_pre_entry_array, new EntryComparator(sort_by));
		return confirmed_pre_entry_array;
	
	}

	/**
	 * Method adds a registered entry to the tournament
	 * 
	 * @param entry the Entry object to be added
	 * @throws GoIOException thrown if the XML date file cannot be modified for some reason
	 * @throws TournamentClosedException thrown if the tournament is closed
	 * @throws DuplicatePlayerException thrown if there already is a registered entry with the same first and last name.
	 */
	protected void addEntry(final Entry entry) 
	throws GoIOException, TournamentClosedException, DuplicatePlayerException {
		if (!this.isTournamentOpen()) {
			throw new TournamentClosedException();
		}
		//check that no entry exists with the same name
		String last_name = entry.getLastName();
		final String first_name = entry.getFirstName();
		final Entry[] existing_entries = this.entries.values().toArray(new Entry[0]);
		for (Entry existing_entry : existing_entries) {
			if (last_name.equalsIgnoreCase(existing_entry.getLastName())
				&& first_name.equalsIgnoreCase(existing_entry.getFirstName())) {
				throw new DuplicatePlayerException(existing_entry);
			}
		}
		this.entries.put(entry.getId(), entry);
		this.xml_file.addEntry(entry);
		this.writeLogMessage("New entry added (" + entry + ')');
	}

	/**
	 * Method adds pre-entries to the tournament
	 * 
	 * @param pre_entries_to_add the Entry objects to be added
	 * @throws GoIOException thrown if the XML date file cannot be modified for some reason
	 * @throws TournamentClosedException thrown if the tournament is closed
	 * @throws GoException thrown if preregistration is not open
	 */
	protected void addPreEntry(final Entry... pre_entries_to_add) 
	throws GoException { //GoException sisältää jo GoIOExceptionin sekä TournamentClosedExceptionin
		if (!this.isTournamentOpen()) {
			throw new TournamentClosedException();
		}
		
		if (!this.isPreRegistrationOpen()) {
			throw new GoException("Cannot add enrollment, tournament pre-registration is closed!");
		}
		
		for (Entry entry : pre_entries_to_add) {		
			this.pre_entries.put(entry.getId(), entry);
			this.writeLogMessage("New pre-entry added (" + entry + ')');
		}
		this.xml_file.addPreEntry(pre_entries_to_add);
	}

	/**
	 * Adds pre-entries from a string in wiki-format.
	 * 
	 * Returns -1 if succeeds, otherwise the string line number.
	 * 
	 * @param pre_entries_from_wiki
	 *            wiki string
	 * @throws GoIOException 
	 * @throws TournamentClosedException 
	 */
	public int importPreEntriesFromWiki(final String pre_entries_from_wiki) 
	throws GoException {
		if (!this.isTournamentOpen()) {
			throw new TournamentClosedException();
		}
		// split input by newlines
		final String[] lines = pre_entries_from_wiki.split("\r\n|\r|\n");
		// match first name, last name, rank and club/country
		// the rest is ignored
		// || ||Name||Rank||Club|| Require accommodation || Discounts ||
		// example :|[{Counter}] | Matti Siivola | 5 dan | HGK | | ok |
		final Pattern pattern = Pattern
		.compile("\\A\\|\\s*\\[\\{Counter\\}\\]\\s*\\|"
				+ "\\s*([\\p{L}-&&[^\\|]]+)\\s+[\\s\\p{L}-&&[^\\|]]*?([\\p{L}-&&[^\\|]]+)\\s*\\|"
				+ "\\s*(\\d+)\\s*(d|k|dan|kyu)\\s*\\|"
				+ "\\s*(([\\s\\p{L}-?&&[^\\|]]*?))\\s*\\|.*?\\Z");

		final EntryInitializer[] import_entry_initializers = this.getPreEntryInitializer(lines.length);
		
		for (int i = 0; i < lines.length; i++) {
			final Matcher matcher = pattern.matcher(lines[i]);

			if (matcher.matches() && matcher.groupCount() == 6) {
				final String first_name = matcher.group(1);
				final String last_name = matcher.group(2);
				final String rank_number = matcher.group(3);
				final String rank_type = matcher.group(4);
				final String club = matcher.group(5);

				// set the EntryInitializer object
				import_entry_initializers[i].setLastName(last_name);
				import_entry_initializers[i].setFirstName(first_name);
				import_entry_initializers[i].setRank(rank_type.substring(0, 1) + rank_number);
				import_entry_initializers[i].setClub(club);
				import_entry_initializers[i].setCountry(this.getDefaultCountry());
			} else {
				return i + 1;
			}
		}
		// no errors, add the entries
		Entry[] wikientries = new Entry[import_entry_initializers.length];
		for (int i = 0; i < import_entry_initializers.length; ++i) {
			wikientries[i] = new Entry(import_entry_initializers[i]);
		}
		this.addPreEntry(wikientries);
		return -1;
	}

	/**
	 * @return the common rank list
	 */
	public Rank[] getRankList() {
		return Rank.getRankList();
	}

	/**
	 * @return the common country list 
	 */
	public Country[] getCountryList() {
		return Country.getCountryList();
	}
	
	/**
	 * Writes a message with system date and time to the tournament log file.
	 * 
	 * @param message
	 */
	private void writeLogMessage(final String message) {
		if (this.log_file != null) {
			this.log_file.writeMessage(GoDate.getDateAndTimeString() + ' ' + message);
		}
	}
	
	/**
	 * Opens/closes the tournament preregistration.
	 * 
	 */
	@Override
	public void setPreregistrationOpen(final boolean open) {
		super.setPreregistrationOpen(open);
		if (open) {
			this.writeLogMessage("Tournament.java: Pre-registration opened.");
		} else {
			this.writeLogMessage("Tournament.java: Pre-registration closed.");
		}
			
	}

	/**
	 * Closes the tournament so that its data cannot be modified.
	 * 
	 * @throws GoIOException thrown if the XML data file cannot be modified
	 */
	protected void closeTournament() throws GoIOException {
		this.setTournamentOpen(false);
		this.xml_file.closeTournament();
		this.writeLogMessage("Tournament closed.");
	}
	
	/**
	 * Opens a closed tournament so that its data can be modified.
	 * 
	 * @throws GoIOException thrown if the XML data file cannot be modified
	 */
	protected void reopenTournament() throws GoIOException {
		this.setTournamentOpen(true);
		this.xml_file.openTournament();
		this.writeLogMessage("Tournament reopened.");
	}		
	
	/**
	 * Deletes a closed tournament.
	 * 
	 * Removes all the tournament files.
	 * 
	 * @throws TournamentOpenException thrown if the tournament is not closed when trying to delete it
	 */
	protected void delete() throws TournamentOpenException {
		if (this.isTournamentOpen()) {
			throw new TournamentOpenException("Delete");
		}
		this.xml_file.deleteBackupFiles();
		this.xml_file.delete();
		this.log_file.delete();
		this.registration_export_file.delete();
		this.pre_registration_export_file.delete();
	}
	
	/**
	 * @return a string representation of the Tournament object
	 */
	@Override
	public String toString() {
		String str = "[id:" + this.getId() + ']';
		str += this.getName() + '|' + this.getDate() + '|';
		str += this.getBaseFee().toString()+'€' + '|';
		str += "pre-registration "+(this.isPreRegistrationOpen() ? "open" : "closed") + '|';
		str += "tournament "+(this.isTournamentOpen() ? "open" : "closed");
		
		return str;
	}

	/**
	 * EntryComparator class is used so that Entry objects can be sorted
	 * by different fields of the objects.
	 *
	 */
	public static class EntryComparator implements Comparator<Entry> {

		public static enum SortBy {
			LASTNAME 	{	
				@Override
				protected int simpleSort(final Entry first_entry, final Entry second_entry) {
					return  first_entry.getLastName().compareToIgnoreCase(second_entry.getLastName());
				}
				@Override
				public int sort(final Entry first_entry, final Entry second_entry) {
					final int comp = this.simpleSort(first_entry, second_entry);
					if (comp == 0) {
						return SortBy.RANK.simpleSort(first_entry, second_entry);
					}
					return comp;
				}},
			RANK 		{	
				@Override
				protected int simpleSort(final Entry first_entry, final Entry second_entry) {
					return second_entry.getRank().compareTo(first_entry.getRank());
				}
				@Override
				public int sort(final Entry first_entry, final Entry second_entry) {
					final int comp = this.simpleSort(first_entry, second_entry);
					if (comp == 0) {
						return SortBy.LASTNAME.simpleSort(first_entry, second_entry);
					}
					return comp;
				}};
			
			abstract int simpleSort(Entry first_entry, Entry second_entry);
			abstract int sort(Entry first_entry, Entry second_entry);
		}
		
		private static SortBy sort_by_field;
		
		public EntryComparator(final SortBy sort_by) {
			EntryComparator.sort_by_field = sort_by;
		}
		
		public int compare(final Entry first_entry, final Entry second_entry) {
			return sort_by_field.sort(first_entry, second_entry);
		}
		
	}

}
