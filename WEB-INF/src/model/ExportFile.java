package model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import model.exceptions.GoException;

import settings.Settings;

/**
 * ExportFile class is used to export registration data (i.e. Entry objects)
 * to a text file that can be imported by the Go player pairing program
 * 'McMahon'
 *
 */
public class ExportFile extends File {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -143894777390668906L;

	/**
	 * Creates an export file in the folder specified in Settings.
	 * 
	 * @param pathname name of the export file
	 */
	protected ExportFile(final String pathname) {
		super(Settings.getPublicFolder(), pathname);
	}

	/**
	 * Writes the entry data to the export file
	 * 
	 * @param entries an array of entries to be exported
	 * @throws GoException thrown in case the export fails
	 */
	protected void write(final Entry[] entries) throws GoException {
		String export_str = "";
		for (Entry e : entries) {
			final char delimiter = '|';
			export_str += e.getLastName() + delimiter; // surname
			export_str += e.getFirstName() + delimiter; // first name
			export_str += e.getRank().getMachMahonRank() + delimiter; // strength
			export_str += e.getClub() + delimiter; // club
			export_str += e.getCountry().toString() + delimiter; // country
			export_str += delimiter; // rating (default = 0)
			export_str += 'f' + "\n"; // registration (f=final)
		}
		try {
			final BufferedWriter out = new BufferedWriter(new FileWriter(this));
			out.write(export_str);
			out.close();
		} catch (IOException e) {
			throw new GoException("Writing export file " + this.getAbsolutePath() + " failed.");
		}
		
	}
}
