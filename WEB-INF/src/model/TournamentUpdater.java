package model;

import java.math.BigDecimal;

import model.exceptions.DiscountNotFoundException;
import model.exceptions.GoException;
import model.exceptions.GoIOException;
import model.tournament.TournamentDataModelEditor;

/**
 * TournamentUpdater is used to create a new Tournament object.
 * 
 * When the run-method is called the tournament that is connected to the
 * instance of this class is updated with the data in this class.
 * 
 * A subclass of TournamentDataModelEditor.
 */
public class TournamentUpdater extends TournamentDataModelEditor {

	private final transient Tournament TOURNAMENT;

	/**
	 * Creates a new TournamentUpdater object and links it to the tournament
	 * given as a parameter.
	 */
	protected TournamentUpdater(final Tournament tournament) throws GoException {
		super(tournament.getId());
		this.TOURNAMENT = tournament;
		//default values
		this.setName(this.TOURNAMENT.getName());
		this.setHtml(this.TOURNAMENT.getHtml());
		this.setDefaultCountry(this.TOURNAMENT.getDefaultCountry());
		this.setBaseFee(this.TOURNAMENT.getBaseFee());
		this.setDate(this.TOURNAMENT.getDate());
		this.setPreregistrationOpen(this.TOURNAMENT.isPreRegistrationOpen());
		for (Discount d : tournament.getDiscounts())
			this.addDiscount(d);
	}

	protected Tournament getTournament() {return this.TOURNAMENT;}
	
	/**
	 * An implementation of the abstract method in superclass TournamentDataModel.
	 * Implemented here so that the method has public visibility.
	 * 
	 * Creates a new Discount object from the parameters and adds it 
	 * to the data structure.
	 */
	@Override
	public void addDiscount(final String name, final String amount, final boolean dominant) throws GoIOException {
		this.addDiscount(new Discount(this.TOURNAMENT.getNextDiscountId(), name, new BigDecimal(amount), dominant));
	}

	/**
	 * Replaces a discount with the given ID with a new discount created from
	 * the info in the parameters. The ID of the new Discount object is the
	 * same as the old ID.
	 * 
	 * @throws DiscountNotFoundException if the discount is not found
	 */
	public void editDiscount(final ID discount_id, final String name, final String amount, final boolean dominant) throws DiscountNotFoundException {
		if (!this.hasDiscountId(discount_id)) {
			throw new DiscountNotFoundException(discount_id);
		}
		this.addDiscount(new Discount(discount_id, name, new BigDecimal(amount), dominant));
	}

	@Override
	public void removeDiscount(final ID discount_id) {
		super.removeDiscount(discount_id);
	}
	
	@Override
	public Discount[] getDiscounts() {
		return super.getDiscounts();
	}

	/**
	 * Edits the tournament linked to this instance of class TournamentUpdater
	 * with the data in this instance.
	 */
	@Override
	public void run() throws GoException {
		this.TOURNAMENT.editTournament(this);
	}
}
