package model;

import java.math.BigDecimal;

import model.entry.EntryDataModelEditor;
import model.exceptions.GoException;


/**
 * EntryInitializer is used to create a new Entry object.
 * 
 * When the run-method is called the a new entry is created
 * with the data in the initializer and added to the tournament that
 * is linked to the initializer.
 * 
 * A subclass of EntryDataModelEditor.
 */
public class EntryInitializer extends EntryDataModelEditor {

	private final transient Tournament TOURNAMENT;
	private final transient boolean IS_PRE_ENTRY;

	/**
	 * Creates a new initializer with the given entryID object and links
	 * it to the tournament given as a parameter.
	 */
	protected EntryInitializer(final Tournament tournament, final ID entry_id, final boolean is_pre_entry) throws GoException {
		super(entry_id);
		this.TOURNAMENT = tournament;
		this.IS_PRE_ENTRY = is_pre_entry;
		//set default values:
		this.setLastName("unknown");
		this.setFirstName("unknown");
		this.setClub("unknown");
		this.setRank(Rank.getLowestRank());
		this.setCountry(Country.getUnknownCountry());
		this.setTotalFee(BigDecimal.ZERO);
		this.setPaidSum(BigDecimal.ZERO);
		this.setInfo("");
		this.setDateOfBirth("");
	}
	
	/**
	 * An implementation of the abstract method in superclass EntryDataModel.
	 * Implemented here so that the method has public visibility.
	 * 
	 * Sets the date_of_birth field in the data structure.
	 */
	@Override
	protected void setDateOfBirth(final String dob) {
		super.setDateOfBirth(dob);
	}

	/**
	 * An implementation of the abstract method in superclass EntryDataModel.
	 * Implemented here so that the method has public visibility.
	 * 
	 * When the method is called the a new Entry object is created
	 * with the data in this EntryInitializer instance and then added to
	 * the tournament that is linked to this initializer.
	 */
	@Override
	public void run() throws GoException { 
		if (this.IS_PRE_ENTRY) {
			this.TOURNAMENT.addPreEntry(new Entry(this));
		} else {
			this.TOURNAMENT.addEntry(new Entry(this));
		}
	}

	public void run(final ID confirmed_pre_entry_id) throws GoException { 
		if (this.IS_PRE_ENTRY) {
			throw new GoException("EntryInitializer.java: Can't use an pre-entry EntryInitializer to confirm an entry");
		}
		this.TOURNAMENT.addEntry(new Entry(this));
		this.TOURNAMENT.confirmPreEntry(confirmed_pre_entry_id);
	}

}
