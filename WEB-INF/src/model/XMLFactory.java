package model;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import model.exceptions.GoException;
import model.exceptions.XMLParsingException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Helper class for XMLFile
 *
 */
public class XMLFactory {
	
	/**
	 * Creates an XML element with subelements that can be used to store
	 * data from an Entry object
	 * 
	 * @param doc parent document
	 * @param entry the entry to be stored
	 * @return an XML element named 'entry' with data from theentry
	 */
	protected static Element createEntryElement(final Document doc, final Entry entry) {
		//create new element
		final Element entry_elem = doc.createElement("entry");
		//create entry element structure
		for (String field_name : Entry.ENTRY_FIELDS)
			entry_elem.appendChild(doc.createElement(field_name));
		//set entry element field values
		setSubElementText(entry_elem, entry.getId().toString(), "id");
		setSubElementText(entry_elem, entry.getLastName(), "lastName");
		setSubElementText(entry_elem, entry.getFirstName(), "firstName");
		setSubElementText(entry_elem, entry.getRank().toString(), "rank");
		setSubElementText(entry_elem, entry.getClub(), "club");
		setSubElementText(entry_elem, entry.getCountry().toString(), "country");
		setSubElementText(entry_elem, entry.getTotalFee().toString(), "totalFee");
		setSubElementText(entry_elem, entry.getPaidSum().toString(), "paid");
		setSubElementText(entry_elem, entry.getInfo(), "info");
		setSubElementText(entry_elem, entry.getDateOfBirth(), "dateOfBirth");
		//set entry discounts
		for (ID discount_id :entry.getDiscountIds()) {
			final Element disc_id_elem = doc.createElement("discount_id");
			disc_id_elem.setTextContent(discount_id.toString());
			entry_elem.appendChild(disc_id_elem);
		}
		
		return entry_elem;
	}
	
	/**
	 * Creates an XML element with subelements that can be used to store
	 * data from a Discount object
	 * 
	 * @param doc parent document
	 * @param discount the discount to be stored
	 * @return an XML element named 'discount' with data from the discount
	 */
	protected static Element createDiscountElement(final Document doc, final Discount discount) {
		final Element elem_discount = doc.createElement("discount");
		final Element elem_discount_id = doc.createElement("discountId");
		final Element elem_discount_name = doc.createElement("name");
		Element elem_discount_amount = doc.createElement("amount");
		final Element elem_discount_dominant = doc.createElement("dominant");

		elem_discount_id.setTextContent(discount.getId().toString());
		elem_discount_name.setTextContent(discount.getName());
		elem_discount_amount.setTextContent(discount.getAmount().toString());
		elem_discount_dominant.setTextContent(Boolean.toString(discount.isDominant()));

		elem_discount.appendChild(elem_discount_id);
		elem_discount.appendChild(elem_discount_name);
		elem_discount.appendChild(elem_discount_amount);
		elem_discount.appendChild(elem_discount_dominant);
		
		return elem_discount;		
	}
	
	/**
	 * Creates a new Entry object from the entry XML element
	 * 
	 * @param entry_elem XML element
	 * @param initializer used to set the data of the new entry
	 * @return new Entry object
	 * @throws GoException throw if the initializer can't be read
	 */
	protected static Entry parseEntry(final Element entry_elem, final EntryInitializer initializer) throws GoException {
		//put field data to the initializer
		initializer.setLastName(getSubElementText(entry_elem, "lastName"));
		initializer.setFirstName(getSubElementText(entry_elem, "firstName"));
		initializer.setRank(getSubElementText(entry_elem, "rank"));
		initializer.setClub(getSubElementText(entry_elem, "club"));
		initializer.setCountry(getSubElementText(entry_elem, "country"));
		initializer.setTotalFee(getSubElementText(entry_elem, "totalFee"));
		initializer.setPaidSum(getSubElementText(entry_elem, "paid"));
		initializer.setInfo(getSubElementText(entry_elem, "info"));
		//discount ids
		for (Element elem_discount : getSubElements(entry_elem, "discount_id")) {
			final ID element_id = new ID(elem_discount.getTextContent());
			initializer.setDiscountState(element_id, true);
		}

		return new Entry(initializer);
		
	}	
	
	/**
	 * Creates an XML document based on a XML document file
	 * 
	 * @param xml_file XML data file
	 * @return an XML document
	 * @throws XMLParsingException
	 */
	protected static Document createXMLDataDocument(final File xml_file) throws XMLParsingException {
		try {
			final DocumentBuilderFactory doc_factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder doc_builder = doc_factory.newDocumentBuilder();
			return doc_builder.parse(xml_file);
		} catch (Exception e) {
			throw new XMLParsingException(xml_file.getAbsolutePath());
		}
		
	}
	
	/**
	 * Creates an empty XML data document with the correct element structure
	 * 
	 * @return XML data document
	 * @throws GoException
	 */
	protected static Document createXMLDataDocument() throws GoException {
		Document new_doc;
		//try to create new Document object
		try {
			DocumentBuilderFactory doc_factory = DocumentBuilderFactory.newInstance();
			final DocumentBuilder doc_builder = doc_factory.newDocumentBuilder();
			new_doc = doc_builder.newDocument();
		} catch (Exception e) {
			throw new GoException("A new XML data document object could not be created.");
		}

		//create header element:
		final Element elem_header = new_doc.createElement("header");
		//create tournament header element structure
		for (String field_name : Tournament.TOURNAMENT_HEADER_FIELDS)
			elem_header.appendChild(new_doc.createElement(field_name));

		//set id counters to 0
		setSubElementText(elem_header, "0", "entryIdCounter");
		setSubElementText(elem_header, "0", "discountIdCounter");

		//create XML document root element
		final Element elem_tournament = new_doc.createElement("tournament");
		//append XML document main elements
		elem_tournament.appendChild(elem_header);
		elem_tournament.appendChild(new_doc.createElement("entries"));
		elem_tournament.appendChild(new_doc.createElement("preEntries"));
		elem_tournament.appendChild(new_doc.createElement("confirmedPreEntries"));
		
		//append root element to new XML document
		new_doc.appendChild(elem_tournament);

		return new_doc;
	}

	/**
	 * Returns the sub element text content of a descendant of the parameter 'element'.
	 * 
	 * (see method getSubElements)
	 */
	protected static String getSubElementText(final Element element, final String... sub_element_names) {
		final Element elem_last = getSubElements(element, sub_element_names)[0];
		return elem_last.getTextContent();
	}

	/**
	 * Sets the sub element text content of a descendant of the parameter 'element'.
	 * 
	 * (see method getSubElements)
	 */
	protected static String setSubElementText(final Element element, final String new_text,
			final String... sub_element_names) {
		final Element elem_last = getSubElements(element, sub_element_names)[0];
		String old_text = elem_last.getTextContent();
		elem_last.setTextContent(new_text);
		return old_text;
	}
		
	/**
	 * Returns a descendant of the parameter 'element'. The descendant 
	 * is found by going down the element tree according to the 
	 * 'sub_element_names' parameters.
	 */
	protected static Element[] getSubElements(final Element element, final String... sub_element_names) {
		NodeList nodelist = element.getElementsByTagName(sub_element_names[0]);
		for (int i = 1; i < sub_element_names.length; ++i) {
			nodelist = ((Element) nodelist.item(0))
					.getElementsByTagName(sub_element_names[i]);
		}
		Element[] return_array = new Element[nodelist.getLength()];
		for (int i = 0; i < nodelist.getLength(); ++i) {
			return_array[i] = (Element) nodelist.item(i);
		}
		return return_array;
	}

	/**
	 * Removes an XML element from a document.
	 */
	protected static Element removeElement(final Element element) {
		return (Element) element.getParentNode().removeChild(element);
	}

}
