package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import settings.Settings;

/**
 * Class is used to read a country list from a file.
 *
 */
public class CountryListFile extends File {

	/**
	 * UID for parallelism testing
	 */
	private static final long serialVersionUID = 8789281302738151481L;

	/**
	 * Constructor creates a new CountryListFile based on the country list
	 * file specified in Settings.
	 */
	protected CountryListFile() {
		super(Settings.getDataFolder(), Settings.COUNTRYLIST_FILE_NAME);
		
	}
	
	/**
	 * Method parses the country list file and creates Country objects
	 * that are essentially country code - country list pairs.
	 * 
	 * @return an array of Country objects 
	 */
	protected Country[] getCountryList() {
		final ArrayList<String> lines = new ArrayList<String>();
		Scanner scanner;
		try {
			scanner = new Scanner(this);
			while (scanner.hasNextLine()) {
				final String line = scanner.nextLine();
				lines.add(line);
			}
			scanner.close();
			Country[] country_array = new Country[lines.size()];
			for (int i = 0; i < country_array.length; ++i)
				country_array[i] = new Country(lines.get(i).substring(0, 2), lines.get(i).substring(3));
			return country_array;
		} catch (FileNotFoundException e) {
			System.out.println("CountryListFile.java: Unable to read country list. The exact error message is: "
					+ e);
			return null;
		}		
	}
}
