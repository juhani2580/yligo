package model.tournament;

import java.math.BigDecimal;

import model.Country;
import model.GoDate;
import model.ID;
import model.exceptions.GoException;
import model.exceptions.GoIOException;
import model.exceptions.ParseGoDateException;

/**
 * This class gives the set-methods of the superclass TournamentDataModel 
 * public visibility so that the data can be changed.
 * 
 * Editing and creating new Tournament objects is done via 
 * TournamentUpdater and TournamentInitializer objects that extend this
 * class.
 * 
 * See TournamentDataModel, TournamentInitializer, TournamentUpdater
 * and the architecture document for more information.
 * 
 */
public abstract class TournamentDataModelEditor extends TournamentDataModel {
	
	protected TournamentDataModelEditor(final ID tournament_id) {
		super(tournament_id);
	}
	
	//override some of parent TournametDataModels set-methods 
	//(give public visibility)
	@Override
	public void setName(final String name) throws GoException {super.setName(name);}
	@Override
	public void setHtml(final String html) {super.setHtml(html);}
	@Override
	public void setDefaultCountry(final String default_country_code) throws GoException {
		super.setDefaultCountry(default_country_code);
	}
	
	@Override
	public void setDefaultCountry(final Country default_country) throws GoException {
		super.setDefaultCountry(default_country);
	}
	
	@Override
	public void setBaseFee(final String new_base_fee) throws GoException {
		super.setBaseFee(new_base_fee);
	}
	
	@Override
	public void setBaseFee(final BigDecimal base_fee) throws GoException {
		super.setBaseFee(base_fee);
	}
	
	@Override
	public void setDate(final String date_str) throws ParseGoDateException {
		super.setDate(date_str);
	}
	
	@Override
	public void setDate(final GoDate date) throws GoException {
		super.setDate(date);
	}

	@Override
	public void setPreregistrationOpen(final boolean open) {
		super.setPreregistrationOpen(open);
	}

	@Override
	public void setTournamentOpen(final boolean open) {
		super.setTournamentOpen(open);
	}

	//set new methods for subclasses to implement
	public abstract void addDiscount(String name, String amount, boolean dominant) throws GoIOException;		

	public abstract void run() throws GoException;

}
