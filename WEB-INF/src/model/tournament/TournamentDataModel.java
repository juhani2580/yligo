package model.tournament;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;

import model.Country;
import model.Discount;
import model.GoDate;
import model.ID;
import model.exceptions.GoException;
import model.exceptions.ParseGoDateException;

/**
 * The class represents the data structure of a Tournament object.
 * 
 * It also gives its public get-methods and protected set-methods.
 * 
 * The child class TournamentDataModelEditor gives the set-methods public 
 * visibility so that the data can be changed.
 * 
 * Editing and creating new Tournament objects is done via 
 * TournamentUpdater and TournamentInitializer objects that extend the
 * class TournamentDataModelEditor.
 * 
 * See the architecture document for more information.
 *
 */
public abstract class TournamentDataModel {

	private final transient ID TOURNAMENT_ID;
	
	private String name;
	private String html;
	private transient Country default_country;
	private transient BigDecimal base_fee;
	private GoDate date;
	private transient boolean preregistration_open;
	private transient HashMap<ID, Discount> discount_map = new HashMap<ID, Discount>(5);
	private transient boolean tournament_open;
	
	protected TournamentDataModel(final ID tournament_id) {
		this.TOURNAMENT_ID = tournament_id;
	}
	
	//get methods
	public ID getId() {
		return this.TOURNAMENT_ID;
	}
	
	public String getName() {return this.name;}
	public String getHtml() {return this.html;}
	public Country getDefaultCountry() {return this.default_country;}
	public BigDecimal getBaseFee() {return this.base_fee;}
	public GoDate getDate() {return this.date;}
	public boolean isPreRegistrationOpen() {return this.preregistration_open;}
	public boolean isTournamentOpen() {return this.tournament_open;}

	public Discount[] getDiscounts() {
		final Discount[] discount_array = this.discount_map.values().toArray(new Discount[0]);
		Arrays.sort(discount_array, new Discount.DiscountComparator());
		return discount_array;
	}

	protected void setName(final String name_to_set) throws GoException {
		if (name_to_set == null || name_to_set.length() == 0) {
			throw new GoException("Tournament name must be entered!");
		}	
		this.name = name_to_set;
	}

	protected void setHtml(final String set_html) {this.html = set_html;}

	protected void setDefaultCountry(final String default_country_code) throws GoException {
		this.default_country = Country.parseCountryByCode(default_country_code);
	}
	protected void setDefaultCountry(final Country default_country_to_set) throws GoException {
		if (default_country_to_set == null) {
			throw new GoException("Field 'default country' cannot be set to null!");
		}	
		this.default_country = default_country_to_set;
	}
	
	protected void setBaseFee(final String new_base_fee) throws GoException {
		try {
			this.base_fee = new BigDecimal(new_base_fee);
		} catch (Exception e) {
			throw new GoException("String '" + new_base_fee + "' cannot be parsed as a number!");
		}
	}
	
	protected void setBaseFee(final BigDecimal new_base_fee) throws GoException {
		if (new_base_fee == null) {
			throw new GoException("Field 'Base fee' cannot be set to null!");
		}
		this.base_fee = new_base_fee;
	}
	
	protected void setDate(final String date_str) throws ParseGoDateException {
		this.date = new GoDate(date_str);
	}
	
	protected void setDate(final GoDate new_date) throws GoException {
		if (new_date == null) {
			throw new GoException("Field 'Date' cannot be set to null!");
		}
		this.date = new_date;
	}

	
	protected void setPreregistrationOpen(final boolean open) {
		this.preregistration_open = open;
	}
	
	protected void setTournamentOpen(final boolean open) {
		this.tournament_open = open;
		System.out.println(this.tournament_open);		
	}
	
	protected void setDiscounts(final Discount[] new_discounts) {
		//clear old discounts
		this.discount_map = new HashMap<ID, Discount>(new_discounts.length);
		//add new discounts
		for (Discount new_d : new_discounts) {
			if (new_d != null) {
				this.addDiscount(new_d);
			}
		}
	}
	
	protected void addDiscount(final Discount new_discount) {
		if (new_discount != null) {
			this.discount_map.put(new_discount.getId(), new_discount);
		}
	}

	protected boolean hasDiscountId(final ID discount_id) {
		return this.discount_map.containsKey(discount_id);
	}

	protected void removeDiscount(final ID discount_id) {
		this.discount_map.remove(discount_id);
	}
}
