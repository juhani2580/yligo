package model;

import model.entry.EntryDataModel;
import model.exceptions.GoException;


/**
 * This class creates a single instance of an Entry object.
 * 
 * The data structure of the entry is based on the class 
 * model.entry.EntryDataModel.
 * 
 * Editing and creating a new entry is handled via
 * the EntryUpdater and EntryInitializer objects.
 * 
 */
public class Entry extends EntryDataModel {

	public static final String[] ENTRY_FIELDS = {
		"id",		"firstName",		"lastName",
		"rank",		"club",				"country",
		"totalFee",	"paid",				"info",		"dateOfBirth"};
	/**
	 * Constructor creates an Entry object based on a EntryInitializer object
	 * that contains all the data to be stored in the entry.
	 * 
	 * @param initializer EntryInitializer object, contains entry data
	 * @throws GoException 
	 */
	protected Entry(final EntryInitializer initializer) throws GoException {
		super(initializer.getId());
		
		super.setLastName(initializer.getLastName());
		super.setFirstName(initializer.getFirstName());
		super.setRank(initializer.getRank());
		super.setClub(initializer.getClub());
		super.setCountry(initializer.getCountry());
		super.setTotalFee(initializer.getTotalFee());
		super.setPaidSum(initializer.getPaidSum());
		super.setInfo(initializer.getInfo());
		super.setDateOfBirth(initializer.getDateOfBirth());
		for (ID id : initializer.getDiscountIds())
			super.setDiscountState(id, true);
	}

	/**
	 * Updates the Entry object.
	 * 
	 * @param updater EntryUpdater instance containing new entry data
	 * @throws GoException 
	 */
	protected void editEntry(final EntryUpdater updater) throws GoException {
		super.setLastName(updater.getLastName());
		super.setFirstName(updater.getFirstName());
		super.setRank(updater.getRank());
		super.setClub(updater.getClub());
		super.setCountry(updater.getCountry());
		super.setTotalFee(updater.getTotalFee());
		super.setPaidSum(updater.getPaidSum());
		super.setInfo(updater.getInfo());
		super.setDateOfBirth(updater.getDateOfBirth());
		//handle selected discounts:
		super.clearDiscounts();
		for (ID id : updater.getDiscountIds())
			super.setDiscountState(id, true);
	}	

	/**
	 * Checks if the Entry object has a given discount.
	 * 
	 * @param query_id discount id
	 * @return true if the entry has a given discount, otherwise false
	 */
	public final boolean hasDiscountId(final ID query_id) {
		for (ID id : this.getDiscountIds())
			if (id.getIdNumber() == query_id.getIdNumber()) {
				return true;
			}
		return false;
	}
	
	/**
	 * Returns the contents of the Entry in a formatted string.
	 * 
	 * @return entry string
	 */
	@Override
	public String toString() {
		final String to_str = "[id:"+ this.getId() + ']' + 
					this.getLastName() + '|' + this.getFirstName() + '|' +
					this.getRank().toString() + '|' + this.getClub() + '|' +
					this.getCountry() + '|' + this.getInfo();		
		return to_str;
	}
}
