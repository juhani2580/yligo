package model;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import model.exceptions.GoException;

/**
 * Country objects are used to store the nationality of the player
 * and the default country of the tournament. 
 *
 */

public class Country {

	private final transient String code;
	private final transient String name;

	private static HashMap<String, Country> country_map;
	private static Country[] country_list;

	/**
	 * Static initialization block reads the country list from the
	 * CountryListFile object when class Country is called for the first time
	 * and creates a mapping for country code - country name pairs.
	 */
	static {
		final CountryListFile country_list_file = new CountryListFile();
		country_list = country_list_file.getCountryList();
		Arrays.sort(country_list, new CountryComparator());
		country_map = new HashMap<String, Country>();
		for (Country c : country_list)
			country_map.put(c.getCode(), c);
	}

	/**
	 * Constructor creates a Country object with a country code and matching
	 * country name.
	 * 
	 * @param countrycode two letter country code
	 * @param countryname country name
	 */
	protected Country(final String countrycode, final String countryname) {
		this.code = countrycode;
		this.name = countryname;
	}

	/**
	 * Returns country code.
	 * 
	 * @return two letter country code
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * Returns country name.
	 * 
	 * @return country name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Returns corresponding country name from a given country code.
	 * 
	 * @param country_code country code
	 * @return country name
	 * @throws GoException 
	 */
	public static Country parseCountryByCode(final String country_code) throws GoException {
		if (!country_map.containsKey(country_code)) {
			throw new GoException("String '" + country_code + "' cannot be parsed as a Country!");
		}
		return country_map.get(country_code);
	}

	/**
	 * Returns a list of countries.
	 * 
	 * @return country list in an array
	 */
	public static Country[] getCountryList() {
		return country_list.clone();
	}

	/**
	 * Returns country code in a string.
	 */
	@Override
	public final String toString() {
		return this.code;
	}

	/**
	 * Method returns the default country that is used to set the
	 * country field in an EntryInitializer object by default.
	 * 
	 * @return Country object with code FI
	 * @throws GoException
	 */
	public static Country getUnknownCountry() throws GoException {
		return Country.parseCountryByCode("FI");
	}

	/**
	 * Inner class CountryComparator is used to enable sorting
	 * Country objects by their country names.
	 *
	 */
	static class CountryComparator implements Comparator<Object> {
		public int compare(final Object first_object, final Object second_object) {
			if (!(first_object instanceof Country) || !(second_object instanceof Country)) {
				throw new ClassCastException();
			}
			return ((Country)first_object).getName().compareToIgnoreCase(((Country)second_object).getName());
		}
		
	}
}
