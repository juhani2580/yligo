package model;

/**
 * Class for IDs used in Entry, Tournament and Discount objects.
 * 
 */
public class ID implements Comparable<Object> {

	private final transient long id_number;

	/**
	 * Overloaded constructor
	 * 
	 * @param id_str discount id
	 */
	public ID(final String id_str) {
		this.id_number = Long.parseLong(id_str);
	}

	/**
	 * Overloaded constructor.
	 * 
	 * @param id_str discount id
	 */
	public ID(final long id_str) {
		this.id_number = id_str;
	}

	/**
	 * Returns id number.
	 * 
	 * @return id number
	 */
	public long getIdNumber() {
		return this.id_number;
	}

	/**
	 * Checks whether one ID object is equal to another.
	 * 
	 * @param other ID instance to compare to
	 * @return true if both have the same id number, otherwise false.
	 */
	@Override
	public boolean equals(final Object other) {
		// ID object compared with itself
		if (this == other) {
			return true;
		}
		// ID compared with String
		if (other instanceof String) {
			return this.toString().equals(other);
		}
		// ID compared with non-ID-object
		if (!(other instanceof ID)) {
			return false;
		}
		// ID compared with other ID
		if (other instanceof ID
				&& this.getIdNumber() == ((ID) other).getIdNumber()) {
			return true;
		}

		return false;
	}

	/**
	 * Methods hashCode and equals are overridden in order to be able
	 * to use ID objects as keys in hashMap objects.
	 */
	@Override
	public int hashCode() {
		return (int) this.id_number;
	}
	
	/**
	 * Method returns a string representation of the ID object
	 * 
	 * @return the id number of the ID object as a string
	 */
	@Override
	public String toString() {
		return Long.toString(this.id_number);
	}

	
	/**
	 * The compareTo method is overridden so that ID objects can
	 * be sorted based on their id numbers.
	 */
	@Override
	public int compareTo(final Object other) {
		
		if (this.id_number > ((ID)other).getIdNumber()) {
			return 1;
		} else if (this.id_number < ((ID)other).getIdNumber()) {
			return -1;
		} else {
			return 0;
		}
		
	}

}
