package model.entry;

import java.math.BigDecimal;


import model.Country;
import model.ID;
import model.Rank;
import model.exceptions.GoException;

/**
 * This class gives the set-methods of the superclass EntryDataModel 
 * public visibility so that the data can be changed.
 * 
 * Editing and creating new Entry objects is done via 
 * EntryUpdater and EntryInitializer objects that extend this
 * class.
 * 
 * See EntryDataModel, EntryInitializer, EntryUpdater
 * and the architecture document for more information.
 * 
 */

public abstract class EntryDataModelEditor extends EntryDataModel {

	protected EntryDataModelEditor(final ID entry_id) {
		super(entry_id);
	}

	@Override
	public void setLastName(final String last_name) throws GoException {
		super.setLastName(last_name);
	}

	@Override
	public void setFirstName(final String first_name) throws GoException {
		super.setFirstName(first_name);
	}

	@Override
	public void setClub(final String club) throws GoException {
		super.setClub(club);
	}
	
	@Override
	public void setRank(final String rank) throws GoException {
		super.setRank(rank);
	}
	@Override
	public void setRank(final Rank rank) throws GoException {
		super.setRank(rank);
	}
	
	@Override
	public void setCountry(final Country country) throws GoException {
		super.setCountry(country);
	}

	@Override
	public void setCountry(final String country_code) throws GoException {
		super.setCountry(country_code);
	}

	@Override
	public void setTotalFee(final String total_fee) throws GoException {
		super.setTotalFee(total_fee);
	}
	
	@Override
	public void setTotalFee(final BigDecimal total_fee) throws GoException {
		super.setTotalFee(total_fee);
	}

	@Override
	public void setPaidSum(final String paid_sum) throws GoException {
		super.setPaidSum(paid_sum);
	}
	
	@Override
	public void setPaidSum(final BigDecimal paid_sum) throws GoException {
		super.setPaidSum(paid_sum);
	}
	
	@Override
	public void setInfo(final String info) {
		super.setInfo(info);
	}

	@Override
	public void setDateOfBirth(final String day, final String month) throws GoException {
		super.setDateOfBirth(day, month);
	}

	@Override
	public void setDiscountState(final ID discount_id, boolean selected) throws GoException {
		super.setDiscountState(discount_id, selected);
	}
	
	protected void clearDiscounts() {
		super.clearDiscounts();
	}
	
	public abstract void run() throws GoException;
}
