package model.entry;

import java.math.BigDecimal;
import java.util.HashSet;
import model.Country;
import model.ID;
import model.Rank;
import model.exceptions.EntryDateNotSetException;
import model.exceptions.GoException;

/**
 * The class represents the data structure of an Entry object.
 * 
 * It also gives its public get-methods and protected set-methods.
 * 
 * The child class EntryDataModelEditor gives the set-methods public 
 * visibility so that the data can be changed.
 * 
 * Editing and creating new Entry objects is done via 
 * EntryUpdater and EntryInitializer objects that extend the
 * class EntryDataModelEditor.
 * 
 * See the architecture document for more information.
 *
 */

public abstract class EntryDataModel {
	
	private final transient ID ENTRY_ID;

	private transient String last_name;
	private transient String first_name;
	private String club;
	private Rank rank;
	private Country country;
	private transient BigDecimal total_fee;
	private transient BigDecimal paid_sum;	
	private HashSet<ID> discount_id_set = new HashSet<ID>(5);
	private String info;
	private transient String date_of_birth;
	
	protected EntryDataModel(final ID entry_id) {
		this.ENTRY_ID = entry_id;
	}
	
	public ID getId() {
		return this.ENTRY_ID;
	}

	public String getFirstName() {return this.first_name;}
	public String getLastName() {return this.last_name;}
	public String getClub() {return this.club;}
	public Rank getRank() {return this.rank;}
	public Country getCountry() {return this.country;}
	public BigDecimal getTotalFee() {return this.total_fee;}
	public BigDecimal getPaidSum() {return this.paid_sum;}
	public String getInfo() {return this.info;}

	public String getDateOfBirth() {return this.date_of_birth;}

	public String getBirthDayOfMonth() throws EntryDateNotSetException {
		if (this.date_of_birth == null || this.date_of_birth.length() != 4) {
			throw new EntryDateNotSetException();
		}
		return this.date_of_birth.substring(0, 2);
	}

	public String getBirthMonthOfYear() throws EntryDateNotSetException {
		if (this.date_of_birth == null || this.date_of_birth.length() != 4) {
			throw new EntryDateNotSetException();
		}
		return this.date_of_birth.substring(2, 4);
	}

	public ID[] getDiscountIds() {
		return this.discount_id_set.toArray(new ID[0]);
	}
	
	protected void setFirstName(final String new_first_name) throws GoException {
		if (new_first_name == null || new_first_name.length() == 0) {
			throw new GoException("First name must be entered!");
		}
		this.first_name = new_first_name;
	}
	
	protected void setLastName(final String new_last_name) throws GoException {
		if (new_last_name == null || new_last_name.length() == 0) {
			throw new GoException("Last name must be entered!");
		}
		this.last_name = new_last_name;
	}
	
	protected void setClub(final String new_club) throws GoException {
		if (new_club == null) {
			throw new GoException("Field 'Club' cannot be set to null!");
		}
		this.club = new_club;
	}
	
	protected void setRank(final String new_rank) throws GoException {
		this.rank = Rank.parseRank(new_rank);
	}
	
	protected void setRank(final Rank new_rank) throws GoException {
		if (new_rank == null) {
			throw new GoException("Field 'Rank' cannot be set to null!");
		}
		this.rank = new_rank;
	}
	

	protected void setCountry(final String country_code) throws GoException {
		this.country = Country.parseCountryByCode(country_code);
	}

	protected void setCountry(final Country new_country) throws GoException {
		if (new_country == null) {
			throw new GoException("Field 'Country' cannot be set to null!");
		}
		this.country = new_country;
	}

	protected void setTotalFee(final String new_total_fee) throws GoException {
		try {
			this.total_fee = new BigDecimal(new_total_fee);
		} catch (Exception e) {
			throw new GoException("String '" + new_total_fee + "' cannot be parsed as a number!");
		}
	}
	
	protected void setTotalFee(final BigDecimal new_total_fee) throws GoException {
		if (new_total_fee == null) {
			throw new GoException("Field 'Total fee' cannot be set to null!");
		}
		this.total_fee = new_total_fee;
	}

	protected void setPaidSum(final String new_paid_sum) throws GoException {
		try {
			this.paid_sum = new BigDecimal(new_paid_sum);
		} catch (Exception e) {
			throw new GoException("String '" + new_paid_sum + "' cannot be parsed as a number!");
		}
	}
	
	protected void setPaidSum(final BigDecimal new_paid_sum) throws GoException {
		if (new_paid_sum == null) {
			throw new GoException("Field 'Paid sum' cannot be set to null!");
		}
		this.paid_sum = new_paid_sum;
	}
	
	protected void setInfo(final String new_info) {this.info = new_info;}

	//for model use only:
	protected void setDateOfBirth(final String dob) {this.date_of_birth = dob;}
	
	@SuppressWarnings("boxing")
	protected void setDateOfBirth(final String day, final String month) throws GoException {
		int day_no;
		int month_no;	
		try {
			day_no = Integer.parseInt(day);
			month_no = Integer.parseInt(month);
		} catch(NumberFormatException e) {
			throw new GoException("Invalid day and/or month given when trying to create a new entry!");			
		}
		if (day_no > 31 || day_no < 1 || month_no > 12 || month_no < 1) {
			throw new GoException("Invalid day and/or month given when trying to create a new entry!");
		}
		this.date_of_birth = "" + (day_no < 10 ? "0"+day_no : day_no)
		+ (month_no < 10 ? "0"+month_no : month_no);
	}
	
	protected void setDiscountState(final ID discount_id, boolean selected) throws GoException {
		if (discount_id == null) {
			throw new GoException("Discount ID cannot be null!");
		}
		if (!selected && this.discount_id_set.contains(discount_id))
			this.discount_id_set.remove(discount_id);
		else if (selected)
			this.discount_id_set.add(discount_id);
	}

	protected void clearDiscounts() {
		this.discount_id_set = new HashSet<ID>();
	}

}
