package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import model.exceptions.ParseGoDateException;

/**
 * Class GoDate is used to parse and store dates used by the program.
 * 
 * Also provides static methods that can be used to print the system
 * date and/or time.
 *
 */
public class GoDate {

	private transient Date date;
    private static String[] date_formats = {"d.M.yy",
											"d/M/yy",
											"d M yy"};
		
    /**
     * Constructor tries to parse a new GoDate object from the parameter string
     * 
     * @param date_str is converted to a GoDate object
     * @throws ParseGoDateException thrown if the parameter string cannot be parsed as a GoDate object
     */
	public GoDate(final String date_str) throws ParseGoDateException {
        //go through date formats one by one and try to parse Date from string parameter
		for (String date_format : date_formats) {
			final SimpleDateFormat sdf = new SimpleDateFormat(date_format, Locale.getDefault());
        	try {
    			//try to parse date from string parameter with current date format
        		this.date = sdf.parse(date_str);
        		//date parsed successfully, stop for loop
        		return;
        	} catch (ParseException e) {
        		throw new ParseGoDateException(date_str);
        	} //try next date format
		}
        //string cannot be parsed, throw exception
		throw new ParseGoDateException(date_str);
	}
	
	@Override
	/**
	 * Method returns a string representation of the GoDate object
	 * 
	 * @return a string representation of the GoDate object
	 */
	public String toString() {
		final SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		return format.format(this.date);	
	}

	/**
	 * Method for getting the system date.
	 * 
	 * @return system date as string
	 */
	public static String getDateString() {
		final Calendar calendar = Calendar.getInstance();
		String day = Integer.toString(calendar.get(Calendar.DATE));
		String month = Integer.toString(calendar.get(Calendar.MONTH)+1);
		final String year = Integer.toString(calendar.get(Calendar.YEAR));
		if (day.length() < 2) {
			day = '0' + day;
		}
		if (month.length() < 2) {
			month = '0' + month;
		}
		return day + '.' + month + '.' + year;
	}
	
	/**
	 * Method for getting the system time.
	 * 
	 * @return system time as string
	 */
	public static String getTimeString() {
		final Calendar calendar = Calendar.getInstance();
		String hour = Integer.toString(calendar.get(Calendar.HOUR_OF_DAY));
		String min = Integer.toString(calendar.get(Calendar.MINUTE));
		String sec = Integer.toString(calendar.get(Calendar.SECOND));
		if (hour.length() < 2) {
			hour = '0' + hour;
		}
		if (min.length() < 2) {
			min = '0' + min;
		}
		if (sec.length() < 2) {
			sec = '0' + sec;
		}
		return hour+':'+min+':'+sec;
	}
	
	/**
	 * Method for getting the system date and time.
	 * 
	 * @return system date and time as string
	 */

	public static String getDateAndTimeString() {
		return getDateString()+' '+getTimeString();
	}

}
