package model;

import java.io.File;
import java.io.FilenameFilter;

import java.util.HashMap;
import java.util.ArrayList;

import model.exceptions.DataFileException;
import model.exceptions.EntryNotFoundException;
import model.exceptions.GoException;
import model.exceptions.GoIOException;
import model.exceptions.TournamentNotFoundException;

import settings.Settings;

/**
 * Class is used to manage the tournaments in the program and to provide
 * an interface for the user interface.
 * 
 * This is a static class.
 *
 */
public class TournamentManager {

	private static HashMap<ID, Tournament> open_tournaments = new HashMap<ID, Tournament>();
	private static HashMap<ID, Tournament> closed_tournaments = new HashMap<ID, Tournament>();

	/**
	 * Reads the tournament data files from the data folder.
	 * 
	 * Throws an exception with the pathnames of the files that cannot be read.
	 * 
	 * @throws GoException thrown if the there are problems reading the data files
	 */
	public static void readTournamentDataFromDisk() throws GoException { 
		//for storing data files that can't be parsed
		final ArrayList<File> error_files = new ArrayList<File>();
		
		//read XML data files from disk
		final File[] data_files = Settings.getDataFolder().listFiles(new DataFileFilter());

		for (File f : data_files) {
			try { //try to create an XMLFile object from XML data file
				final Tournament loaded_tournament = new Tournament(new XMLFile(f.getAbsolutePath()));
				if (loaded_tournament.isTournamentOpen()) {
					open_tournaments.put(loaded_tournament.getId(), loaded_tournament);
				} else {
					closed_tournaments.put(loaded_tournament.getId(), loaded_tournament);
				}
			} catch (DataFileException dfe) {
				error_files.add(f);
			}
		}

		//if some data XML files could not be parsed throw an exception
		//with a message that contains all the pathnames of the problem files
		if (!error_files.isEmpty()) {
			String all_paths = "";
			for (File f : error_files)
				all_paths += f.getAbsolutePath() + " ";
			throw new DataFileException(all_paths);
		}
			
	}
		
	/**
	 *	Returns a TournamentInitializer object that can be used to create a new tournament. 
	 * 
	 * @return tournament initializer
	 * @throws GoException
	 */
	public static TournamentInitializer getTournamentInitializer() throws GoException { //GoIOException on GoExceptionin aliluokka
		return new TournamentInitializer(Settings.getNextTournamentId());
	}

	public static TournamentUpdater getTournamentUpdater(final ID tournament_id) 
	throws GoException {
		return getOpenTournament(tournament_id).getTournamentUpdater();
	}
	
	public static EntryInitializer getEntryInitializer(final ID tournament_id) 
	throws GoException {
		return getOpenTournament(tournament_id).getEntryInitializer();
	}

	public static EntryInitializer getPreEntryInitializer(final ID tournament_id) 
	throws GoException {
		return getOpenTournament(tournament_id).getPreEntryInitializer();
	}

	public static EntryUpdater getEntryUpdater(final ID tournament_id, final ID entry_id) 
	throws GoException {
		return getOpenTournament(tournament_id).getEntryUpdater(entry_id);
	}
	
	public static EntryUpdater getPreEntryUpdater(final ID tournament_id, final ID entry_id) 
	throws GoException {
		return getOpenTournament(tournament_id).getPreEntryUpdater(entry_id);
	}

	public static synchronized void deleteEntry(final ID tournament_id, final ID entry_id) 
	throws TournamentNotFoundException, EntryNotFoundException, GoIOException {
		getOpenTournament(tournament_id).deleteEntry(entry_id);
	}
		
	public static synchronized void closeTournament(final ID tournament_id) 
	throws TournamentNotFoundException, GoIOException {
		final Tournament to_close = getOpenTournament(tournament_id);
		to_close.closeTournament();
		open_tournaments.remove(tournament_id);		
		closed_tournaments.put(tournament_id, to_close);		
	}

	public static synchronized void reopenTournament(final ID tournament_id) 
	throws TournamentNotFoundException, GoIOException {
		final Tournament to_reopen = getClosedTournament(tournament_id);
		to_reopen.reopenTournament();
		closed_tournaments.remove(tournament_id);		
		open_tournaments.put(tournament_id, to_reopen);		
	}
		
	public static synchronized void deleteTournament(final ID tournament_id) 
	throws GoException { //TournamentOpenException ja TournamentNotFoundException sisältyvät jo GoExceptioniin
		final Tournament to_delete = getClosedTournament(tournament_id);
		closed_tournaments.remove(tournament_id);
		to_delete.delete();
		final long mem0 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
		System.runFinalization();
		System.gc();
		final long mem1 = Runtime.getRuntime().totalMemory() -
	      Runtime.getRuntime().freeMemory();
		final long erotus = mem0-mem1;
		System.out.println("TournamentManager.java: GC: "+erotus+" bytes RAM freed.");

	}

	// Tämä javadoc  pitää päivittää!!!
	/**
	 * Creates a new tournament data file and adds tournament 
	 * header information to the tournament list file (tournaments.xml)
	 * 
	 * (tournament id number and filename are generated automatically)
	 * 
	 * @throws GoException 
	 */
	public static synchronized Tournament addTournament(final Tournament new_tournament) throws GoException {
		if (open_tournaments.containsKey(new_tournament.getId())) {
			throw new GoException("Error adding new tournament, tournament with the same id "+new_tournament.getId()+" already exist!");
		}
		return open_tournaments.put(new_tournament.getId(), new_tournament);
	}

	public static synchronized Tournament getTournament(final ID tournament_id) 
	throws TournamentNotFoundException {
		if (open_tournaments.containsKey(tournament_id)) {
			return open_tournaments.get(tournament_id);
		}
		if (closed_tournaments.containsKey(tournament_id)) {
			return closed_tournaments.get(tournament_id);
		}
		
		throw new TournamentNotFoundException(tournament_id);
}
	/**
	 * Returns an open tournament with the given tournament id number.
	 */
	public static synchronized Tournament getOpenTournament(final ID tournament_id) 
		throws TournamentNotFoundException {
			if(!open_tournaments.containsKey(tournament_id)) {
				throw new TournamentNotFoundException(tournament_id,
				"It might have been closed.");
			}
		return open_tournaments.get(tournament_id);
	}

	/**
	 * Returns a closed tournament with the given tournament id number.
	 */
	public static synchronized Tournament getClosedTournament(final ID tournament_id) 
		throws TournamentNotFoundException {
			if(!closed_tournaments.containsKey(tournament_id)) {
				throw new TournamentNotFoundException(tournament_id,
						"It might have been deleted or reopened.");
			}
		return closed_tournaments.get(tournament_id);
	}

	public static synchronized Tournament[] getOpenTournaments() {
		return open_tournaments.values().toArray(new Tournament[0]);
	}
	
	public static synchronized Tournament[] getClosedTournaments() {
		return closed_tournaments.values().toArray(new Tournament[0]);
	}

	private static class DataFileFilter implements FilenameFilter {

		@Override
		public boolean accept(final File folder, final String filename) {
			if (filename.startsWith(Settings.TOURNAMENT_FILENAME_PREFIX) 
			&& filename.endsWith(Settings.DATA_FILE_SUFFIX)) {
				return true;
			}
			return false;
		}
		
	}
}
