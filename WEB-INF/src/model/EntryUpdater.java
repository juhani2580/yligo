package model;

import model.entry.EntryDataModelEditor;
import model.exceptions.GoException;

/**
 * EntryUpdater is used to create a new Tournament object.
 * 
 * When the run-method is called the Entry object that is connected to the
 * instance of this class is updated with the data in this class.
 * 
 * A subclass of EntryDataModelEditor.
 */

public class EntryUpdater extends EntryDataModelEditor {

	private final transient Tournament TOURNAMENT;
	
	protected EntryUpdater(final Tournament tournament, final Entry target_entry) throws GoException {
		super(target_entry.getId());
		this.TOURNAMENT = tournament;
		//set default values
		this.setLastName(target_entry.getLastName());
		this.setFirstName(target_entry.getFirstName());
		this.setRank(target_entry.getRank());
		this.setClub(target_entry.getClub());
		this.setCountry(target_entry.getCountry());
		this.setTotalFee(target_entry.getTotalFee());
		this.setPaidSum(target_entry.getPaidSum());
		this.setInfo(target_entry.getInfo());
		this.setDateOfBirth(target_entry.getDateOfBirth());
		for (ID id : target_entry.getDiscountIds())
			this.setDiscountState(id, true);
	}
	
	/**
	 * Edits the tournament linked to this instance of class TournamentUpdater
	 * with the data in this instance.
	 */
	@Override
	public void run() throws GoException {
		this.TOURNAMENT.editEntry(this.getId(), this);
	}
}
