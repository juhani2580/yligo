package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import model.exceptions.DataFileException;
import model.exceptions.GoException;
import model.exceptions.GoIOException;
import model.exceptions.XMLParsingException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import settings.Settings;

/**
 * The class is used to store the data contained in a Tournament object
 * to the file system in XML format. 
 * 
 * The XML document is maintained in memory and when it is altered the
 * changes are saved to disk.
 *
 */

public class XMLFile extends File {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -4226809799635493673L;

	private transient Document doc;
	
	private transient Element header;

	private transient Element element_id;
	private transient Element name;
	private transient Element html;
	private transient Element default_country;
	private transient Element date;
	private transient Element base_fee;
	private transient Element preregistration_open;
	private transient Element tournament_open;
	
	private transient Element discounts;

	private transient Element entry_id_counter;
	private transient Element discount_id_counter;
	
	private transient Element entries;
	private transient Element pre_entries;
	private transient Element confirmed_pre_entries;

	/**
	 * Create a new XMLFile object based on a XML data file on disk
	 * 
	 * @param pathname absolute pathname of the XML file to be read
	 * @throws DataFileException  thrown if the XML data file is not found
	 * @throws XMLParsingException thrown if the data file cannot be parsed as an XML document
	 */
	protected XMLFile(final String pathname) throws DataFileException, XMLParsingException {
		super(pathname);
		//create Document object for XMLDoc object
		if (this.exists()) {
			//try to create a XML data Document object from the XML file
			this.doc = XMLFactory.createXMLDataDocument(this);
		} else {
			//tournament XML data file with given pathname not found
			throw new DataFileException(pathname);
		}
		this.setElementReferences();
	}


	
	
	/**
	 * Creates a completely new tournament XML data file with data from the initializer.
	 * 
	 * @param pathname absolute pathname of the XMLFile to be created
	 * @throws GoException thrown if the XMLFile cannot be created or the initializer cannot be read
	 */
	protected XMLFile(final String pathname, final TournamentInitializer initializer) throws GoException {
		super(pathname);
		//create new XML data document 
		this.doc = XMLFactory.createXMLDataDocument();
		this.setElementReferences();
		this.element_id.setTextContent(initializer.getId().toString());
		this.name.setTextContent(initializer.getName());
		this.html.setTextContent(initializer.getHtml());
		this.default_country.setTextContent(initializer.getDefaultCountry().toString());
		this.base_fee.setTextContent(initializer.getBaseFee().toString());
		this.date.setTextContent(initializer.getDate().toString());
		this.preregistration_open.setTextContent(Boolean.toString(initializer.isPreRegistrationOpen()));
		this.tournament_open.setTextContent(Boolean.toString(initializer.isTournamentOpen()));
		this.discount_id_counter.setTextContent(Integer.toString(initializer.getDiscounts().length));
		for (Discount discount : initializer.getDiscounts()) {
			this.discounts.appendChild(XMLFactory.createDiscountElement(this.doc, discount));
		}
		//save the created XML doc to disk
		this.write();
	}

	/**
	 * Creates a pathname string and calls the constructor XMLFile(final String pathname)
	 */
	protected XMLFile(final File folder, final String filename) throws DataFileException, XMLParsingException {
		this(folder.getAbsolutePath()+File.separator+filename);
	}
	
	/**
	 * Creates a pathname string and calls the constructor XMLFile(final String pathname, final TournamentInitializer initializer)
	 */
	protected XMLFile(final File folder, final String filename, final TournamentInitializer initializer) throws GoException {
		this(folder.getAbsolutePath()+File.separator+filename, initializer);
	}

	/**
	 * Sets the fields of the class to point to different elements of the XML document for convenience
	 */
	private void setElementReferences() {
		//set XMLDoc element references
		this.header = (Element)this.doc.getElementsByTagName("header").item(0);

		this.element_id = (Element)this.header.getElementsByTagName("id").item(0);
		this.name = (Element)this.header.getElementsByTagName("name").item(0);
		this.html = (Element)this.header.getElementsByTagName("html").item(0);
		this.default_country = (Element)this.header.getElementsByTagName("defaultCountry").item(0);
		this.date = (Element)this.header.getElementsByTagName("date").item(0);
		this.base_fee = (Element)this.header.getElementsByTagName("baseFee").item(0);
		this.preregistration_open = (Element)this.header.getElementsByTagName("preregistrationOpen").item(0);
		this.tournament_open = (Element)this.header.getElementsByTagName("tournamentOpen").item(0);
	
		this.discounts = (Element)this.header.getElementsByTagName("discounts").item(0);

		this.entry_id_counter = (Element)this.header.getElementsByTagName("entryIdCounter").item(0);
		this.discount_id_counter = (Element)this.header.getElementsByTagName("discountIdCounter").item(0);
		
		this.entries = (Element)this.doc.getDocumentElement().getElementsByTagName("entries").item(0);
		this.pre_entries = (Element)this.doc.getDocumentElement().getElementsByTagName("preEntries").item(0);
		this.confirmed_pre_entries = (Element)this.doc.getDocumentElement().getElementsByTagName("confirmedPreEntries").item(0);
		
		//normalize document
		this.doc.getDocumentElement().normalize();
	}

	/**
	 * @param entry_id ID
	 * @return entry XML element with given ID
	 */
	private Element getEntryElement(final ID entry_id) {
		return getEntryElement(entry_id, this.doc.getDocumentElement());
	}

	/**
	 * @param entry_id ID
	 * @param parent_elem
	 * @return entry XML element with given ID under parent element
	 */
	private Element getEntryElement(final ID entry_id, final Element parent_elem) {
		// 1. iterate through the entry elements
		for (Element entry_elem : XMLFactory.getSubElements(parent_elem, "entry")) {
			// 1.1 get the id of the current entry element and compare it to given entry id parameter
			final String curr_id = XMLFactory.getSubElementText(entry_elem, "id");
			if (curr_id.equals(entry_id.toString())) {
				// 1.2 if a match is found return current element
				return entry_elem;
			}
		}
		return null;
	}

	/** 
	 * @return ID of the tournament
	 */
	protected ID getTournamentId() {
		return new ID(this.element_id.getTextContent());
	}

	/**
	 * 
	 * @return a new TournamentInitializer object based on the data in the XML document
	 * @throws GoException if there is a problem with the initializer
	 */
	protected TournamentInitializer getTournamentInitializer() throws GoException { //DOMException on unchecked, ei tarvitse heittää
		//create new TournamentInitializer object
		final TournamentInitializer initializer = new TournamentInitializer(this.getTournamentId());
		//set initializer fields
		initializer.setName(this.name.getTextContent());
		initializer.setHtml(this.html.getTextContent());
		initializer.setDefaultCountry(this.default_country.getTextContent());
		initializer.setDate(this.date.getTextContent());
		initializer.setBaseFee(this.base_fee.getTextContent());
		initializer.setPreregistrationOpen(Boolean.parseBoolean(this.preregistration_open.getTextContent()));
		initializer.setTournamentOpen(Boolean.parseBoolean(this.tournament_open.getTextContent()));
		//set initializer discounts
		final NodeList discount_list = this.discounts.getElementsByTagName("discount");
		for (int i = 0; i < discount_list.getLength(); ++i) { 
			final Element discount_elem = (Element)discount_list.item(i);
			final ID discount_id = new ID(XMLFactory.getSubElementText(discount_elem, "discountId"));
			final String discount_name = XMLFactory.getSubElementText(discount_elem, "name");
			final BigDecimal amount = new BigDecimal(XMLFactory.getSubElementText(discount_elem, "amount"));
			final String dominant = XMLFactory.getSubElementText(discount_elem, "dominant");
			initializer.addDiscount(new Discount(discount_id, discount_name,  amount, "true".equals(dominant)));
		}
		
		return initializer;
	}
	
	/**
	 * Sets the tournament as closed in the XML data file
	 * @throws GoIOException if write operation fails
	 */
	protected void closeTournament() throws GoIOException {
		this.tournament_open.setTextContent("false");
		this.write();
	}
	
	/**
	 * Sets the tournament as open in the XML data file
	 * @throws GoIOException if write operation fails
	 */
	protected void openTournament() throws GoIOException {
		this.tournament_open.setTextContent("true");
		this.write();
	}

	/**
	 * @return next available entry ID of the tournament
	 */
	protected ID getEntryIdCounter() {
		return new ID(this.entry_id_counter.getTextContent());
	}

	/**
	 * @return next available discount ID of the tournament
	 */
	protected ID getDiscountIdCounter() {
		return new ID(this.discount_id_counter.getTextContent());
	}

	/**
	 * Updates the entry ID counter of the tournament
	 * 
	 * @param increment by how much the counter is incremented
	 * @return next available entry ID of the tournament
	 * @throws GoIOException if the write operation fails
	 */
	protected ID incrementEntryIdCounter(final int increment) throws GoIOException {
		//get current counter value
		String id_counter_str = this.entry_id_counter.getTextContent();
		//calculate incremented counter value
		final long incremented_value = Long.parseLong(id_counter_str) + increment;
		//increment counter
		this.entry_id_counter.setTextContent(Long.toString(incremented_value));
		//save to disk
		this.write();
		return new ID(incremented_value);
	}

	/**
	 * Updates the discount ID counter of the tournament
	 * 
	 * @param increment by how much the counter is incremented
	 * @return next available discount ID of the tournament
	 * @throws GoIOException if the write operation fails
	 */
	protected ID incrementDiscountIdCounter(final int increment) throws GoIOException {
		//get current counter value
		final String id_counter_str = this.discount_id_counter.getTextContent();
		//calculate incremented counter value
		final long incremented_value = Long.parseLong(id_counter_str) + increment;
		//increment counter
		this.discount_id_counter.setTextContent(Long.toString(incremented_value));
		this.write();
		return new ID(incremented_value);
	}
	
	/**
	 * Adds a new entry element to XML document as a child of element 'preEntries'
	 * with the data from the entry.
	 * 
	 * @param preentries the pre-entries to be added
	 * @throws GoIOException if the write operation fails
	 */
	protected void addPreEntry(final Entry... preentries) throws GoIOException {
		for (Entry entry : preentries)
			this.pre_entries.appendChild(XMLFactory.createEntryElement(this.doc, entry));
		this.write();
	}

	/**
	 * Adds a new entry element to XML document as a child of element 'entries'
	 * with the data from the entry.
	 * 
	 * @param entriestoadd the entries to be added
	 * @throws GoIOException if the write operation fails
	 */
	protected void addEntry(final Entry... entriestoadd) throws GoIOException {
		for (Entry entry : entriestoadd)
			this.entries.appendChild(XMLFactory.createEntryElement(this.doc, entry));
		this.write();
	}
	
	/**
	 * Returns an Entry-array of all the entries in the tournament file.
	 * @throws GoException 
	 * 
	 */
	protected ArrayList<Entry> getEntries(final Tournament tournament) throws GoException {
		final ArrayList<Entry> local_entries = new ArrayList<Entry>();
		for (Element entry_elem : XMLFactory.getSubElements(this.entries, "entry")) {
			final ID entry_id = new ID(XMLFactory.getSubElementText(entry_elem, "id"));
			final EntryInitializer initializer =  new EntryInitializer(tournament, entry_id, false);
			local_entries.add(XMLFactory.parseEntry(entry_elem, initializer));
		}
		return local_entries;
	}

	/**
	 * Returns an Entry-array of all the pre-entries in the tournament file.
	 * @throws GoException 
	 */
	protected ArrayList<Entry> getPreEntries(final Tournament tournament) throws GoException {
		final ArrayList<Entry> pre_entries1 = new ArrayList<Entry>();
		for (Element entry_elem : XMLFactory.getSubElements(this.pre_entries, "entry")) {
			final ID entry_id = new ID(XMLFactory.getSubElementText(entry_elem, "id"));
			final EntryInitializer initializer =  new EntryInitializer(tournament, entry_id, false);
			pre_entries1.add(XMLFactory.parseEntry(entry_elem, initializer));
		}
		return pre_entries1;
	}

	/**
	 * Returns an Entry-array of all the confirmed pre-entries in the tournament
	 * file.
	 * @throws GoException 
	 */
	protected ArrayList<Entry> getConfirmedPreEntries(final Tournament tournament) throws GoException {
		final ArrayList<Entry> local_confirmed_pre_entries = new ArrayList<Entry>();
		for (Element entry_elem : XMLFactory.getSubElements(this.confirmed_pre_entries, "entry")) {
			final ID entry_id = new ID(XMLFactory.getSubElementText(entry_elem, "id"));
			final EntryInitializer initializer =  new EntryInitializer(tournament, entry_id, false);
			local_confirmed_pre_entries.add(XMLFactory.parseEntry(entry_elem, initializer));
		}
		return local_confirmed_pre_entries;
	}
	
	/**
	 * Moves the pre-entry with the given ID under the element 'confirmedPreEntries'
	 */
	protected void confirmPreEntry(final ID entry_id) throws GoIOException {
		final Element confirmed_pre_entry = deleteEntry(entry_id, this.pre_entries);
		this.confirmed_pre_entries.appendChild(confirmed_pre_entry);
		this.write();
	}

	/**
	 * Updates the tournament information and discounts.
	 */
	protected void updateHeader(final TournamentUpdater updater) throws GoIOException {
		//1. set tournament id (only if it is not yet set)
		if (this.element_id.getTextContent().equals("")) {
			this.element_id.setTextContent(updater.getTournament().getId().toString());
		}
		// 2. set tournament name
		this.name.setTextContent(updater.getName());
		// 3. set tournament html
		this.html.setTextContent(updater.getHtml());
		// 4. set tournament default country
		this.default_country.setTextContent(updater.getDefaultCountry().toString());
		// 5. set tournament base fee
		this.base_fee.setTextContent(updater.getBaseFee().toString());
		// 6. set tournament base fee
		this.date.setTextContent(updater.getDate().toString());
		// 7. set tournament base fee
		this.preregistration_open.setTextContent(Boolean.toString(updater.isPreRegistrationOpen()));
		// 8. set tournament discounts
		//8.1 replace old "discounts"-element
		final Element elem_new_discounts = this.doc.createElement("discounts");
		this.header.replaceChild(elem_new_discounts, this.discounts);
		this.discounts = elem_new_discounts;
		//8.2 create new discount element for every Discount object in initializer
		for (Discount discount : updater.getDiscounts()) {
			this.discounts.appendChild(XMLFactory.createDiscountElement(this.doc, discount));
		}
		this.write();
	}

	
	/**
	 * Deletes an entry element that is a descendant of 'entries'
	 */
	protected void deleteEntry(final ID entry_id) throws GoIOException {
		this.deleteEntry(entry_id, this.entries);
		this.write();
	}

	/**
	 * Deletes an entry element that is a descendant of 'preEntries'
	 */
	protected void deletePreEntry(final ID entry_id) throws GoIOException {
		this.deleteEntry(entry_id, this.pre_entries);
		this.write();
	}

	/**
	 * Deletes an entry element that is a descendant of 'parent_elem'
	 */
	private Element deleteEntry(final ID entry_id, final Element parent_elem) {
		final Element deleted_entry_elem = getEntryElement(entry_id, parent_elem);
		if (deleted_entry_elem == null) {
			return null;
		}
		XMLFactory.removeElement(deleted_entry_elem);
		return deleted_entry_elem;
	}

	/**
	 * Updates the entry element with the ID 'entry_id' with the data from the updater
	 */
	protected void updateEntry(final ID entry_id, final EntryUpdater updater) throws GoIOException {
		//get entry element to be updated
		final Element update_elem = getEntryElement(entry_id);
		//update entry element fields
		XMLFactory.setSubElementText(update_elem, updater.getLastName(), "lastName");
		XMLFactory.setSubElementText(update_elem, updater.getFirstName(), "firstName");
		XMLFactory.setSubElementText(update_elem, updater.getRank().toString(), "rank");
		XMLFactory.setSubElementText(update_elem, updater.getClub(), "club");
		XMLFactory.setSubElementText(update_elem, updater.getCountry().getCode(), "country");
		XMLFactory.setSubElementText(update_elem, updater.getTotalFee().toString(), "totalFee");
		XMLFactory.setSubElementText(update_elem, updater.getPaidSum().toString(), "paid");
		// update discounts: remove all and add new ones
		for (Element old_id_elem : XMLFactory.getSubElements(update_elem, "discount_id"))
			XMLFactory.removeElement(old_id_elem);
		for (ID local_id : updater.getDiscountIds()) {
			final Element elem_discount = this.doc.createElement("discount_id");
			elem_discount.setTextContent(local_id.toString());
			update_elem.appendChild(elem_discount);
		}
		this.write();
	}
	
	/**
	 * Writes the XML document to disk.
	 * 
	 * A backup copy is made to the folder given by the method 
	 * Settings.getBackupFolder.
	 */
	protected void write() throws GoIOException {
		//backup operation
		for (int i = Settings.BACKUP_FILE_AMOUNT-2; i >= 0; --i) {
			final File backup_file = new File(Settings.getBackupFolder(), this.getName()+"_"+i);
			if (backup_file.exists()) {
				copyFile(backup_file, new File(Settings.getBackupFolder(), this.getName()+"_"+(i+1)));
			}
		}
		if (this.exists()) {
			copyFile(this, new File(Settings.getBackupFolder(), this.getName()+"_0"));
		}			
		//write operation
		try {
            // 1. use Transformer(Factory) to convert XML doc to String:
            final TransformerFactory trans_factory = TransformerFactory.newInstance();
            final Transformer trans = trans_factory.newTransformer();
            trans.setOutputProperty(OutputKeys.ENCODING, Settings.CHAR_ENCODING);
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            final StringWriter stringwriter = new StringWriter();
            final StreamResult result = new StreamResult(stringwriter);
            final DOMSource source = new DOMSource(this.doc);
            trans.transform(source, result);
            final String xml_string = stringwriter.toString();


            // 2. save the string to tournament file:
            OutputStream file_out;
            byte[] buffer = xml_string.getBytes(Settings.CHAR_ENCODING);
            file_out = new FileOutputStream(this);
            for (int i = 0; i < buffer.length; ++i) {
                    file_out.write(buffer[i]);
            }
            // 3. close buffer and stream, free allocated memory:
            file_out.close();
            buffer = null;
            System.runFinalization();
            System.gc();
            
		} catch (TransformerException tfe) { 
			throw new GoIOException(this.getAbsolutePath());
		} catch (UnsupportedEncodingException e1) {
			throw new GoIOException(this.getAbsolutePath());
		} catch (FileNotFoundException e2) {
			throw new GoIOException(this.getAbsolutePath());
		} catch (IOException e1) {
			throw new GoIOException(this.getAbsolutePath());
		}		

	}
	
	/**
	 * Copies a file. Used for creating backups of the XML data file.
	 */
	private static void copyFile(final File from_file, final File to_file) {
		FileInputStream from_stream = null;
	    FileOutputStream to_stream = null;
	    try {
	      from_stream = new FileInputStream(from_file);
	      to_stream = new FileOutputStream(to_file);
	      final byte[] buffer = new byte[65536];
	      int bytesRead;
	      while ((bytesRead = from_stream.read(buffer)) != -1) { //should this be 0?
	        to_stream.write(buffer, 0, bytesRead); // write
	      }
	    } catch(Exception e) {
    		System.out.println("XMLFile.java: error copying files "+e+
    				" "+from_file.getAbsolutePath()+ ", "+to_file.getAbsolutePath());
	    	
	    } finally {
	    	if(from_stream != null && to_stream != null) {
	    	try {
	    		
	    		from_stream.close();
	    		to_stream.close();
	    		}
	    	 catch(Exception e) {
	    		System.out.println("XMLFile.java: error closing file streams "+e);
	    	}}
	    	else
	    	{
	    		System.out.println("XMLFile.java: from_stream and/or to_stream is NULL: can't handle the file stream(s)");	
	    	}
	    }  		
	}	
	
	protected void deleteBackupFiles() {
		final File[] backup_files = Settings.getBackupFolder().listFiles(new BackupDataFileFilter(this.getName()));
		for (File f : backup_files) {
			f.delete();
		}
	}
	
	private static class BackupDataFileFilter implements FilenameFilter {

		private final transient String data_file_name;
		
		protected BackupDataFileFilter(final String backup_data_file_name) {
			this.data_file_name = backup_data_file_name;
		}
		
		@Override
		public boolean accept(final File folder, final String filename) {
			if (filename.startsWith(this.data_file_name)) {
				return true;
			}
			return false;
		}
		
	}


}
