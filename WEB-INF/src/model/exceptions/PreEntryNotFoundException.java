package model.exceptions;

import model.ID;

public class PreEntryNotFoundException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = 4870733430837258713L;

	public PreEntryNotFoundException(final ID entry_id) {
		super("Pre-entry with id " + entry_id + " not found! \n" +
				"It might have been confirmed.");
	}

}
