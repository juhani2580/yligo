package model.exceptions;

import model.ID;

public class EntryNotFoundException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -1749598220492447363L;

	public EntryNotFoundException(final ID entry_id) {
		super("Entry with id " + entry_id + " not found!");
	}

}
