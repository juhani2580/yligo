package model.exceptions;

/**
 * Base class of the exceptions used in the program.
 * All the other exception classes are subclasses of GoException.
 * 
 *
 */
public class GoException extends Exception {

	/**
	 * UID for serialization
	 */
	private static final transient long serialVersionUID = -8952738567177824241L;
	transient String error_message;
	
	/**
	 * When a GoException instance is created, its error message is stored.
	 */
	public GoException(final String exc_error_message) {
		super();
		this.error_message = exc_error_message;
	}
	
	/**
	 * Returns the error message of the exception.
	 */
	public String getErrorMessage() {
		return this.error_message;
	}
	
	/**
	 * Returns the error message of the exception.
	 */
	@Override
	public String toString() {
		return this.error_message;
	}
}
