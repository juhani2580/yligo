package model.exceptions;

public class TournamentOpenException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -7569292672397135738L;

	public TournamentOpenException(final String operation) {
		super("Tournament is open. " + operation + " operation can't be performed.");
		
	}

}
