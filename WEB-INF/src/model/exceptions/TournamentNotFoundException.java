package model.exceptions;

import model.ID;

public class TournamentNotFoundException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -2126691340719457524L;

	public TournamentNotFoundException(final ID tournament_id) {
		super("Tournament with id " + tournament_id + " not found!");
	}

	public TournamentNotFoundException(final ID tournament_id, final String extra_info) {
		super("Tournament with id " + tournament_id + " not found! \n" + extra_info);
	}
}
