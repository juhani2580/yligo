package model.exceptions;

import model.Entry;

public class DuplicatePlayerException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = 9075390606270253347L;

	public DuplicatePlayerException(final Entry existing_entry) {
		super("Player with the same name '" + existing_entry.getFirstName() + 
				' '+ existing_entry.getLastName()+ "' already exists!");
	}

}
