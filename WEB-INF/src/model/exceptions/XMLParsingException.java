package model.exceptions;

import settings.Settings;

public class XMLParsingException extends GoException {

	public XMLParsingException(String pathname) {
		super("XML data file " + pathname + " cannot be parsed, there is probably a problem with the XML document structure. \n" +
				"Please try to use a backup from the backup folder "+Settings.getBackupFolder().getAbsolutePath());
	}

}
