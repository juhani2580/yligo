package model.exceptions;


public class DataFileException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -6525000299767883317L;

	public DataFileException(final String pathname) {
		super("Problem reading XML data file(s) " + pathname);
	}

}
