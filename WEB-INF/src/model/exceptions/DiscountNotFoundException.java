package model.exceptions;

import model.ID;

public class DiscountNotFoundException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -2787650613868431097L;

	public DiscountNotFoundException(final ID discount_id) {
		super("Discount with ID " + discount_id + " not found!");
	}
}
