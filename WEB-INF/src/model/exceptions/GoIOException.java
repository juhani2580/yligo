package model.exceptions;


public class GoIOException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -768183919390432429L;

	public GoIOException(final String pathname) {
		super("Problem with accessing file " + pathname);
	}
}
