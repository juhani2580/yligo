package model.exceptions;

public class EntryDateNotSetException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = 2233189053030629160L;

	public EntryDateNotSetException() {
		super("Entry date not set.");
	}
}
