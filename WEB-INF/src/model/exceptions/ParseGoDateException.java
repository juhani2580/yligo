package model.exceptions;

public class ParseGoDateException extends GoException {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = 237943436632365242L;

	public ParseGoDateException(final String date_string) {
		super("String '" + date_string + "' cannot be parsed as a date.\n" +
			 "Please enter the date in the format dd.MM.yyyy, as in 31.12.2000");
	}

}
