package model.exceptions;


public class TournamentClosedException extends GoException {
	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = 8594304503018001764L;

	public TournamentClosedException() {
		super("The tournament is closed, cannot perform operation.\n" + 
				"Reopen the tournament to in order to make changes.");	
	}
	//TODO: toteutus
//
//	public TournamentClosedException(ID tournament_id, String extra_info) {
//		super("Tournament with id " + tournament_id + " not found! \n" + extra_info);
//	}
}
