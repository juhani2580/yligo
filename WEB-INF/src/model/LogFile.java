package model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import model.exceptions.GoException;

import settings.Settings;

/**
 * Class LogFile is used to store change events of the tournament
 * (name changes, new entries etc.) in a simple text file.
 *
 */
public class LogFile extends File {

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = 5264057277690121211L;

	/**
	 * Creates a log file in the public data folder specified in Settings.
	 * 
	 * @param pathname name of the log file
	 */

	protected LogFile(final String pathname) {
		super(Settings.getPublicFolder(), pathname);
	}

	/**
	 * Method writes a new text row in the log file.
	 * 
	 * @param message the string to be written
	 */
	protected void writeMessage(final String message) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(this, true));
			out.write(message+"\n");
			out.close();
		} catch(IOException e) {
			System.out.println("LogFile.java: error writing to log file "+this.getAbsolutePath());
		}
	}
	
	/**
	 * Returns the contents of the log file 
	 * 
	 * @return a String representation of the log file contents
	 * @throws GoException thrown if the log file cannot be read
	 */
	protected String getLogData() throws GoException {
		String log = "";
		try {
			final Scanner scanner = new Scanner(this);
			while (scanner.hasNextLine()) {
				log += scanner.nextLine() + "\n";
			}
			scanner.close();
		} catch (Exception e) {
			throw new GoException("Log file " + this.getAbsolutePath() + " cannot be read!");
		}
		return log;
	}
}
