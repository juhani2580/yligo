package model;

import java.math.BigDecimal;

import model.exceptions.GoException;
import model.tournament.TournamentDataModelEditor;

/**
 * TournamentInitializer is used to create a new Tournament object.
 * 
 * When the run-method is called the a new Tournament object is created
 * with the data in the initializer and added to the TournamentManager.
 * 
 * A subclass of TournamentDataModelEditor.
 */
public class TournamentInitializer extends TournamentDataModelEditor {
	
	/**
	 * Creates a new initializer with the given ID object.
	 */
	public TournamentInitializer(final ID tournament_id) throws GoException {
		super(tournament_id);
		//default values:
		this.setName("unknown");
		this.setHtml("");
		this.setDefaultCountry(Country.getUnknownCountry());
		this.setBaseFee(BigDecimal.ZERO);
		this.setDate(GoDate.getDateString());
		this.setPreregistrationOpen(false);
		this.setTournamentOpen(true);//tournament is by default open when created
	}

	/**
	 * An implementation of the abstract method in superclass TournamentDataModel.
	 * Implemented here so that the package 'model' can see the method.
	 * 
	 * Adds a Discount object to the data structure.
	 */
	@Override
	protected void addDiscount(final Discount discount) {
		super.addDiscount(discount);
	}

	/**
	 * An implementation of the abstract method in superclass TournamentDataModel.
	 * Implemented here so that the method has public visibility.
	 * 
	 * Creates a new Discount object from the parameters and adds it 
	 * to the data structure.
	 */
	@Override
	public void addDiscount(final String name, final String amount, final boolean dominant) {
		this.addDiscount(new Discount(new ID(this.getDiscounts().length), name, new BigDecimal(amount), dominant));
	}
	
	/**
	 * An implementation of the abstract method in superclass TournamentDataModel.
	 * Implemented here so that the method has public visibility.
	 * 
	 * When the method is called the a new Tournament object is created
	 * with the data in this TournamentInitializer instance and added 
	 * to the TournamentManager.
	 */
	@Override
	public void run() throws GoException {
		TournamentManager.addTournament(new Tournament(this));
	}
}
