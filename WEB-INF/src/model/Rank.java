package model;

import java.util.ArrayList;

import model.exceptions.GoException;

/**
 * A class for representing Go ranks (7..1 dan, 1..30 kyu)
 * 
 */
public class Rank implements Comparable<Object> {
	
	private static Rank[] rank_list;
	
	private final transient String id;
	private final transient int value;
	private final transient boolean is_dan;
	
	/**
	 * Static initialization block creates the rank list when Rank class
	 * is first called.
	 */
	static {
		//create rank list
		final ArrayList<Rank> ranks = new ArrayList<Rank>();
		for(int i = 7; i >= 1; i--) {
			ranks.add(new Rank(i, true));
		}
		for(int i = 1; i <= 30; i++) {
			ranks.add(new Rank(i, false));
		}
		
		rank_list = new Rank[ranks.size()];
		for(int i = 0; i < rank_list.length; i++) {
			rank_list[i] = ranks.get(i);
		}
			
	}
	
	/**
	 * Constructor creates a new Rank object that represents a Go rank.
	 * 
	 * 
	 * @param rank_value rank value
	 * @param rank_dan true if dan, false if kyu.
	 */
	protected Rank(final int rank_value, final boolean rank_dan) {
		this.id = (rank_dan ? "d" : "k") + rank_value;
		this.value = rank_value;
		this.is_dan = rank_dan;
	}
	
	/**
	 * Returns the rank id string. The string is of the same format
	 * as in the method toString.
	 * 
	 * @return rank id
	 */
	public final String getId() {
		return this.id;
	}
	
	/**
	 * Returns the value of the rank.
	 * @return rank value, i.e. the numeric part of the rank
	 */
	public final int getValue() {
		return this.value;
	}
	
	/**
	 * Returns true if dan, false if kyu.
	 * 
	 * @return true if dan, false if kyu
	 */
	public final boolean isDan() {
		return this.is_dan;
	}
	
	/**
	 * Returns a String that is created as follows: 
	 * 
	 * 1. the first character is "d" if the rank is dan and "k" if kyu.
	 * 2. rest of the string is the numeral value of the rank.
	 */
	@Override
	public final String toString() {
		return this.id;
	}
	
	/**
	 * Returns a more readable String representation of the rank.
	 * 
	 * @return	a string with the numeric value of the rank followed by 
	 * a space and "kyu" or "dan" rank indicators.
	 */
	public final String getRepresentation() {
		return this.value + " " + (this.is_dan ? "dan" : "kyu");
	}
	
	/**
	 * Returns the rank in a format compatible with the MacMahon program.
	 * 
	 * @return rank in MacMahon format
	 */
	protected final String getMachMahonRank() {
		String rank_str = Integer.toString(this.value);
		if (this.is_dan) {
			rank_str += 'd';
		}
		return rank_str;
	}
	
	/**
	 * Parses a Rank object from a string.
	 * 
	 * @param rank string representing a rank
	 * @return corresponding rank in a Rank instance
	 * @throws GoException thrown if the string cannot be parsed a rank
	 */
	public static Rank parseRank(final String rank) throws GoException {
		for (Rank r : rank_list) {
			if (rank.equals(r.getId())) {
				return r;
			}
		}
		throw new GoException("String '" + rank + "' cannot be parsed as a Rank!");
	}
	
	/**
	 * Returns lowest rank in the rank list.
	 *  
	 * @return lowest rank in the rank list
	 */
	public static Rank getLowestRank() {
		return rank_list[rank_list.length - 1];
	}
	/**
	 * Returns a list of all the Rank objects in the rank list.
	 * 
	 * @return a Rank array
	 */
	protected static Rank[] getRankList() {
		return rank_list.clone();
	}

	/**
	 * Checks whether one Rank object is equal to another.
	 * 
	 * @param other Rank instance to compare to
	 * @return true if both represent the same Go rank, otherwise false.
	 */

	@Override
	public boolean equals(final Object other) {
		return this.id.equals(((Rank)other).id);
	}
	

	/**
	 * The compareTo method is overridden so that Rank objects can
	 * be sorted in the order of strongest to weakest or vice versa.
	 */
	@Override
	public int compareTo(final Object other) {
		Rank other_rank = (Rank)other;
		if(this.is_dan && !other_rank.is_dan) {
			return 1;
		} else if(!this.is_dan && other_rank.is_dan) {
			return -1;
		}
		
		return (this.is_dan ? 1 : -1) * Integer.valueOf(this.value).compareTo(other_rank.value);
	}
}
