package settings;

import java.io.File;
import model.ID;
import model.exceptions.GoException;

/**
 * Settings class contains fields that define the filenames and folder
 * structure used in by the program.
 * 
 * Also contains some other parameters for convenient access.
 * 
 * Before the getFolder-methods can be used correctly the setAppRoot method must be
 * run so that the application knows where to look for the folders and files.
 */
public class Settings {
	//application root folder path, all files and folders are made relative to this
	private static String app_root;
	//filenames
	private static final String DATA_FOLDER_NAME = "WEB-INF/data/";
	private static final String CLOSED_DATA_FOLDER_NAME = "WEB-INF/data/closed/";
	private static final String BACKUP_FOLDER_NAME = "WEB-INF/data/backup/";
	private static final String PUBLIC_FOLDER_NAME = "pub/";
	private static final String SYSLOG_FILE_NAME = "syslog.txt";
	public static final String CONF_FILE_NAME = "conf.txt";
	public static final String COUNTRYLIST_FILE_NAME = "countrylist.txt";
	//filename prefixes and suffixes
	public static final String TOURNAMENT_FILENAME_PREFIX = "tournament_";
	public static final String DATA_FILE_SUFFIX = ".xml";
	public static final String LOG_FILE_SUFFIX = ".log";
	public static final String REGISTRATION_EXPORT_FILE_SUFFIX = "_export.txt";	
	public static final String PRE_REGISTRATION_EXPORT_FILE_SUFFIX = "_export_pre.txt";	
	
	public static final String CHAR_ENCODING = "UTF-8";
	public static final int BACKUP_FILE_AMOUNT = 10;

	private static String password;
	
	private static ConfFile conf_file;
	
	/**
	 * Sets the root folder of the application. 
	 * 
	 * Must be run before calling the getFolder methods or the return 
	 * values will point to the root of the file system!
	 * 
	 * The method also reads the administrator password from the configuration file.
	 */
	public static void setAppRoot(final String root) throws GoException {
		app_root = root;
		if (!app_root.endsWith(File.separator)) {
			app_root += File.separatorChar;
		}

		conf_file = new ConfFile();
	
		password = conf_file.getPassword();
	}
		
	public static File getDataFolder() {
		return new File(app_root+DATA_FOLDER_NAME);
	}

	public static File getClosedDataFolder() {
		return new File(app_root+CLOSED_DATA_FOLDER_NAME);
	}

	public static File getBackupFolder() {
		return new File(app_root+BACKUP_FOLDER_NAME);
	}

	public static File getPublicFolder() {
		return new File(app_root+PUBLIC_FOLDER_NAME);
	}

		
	public static File getSysLogFile() {
		return new File(app_root+SYSLOG_FILE_NAME);
	}
	
	public static String getPassword() {
		return password;
	}

	/**
	 * Reads the next tournament ID from the configuration file.
	 */
	public static ID getNextTournamentId() throws GoException {
		return conf_file.getNextTournamentId();
	}
	
}
