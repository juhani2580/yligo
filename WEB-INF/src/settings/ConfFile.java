package settings;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

import model.ID;
import model.exceptions.GoException;
import model.exceptions.GoIOException;

/**
 * ConfFile is used to read application settings from a file in the file system.
 * 
 * At this point the only parameters are the administration password and
 * the tournament ID counter. The counter is also incremented every time
 * a new tournament ID is requested.
 */
public class ConfFile extends File {
	
	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -7879156866681564483L;

	/**
	 * Creates a new ConfFile instance that points to a file in folder
	 * Settings.getDataFolder with the filename Settings.CONF_FILE_NAME
	 */
	protected ConfFile() {
		super(Settings.getDataFolder(), Settings.CONF_FILE_NAME);
	}
	
	protected String getPassword() throws GoException {
		return this.getConfParameter("pwd");
	}
	
	/**
	 * Increments the tournament ID counter and returns an ID instance
	 * created with the old value.
	 */
	protected ID getNextTournamentId() throws GoException {
		//get next tournament id:
		final ID next_id = new ID(this.getConfParameter("tidcounter"));
		//increment tournament id counter:
		this.setConfParameter("tidcounter", Long.toString((next_id.getIdNumber()+1)));
		return next_id;
	}
	
	/**
	 * Returns a given parameter value as a string from the configuration file.
	 */
	private synchronized String getConfParameter(final String param_name) throws GoException { //GoIOException on GoExceptionin aliluokka
		try {
			final String param = '['+param_name+']';
			final int param_length = param.length();
			//read configuration file:
			final Scanner scanner = new Scanner(this);
			while (scanner.hasNextLine()) {
				final String row = scanner.nextLine();
				if (row.length() >= param_length && row.substring(0, param_length).equals(param)) {
					final String param_value = row.substring(param_length);
					scanner.close();
					return param_value;
				}
			}
			scanner.close();
		} catch(Exception e) {
			throw new GoIOException(this.getAbsolutePath());
		}
		throw new GoException("Parameter " + param_name + " not found from settings file "+this.getAbsolutePath());
	}

	/**
	 * Sets a given parameter value in the configuration file.
	 */
	private synchronized void setConfParameter(final String param_name, final String param_value) throws GoException { //GoIOException on GoExceptionin aliluokka
		try {
			final String param = '['+param_name+']';
			final int param_length = param.length();
			//read file:
			final ArrayList<String> row_list = new ArrayList<String>();
			final Scanner scanner_in = new Scanner(this);
			while (scanner_in.hasNextLine()) {
				String row = scanner_in.nextLine();
				if (row.length() >= param_length && row.substring(0, param_length).equals(param)) {
					row = param + param_value; //change param
				}
				row_list.add(row);
			}
			scanner_in.close();
			//write file:
			final BufferedWriter out = new BufferedWriter(new FileWriter(this));
			while (!row_list.isEmpty()) {
				out.write(row_list.remove(0)+"\n");
			}
			out.close();
		} catch(Exception e) {
			throw new GoIOException(this.getAbsolutePath());
		}
	}
}
