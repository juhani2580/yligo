/**
 * The HTTP servlet, which acts as the main controller of the system.
 * The servlet receives all HTTP requests from the user, handles the
 * data transactions and/or produces the view requested and sends it
 * back to the user.
 *
 * @author Kustaa Kangas
 */

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import settings.Settings;
import model.*;
import model.tournament.*;
import model.entry.*;
import model.exceptions.GoException;
import model.Tournament.EntryComparator.SortBy;
import view.*;
import view.page.*;
import view.url.*;

public class GoTournaments extends HttpServlet {
	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = 3198751315130946025L;
	private static final int SESSION_TIME = 31622400;	// Session lasts a year
	private transient Exception init_exception;
	private transient String password;
	
	/**
	 * Servlet initialization method, invoked right after construction.
	 * Sets up application root path and the administrator password as
	 * well as redirects the standard output stream to the system log
	 * file. If an exception occurs, it will be stored and displayed
	 * on each user request.
	 */
	@Override
	public void init() throws javax.servlet.ServletException {
		try {
			//set application root path:
			Settings.setAppRoot(this.getServletContext().getRealPath(""));
			//get system password from configuration file:
			this.password = Settings.getPassword();
			
			//read tournament data files into TournamentManager
			TournamentManager.readTournamentDataFromDisk();
			
			System.setOut(new PrintStream(new FileOutputStream(Settings.getSysLogFile(), true)));
			System.out.println(GoDate.getDateAndTimeString() + " GoTournaments.java:  System initialized");
		} catch(Exception e) {
			this.init_exception = e;
		}
	}
	
	/**
	 * A class to provide a simple interface for the HttpServletRequest.
	 * Provides methods to handle sessions and cookies and to automatically
	 * convert data received to various types.
	 */
	private class GoHttpRequest {
		private final transient HttpServletRequest req;
		
		/**
		 * Constructs a GoHttpRequest interface for an HttpServletRequest.
		 */
		public GoHttpRequest(final HttpServletRequest httpservletrequest) {
			this.req = httpservletrequest;
			
			try {
				this.req.setCharacterEncoding("utf-8");
			} catch(Exception e) {
				System.out.println("Warning: The character encoding of the HTTP request could not be interpreted as UTF-8.");
			}
		}
		
		/**
		 * Returns the value of a field sent either with 'get' or 'post'
		 * method as a String. If the field isn't set, null is returned.
		 *
		 * @param name   the name of the field
		 */
		public String getParameter(final String name) {
			return this.req.getParameter(name);
		}
		
		/**
		 * As above, but an empty String is returned instead of null.
		 */
		public String get(final String name) {
			final String parameter = this.getParameter(name);
			return (parameter == null) ? "" : parameter;
		}
		
		/**
		 * As in get(), but with an automatic conversion to a boolean.
		 * A String equal to "true" is converted to a true.
		 * Anything else is converted to a false.
		 */
		public boolean getBool(final String name) {
			return this.get(name).equals("true");
		}
		
		/**
		 * As in get(), but with an automatic conversion to a checkbox
		 * state (boolean). A checked checkbox always has the value
		 * "checked" in the system, which is converted to a true,
		 * while a non-checked checkbox has no value (an empty String),
		 * which, as well as anything else, is converted to a false.
		 */
		public boolean getCheckbox(final String name) {
			return this.get(name).equals("checked");
		}
		
		/**
		 * As in get(), but with an automatic conversion to a Go ID object.
		 * If the field value cannot be parsed, a null is returned.
		 */
		public ID getID(final String name) {
			try {
				return new ID(this.get(name));
			} catch(Exception e) {
				return null;
			}
		}
		
		/**
		 * As above, but always gets the tournament ID ("tid").
		 */
		public ID getTID() {
			return this.getID("tid");
		}
		
		/**
		 * As above, but always gets the entry ID ("eid").
		 */
		public ID getEID() {
			return this.getID("eid");
		}
		
		/**
		 * Logs the user in to the system as an administrator
		 * if given password matches the administrator password.
		 * Returns true on a match, otherwise false.
		 * Uses a HTTP session to store the state.
		 *
		 * @param  login_password   the password given by the user.
		 */
		public boolean login(final String login_password) {
			if(!login_password.equals(GoTournaments.this.password)) {
				return false;
			}
			
			final HttpSession session = this.req.getSession(true);
			session.setMaxInactiveInterval(GoTournaments.SESSION_TIME);
			return true;
		}
		
		/**
		 * Logs the user out of the system.
		 * Uses a HTTP session to store the state.
		 */
		public void logout() {
			final HttpSession session = this.req.getSession(true);
			session.invalidate();
		}
		
		/**
		 * Returns true if the user is logged in to the system, otherwise false.
		 * Uses a HTTP session to store the state.
		 */
		public boolean isAdmin() {
			final HttpSession session = this.req.getSession(true);
			
			// If the user isn't logged in, the session returned was just created.
			if(session.isNew()) {
				session.invalidate();
				return false;
			}
			
			return true;
		}
		
		/**
		 * Returns the value of a cookie or an empty String if the cookie isn't set.
		 *
		 * @param name   the name of the cookie.
		 */
		public String getCookie(final String name) {
			final Cookie[] cookies = this.req.getCookies();
			if(cookies == null) {
				return "";
			}
			
			for(Cookie cookie : cookies) {
				if(name.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
			
			return "";
		}
	}
	
	/**
	 * An exception thrown when the user isn't logged in
	 * but the request requires administrator privileges.
	 */
	private class AdminException extends Exception {
		/**
		 * UID for serialization
		 */
		private static final long serialVersionUID = -6427670035107249770L;
	}
	
	/**
	 * An exception thrown when an incorrect password is given.
	 */
	private class IncorrectPasswordException extends GoException {
		/**
		 * UID for serialization
		 */
		private static final long serialVersionUID = 3860121038612425540L;

		public IncorrectPasswordException() {
			super("The date of birth does not match the one given previously.");
		}
	}
	
	/**
	 * Extracts and performs a request described by a GoHttpRequest object,
	 * produces a response and returns it.
	 *
	 * @throws GoException      if a normal use error occurs, should
	 *                          be caught and reported to the user.
	 * @throws AdminException   if the request requires admin privileges
	 *                          and the user isn't logged in.
	 * @throws Exception        in case of an internal system error,
	 *                          which should be treated as a bug.
	 */
	private synchronized GoResponse serveRequest(final GoHttpRequest http_request) throws Exception {
		
		// The main request parameter, which determines the page or action requested
		final String request_parameter = http_request.getParameter("req");
		
		// Other common parameters, extracted here to avoid redundant code
		final boolean logged = http_request.isAdmin();		// Logged in flag
		final ID tid = http_request.getTID();			// Tournament ID
		final ID eid = http_request.getEID();			// Entry ID
		final boolean pre = http_request.getBool("pre");	// True if dealing with pre-registration
		final boolean edit = http_request.getBool("edit");	// True if requesting entry edit
		
		// If request parameter omitted, redirect by default to admin page
		// or pre-registration if a tournament ID is provided.
		if(request_parameter == null) {
			return new Redirection((tid == null)
					? new PAdminURL()
					: new PRegistrationURL(tid, true));
			
		} else if("a_login".equals(request_parameter)) {
			
			return http_request.login(http_request.get("password"))
					? new Redirection(new PAdminURL())
					: new ErrorPage("Incorrect password");
			
		} else if("a_sort_entry_list".equals(request_parameter)) {
			
			final GoResponse redir = new Redirection(new PRegistrationURL(tid, pre, http_request.getBool("print")));
			redir.addCookie(new Cookie("sort_by", http_request.get("by")));
			return redir;
			
		} else if("p_registration".equals(request_parameter)) {
			
			final boolean printable = http_request.getBool("print");
			
			if(!logged && (!pre || printable)) {
				throw new AdminException();
			}
			
			final Tournament tournament = TournamentManager.getOpenTournament(http_request.getTID());
			final SortBy sort_by = "rank".equals(http_request.getCookie("sort_by"))
					? SortBy.RANK
					: SortBy.LASTNAME;
			
			if(eid == null) {
				return new RegistrationPage(logged, tournament, pre, sort_by, printable);
			}
			
			final Entry entry = (pre || !edit)
					? tournament.getPreEntry(eid)
					: tournament.getEntry(eid);
			
			return new RegistrationPage(logged, tournament, pre, entry, edit, sort_by, printable);
			
		} else if("a_edit_entry".equals(request_parameter)) {
			
			// Only pre-entries can be edited while not logged.
			if(!pre && !logged) {
				throw new AdminException();
			}
			
			final String dob_day = http_request.get("day");
			final String dob_month = http_request.get("month");
			
			// If editing a pre-entry while not logged in, a password is required
			if(pre && edit && !logged) {
				final Entry pre_entry = TournamentManager.getOpenTournament(tid).getPreEntry(eid);
				final String correct_day = pre_entry.getBirthDayOfMonth();
				final String correct_month = pre_entry.getBirthMonthOfYear();
				
				if(!correct_day.equals(dob_day) || !correct_month.equals(dob_month)) {
					throw new IncorrectPasswordException();
				}
			}
			
			if(http_request.getCheckbox("delete")) {
				TournamentManager.deleteEntry(tid, eid);
				return new Redirection(new PRegistrationURL(tid, pre));
			}
			
			final EntryDataModelEditor editor = edit
					? (pre ? TournamentManager.getPreEntryUpdater(tid, eid)
						: TournamentManager.getEntryUpdater(tid, eid))
					: (pre ? TournamentManager.getPreEntryInitializer(tid)
						: TournamentManager.getEntryInitializer(tid));
			
			editor.setLastName(http_request.get("last_name"));
			editor.setFirstName(http_request.get("first_name"));
			editor.setRank(http_request.get("rank"));
			editor.setClub(http_request.get("club"));
			editor.setCountry(http_request.get("country"));
			editor.setInfo(http_request.get("info"));
			
			if(pre) {
				if(!edit) {
					editor.setDateOfBirth(dob_day, dob_month);
				}
			} else {
				editor.setTotalFee(http_request.get("total_fee"));
				editor.setPaidSum(http_request.get("paid"));
			}
			
			for(Discount discount : TournamentManager.getOpenTournament(tid).getDiscounts()) {
				final ID did = discount.getId();
				final boolean selected = http_request.getCheckbox("discount" + did);
				editor.setDiscountState(did, selected);
			}
			
			// If adding a new entry but an entry ID given, it's a pre-entry,
			// which is confirmed while the actual entry is added
			if(!pre && !edit && (eid != null)) {
				((EntryInitializer)editor).run(eid);
			} else {
				editor.run();
			}
			
			return new Redirection(new PRegistrationURL(tid, pre));
		}
		
		// The following requests always require admin privileges
		if(!logged) {
			
			throw new AdminException();
			
		} else if("a_logout".equals(request_parameter)) {
			
			http_request.logout();
			return new Redirection(new PAdminURL());
			
		} else if("p_admin".equals(request_parameter)) {
			
			return new AdminPage(TournamentManager.getOpenTournaments(),
					     TournamentManager.getClosedTournaments());
		} else if("p_edit_tournament".equals(request_parameter)) {
			
			return new EditTournamentPage((tid != null) ? TournamentManager.getOpenTournament(tid) : null);
			
		} else if("a_edit_tournament".equals(request_parameter)) {
			
			final TournamentDataModelEditor model_editor = (tid == null)
					? TournamentManager.getTournamentInitializer()
					: TournamentManager.getTournamentUpdater(tid);
			
			model_editor.setName(http_request.get("name"));
			model_editor.setBaseFee(http_request.get("base_fee"));
			model_editor.setDefaultCountry(http_request.get("default_country"));
			model_editor.setHtml(http_request.get("html"));
			model_editor.setDate(http_request.get("date"));
			model_editor.setPreregistrationOpen(http_request.getCheckbox("pre_open"));
			
			for(int ndid = 0; ndid < 20; ndid++) {
				final String new_discount_name = http_request.get("new_discount_name" + ndid);
				if(!"".equals(new_discount_name)) {
					final String new_discount_amount = http_request.get("new_discount_amount" + ndid);
					final boolean new_discount_dominant = http_request.getCheckbox("new_discount_dominant" + ndid);
					model_editor.addDiscount(new_discount_name, new_discount_amount, new_discount_dominant);
				}
			}
			
			if(tid != null) {
				final TournamentUpdater updater = (TournamentUpdater)model_editor;
				
				for(Discount discount : TournamentManager.getOpenTournament(tid).getDiscounts()) {
					final ID did = discount.getId();
					final boolean discount_remove = http_request.getCheckbox("discount_remove" + did);
					if(discount_remove) {
						updater.removeDiscount(did);
						continue;
					}
					
					final String discount_name = http_request.get("discount_name" + did);
					final String discount_amount = http_request.get("discount_amount" + did);
					final boolean discount_dominant = http_request.getCheckbox("discount_dominant" + did);
					updater.editDiscount(did, discount_name, discount_amount, discount_dominant);
				}
			}
			
			model_editor.run();
			
			return (tid == null)
					? new Redirection(new PAdminURL())
					: new Redirection(new PEditTournamentURL(tid));
			
		} else if("a_delete_tournaments".equals(request_parameter)) {
			
			for(Tournament tournament : TournamentManager.getClosedTournaments()) {
				final ID ctid = tournament.getId();
				if(http_request.getCheckbox("delete" + ctid)) {
					TournamentManager.deleteTournament(ctid);
				}
			}
			
			return new Redirection(new PAdminURL());
			
		} else if("a_delete_entry".equals(request_parameter)) {
			
			TournamentManager.deleteEntry(tid, http_request.getEID());
			return new Redirection(new PRegistrationURL(tid));
			
		} else if("p_import".equals(request_parameter)) {
			
			return new ImportPage(TournamentManager.getOpenTournament(tid));
			
		} else if("a_import".equals(request_parameter)) {
			
			final String markup = http_request.get("wiki_markup");
			final Tournament tournament = TournamentManager.getOpenTournament(tid);
			final int line = tournament.importPreEntriesFromWiki(markup);
			
			if(line != -1) {
				return new ErrorPage("Error on line: " + line + ", \"" + markup.split("\n")[line-1] + '"');
			}
			return new Redirection(new PAdminURL());
			
		} else if("p_export".equals(request_parameter)) {
			
			final Tournament tournament = TournamentManager.getOpenTournament(tid);
			final String export_file = tournament.writeExportFile(pre);
			return new Redirection("pub/" + export_file);
			
		} else if("p_log".equals(request_parameter)) {
			
			final Tournament tournament = TournamentManager.getOpenTournament(tid);
			final String log_file = tournament.getLogFileName();
			return new Redirection("pub/" + log_file);
			
		} else if("a_set_tournament_state".equals(request_parameter)) {
			
			if(http_request.getBool("open")) {
				TournamentManager.reopenTournament(tid);
			} else {
				TournamentManager.closeTournament(tid);
			}
			
			return new Redirection(new PAdminURL());
			
		} else if("p_statistics".equals(request_parameter)) {
			
			return new StatisticsPage(TournamentManager.getTournament(tid));
		}
		
		return new ErrorPage("Invalid request parameter \"" + request_parameter + "\". Make sure that you followed a valid link.");
	}
	
	/**
	 * Produces a response to a Go HTTP request.
	 * If a fatal exception was thrown during system initialization, the
	 * user request is ignored and an appropriate error message is returned.
	 * Otherwise the request is passed on to serverRequest(), which will
	 * perform the transactions and return the view requested. Exceptions
	 * thrown during the request handling are caught and a corresponding
	 * error page is returned.
	 *
	 * @param req   Go HTTP request
	 */
	private GoResponse produceResponse(final GoHttpRequest req) {
		try {
			// Exceptions thrown during initialization are handled here.
			if(this.init_exception != null) {
				return new ErrorPage("System initialization failed:", this.init_exception);
			}
			
			return serveRequest(req);
		} catch(AdminException e) {
			return new LoginPage();
		} catch(GoException e) {
			return new ErrorPage(e.getErrorMessage());
		} catch(Exception e) {
			return new ErrorPage("An unexpected exception occured. This indicates an internal error within the system.", e);
		}
	}
	
	/**
	 * Dispatches all HTTP requests received by the system to the request handler
	 * and sends the response generated by the handler to the user. The response
	 * is either an HTML document or a redirection to a new request. If a general
	 * exception occurs during the request handling, an error page is sent to the
	 * user. An IOException, which prevents data from being sent to the user is
	 * propagated and will be handled by the environment.
	 *
	 * @param req   the HTTP request received from the user
	 * @param res   the HTTP response sent back to the user
	 */
	@Override
	protected void service(final HttpServletRequest req, final HttpServletResponse res) throws IOException {
		res.setContentType("text/html; charset=utf-8");		// The content, if sent, will always be HTML
		final ServletOutputStream out = res.getOutputStream();
		
		try {
			final GoResponse response = produceResponse(new GoHttpRequest(req));
			
			for(Cookie cookie : response.getCookies()) {
				res.addCookie(cookie);
			}
			
			if(response instanceof GoPage) {
				final GoPage page = (GoPage)response;
				final byte[] utf_bytes = page.getSource(4).getBytes("UTF-8");
				out.write(utf_bytes);
			} else {
				final Redirection redir = (Redirection)response;
				res.sendRedirect(redir.getUrl());
			}
		} catch(Exception e) {
			out.print(new ErrorPage("An unexpected exception occured. This might indicate an internal error within the system.", e).getSource(4));
		}
	}
}
