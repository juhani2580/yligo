/**
 * A class for a URL of a "delete tournaments" action.
 *
 * @author Kustaa Kangas
 */

package view.url;

public class ADeleteTournamentsURL extends GoURL {
	/**
	 * Constructs the URL.
	 */
	public ADeleteTournamentsURL() {
		super("a_delete_tournaments");
	}
}
