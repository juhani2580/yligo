/**
 * A class for a URL of a "sort entry list" action.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class ASortEntryListURL extends GoURL {
	/**
	 * Constructs the URL.
	 *
	 * @param sort_by     the column by which to sort.
	 * @param tid         the tournament the registration of which to return to.
	 * @param pre         if true, redirects back to pre-registration
	 *                    otherwise final registration.
	 * @param printable   if true, redirects to a printable entry list.
	 */
	
	@SuppressWarnings("boxing")
	public ASortEntryListURL(final String sort_by, final ID tid, final boolean pre, final boolean printable) {
		super("a_sort_entry_list", "by", sort_by, "tid", tid, "pre", pre, "print", printable);
	}
}
