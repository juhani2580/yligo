/**
 * A class for a URL of an "import" page.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class PImportURL extends GoURL {	
	/**
	 * Constructs the URL.
	 *
	 * @param tid   the ID of the tournament to import to.
	 */
	public PImportURL(final ID tid) {
		super("p_import", "tid", tid);
	}
}
