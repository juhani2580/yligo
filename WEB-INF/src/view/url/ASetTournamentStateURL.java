/**
 * A class for a URL of a "set tournament state" action.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class ASetTournamentStateURL extends GoURL {
	/**
	 * Constructs the URL.
	 *
	 * @param tid    the ID of the tournament the state of which to set.
	 * @param open   if true, the state is set to "open", otherwise "closed".
	 */
	
	@SuppressWarnings("boxing")
	public ASetTournamentStateURL(final ID tid, final boolean open) {
		super("a_set_tournament_state", "tid", tid, "open", open);
	}
}
