/**
 * A class for a URL of the "admin" page.
 *
 * @author Kustaa Kangas
 */

package view.url;

public class PAdminURL extends GoURL {
	/**
	 * Constructs the URL.
	 */
	public PAdminURL() {
		super("p_admin");
	}
}
