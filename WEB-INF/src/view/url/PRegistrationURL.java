/**
 * A class for a URL of a "registration" page.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class PRegistrationURL extends GoURL {
	
	/**
	 * Constructs the URL.
	 *
	 * @param tid         the ID of the tournament the registration of which to display.
	 * @param pre         if true, displays the pre-registration, otherwise final registration.
	 * @param eid         the ID of an entry to prefill the registration form with.
	 *                    if null, the entry will be empty.
	 * @param edit        if true, the page will contain an edit form, prefilled with given entry data (eid).
	 *                    if false but eid isn't null, the form will be prefilled with a pre-entry (eid),
	 *                    which can be confirmed on the page.
	 * @param printable   if true, displays only a printable list of entries,
         *                    ignoring the three parameters above.
	 */
	@SuppressWarnings("boxing")
	public PRegistrationURL(final ID tid, final boolean pre, final ID eid, final boolean edit, final boolean printable) {
		super("p_registration", "tid", tid, "pre", pre, "eid", eid, "edit", edit, "print", printable);
	}
	
	/**
	 * As above, but missing parameters are given default values. Here 'edit' defaults to false.
	 */
	public PRegistrationURL(final ID tid, final boolean pre, final ID eid, final boolean printable) {
		this(tid, pre, eid, false, printable);
	}
	
	/**
	 * As above, but 'printable' also default to false.
	 */
	public PRegistrationURL(final ID tid, final boolean pre, final ID eid) {
		this(tid, pre, eid, false, false);
	}
	
	/**
	 * 'edit' defaults to false and 'eid' to null.
	 */
	public PRegistrationURL(final ID tid, final boolean pre, final boolean printable) {
		this(tid, pre, null, printable);
	}
	
	/**
	 * As above, but 'printable' also defaults to false.
	 */
	public PRegistrationURL(final ID tid, final boolean pre) {
		this(tid, pre, null);
	}
	
	/**
	 * As above, but 'pre' also defaults to false.
	 */
	public PRegistrationURL(final ID tid) {
		this(tid, false);
	}
}
