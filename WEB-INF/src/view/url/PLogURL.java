/**
 * A class for a URL of a "log" page.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class PLogURL extends GoURL {	
	/**
	 * Constructs the URL.
	 *
	 * @param tid   the ID of the tournament the log of which to view.
	 */
	public PLogURL(final ID tid) {
		super("p_log", "tid", tid);
	}
}
