/**
 * A class for a URL of a "statistics" page.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class PStatisticsURL extends GoURL {
	/**
	 * Constructs the URL.
	 *
	 * @param tid   the ID of the tournament the
	 *              statistics of which to access.
	 */
	public PStatisticsURL(final ID tid) {
		super("p_statistics", "tid", tid);
	}
}
