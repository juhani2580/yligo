/**
 * A class for a URL of an "export" page.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class PExportURL extends GoURL {
	/**
	 * Constructs the URL.
	 *
	 * @param tid   the ID of the tournament to export from.
	 * @param pre   if true, exports from pre-registration,
	 *              otherwise from final registration.
	 */
	@SuppressWarnings("boxing")
	public PExportURL(final ID tid, final boolean pre) {
		super("p_export", "tid", tid, "pre", pre);
	}
}
