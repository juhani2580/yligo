/**
 * A class for a URL of an "edit tournament" page.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class PEditTournamentURL extends GoURL {
	/**
	 * Constructs the URL.
	 *
	 * @param tid   the ID of the tournament to edit.
	 *              if null, the page will contain an empty
	 *              form for creating a new tournament.
	 */
	public PEditTournamentURL(final ID tid) {
		super("p_edit_tournament", "tid", tid);
	}
	
	/**
	 * As above, but with 'tid' set to null.
	 */
	public PEditTournamentURL() {
		this(null);
	}
}
