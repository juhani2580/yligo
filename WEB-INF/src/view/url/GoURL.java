/**
 * A class for an internal URL of the system.
 * Consists of the servlet name, a request parameter
 * and an arbitrary number of other parameters.
 */

package view.url;

public class GoURL {
	private final transient String url;
	
	/**
	 * Constructs the URL.
	 *
	 * @param req    a value for the main request parameter ('req').
	 * @param args   a list of other parameters and their values,
	 *               given as [arg1, value1, arg2, value2, arg3, ...]
	 *               a pairless number of arguments drops the final one.
	 *               each object is coerced to a String using their
	 *               toString() methods.
	 */
	public GoURL(final Object req, final Object ... args) {
		String url_constr = "GoTournaments?req=" + req;
		
		for(int i = 0; i < args.length - 1; i += 2) {
			url_constr += "&" + args[i] + "=" + args[i + 1];
		}
		
		this.url = url_constr;
	}
	
	/**
	 * Returns the URL as a String.
	 */
	@Override
	public String toString() {
		return this.url;
	}
}
