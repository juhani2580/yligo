/**
 * A class for a URL for an "edit entry" action.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class AEditEntryURL extends GoURL {
	/**
	 * Constructs the URL.
	 *
	 * @param tid      the ID of the tournament of the entry.
	 * @param pre      if the true, the URL refers to pre-registration,
	 *                 otherwise final registration
	 * @param eid      the ID of the entry
	 *                 if null, the URL denotes the creation of a new entry.
	 * @param edit     if true, the URL refers to an entry edit.
	 *                 if false but eid not null, the URL refers to
	 *                 final registration with the form prefilled with
	 *                 pre-entry data.
	 */
	@SuppressWarnings("boxing")
	public AEditEntryURL(final ID tid, final boolean pre, final ID eid, final boolean edit) {
		super("a_edit_entry", "tid", tid, "pre", pre, "eid", eid, "edit", edit);
	}
}
