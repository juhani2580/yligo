/**
 * A class for a URL of an "import" action.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class AImportURL extends GoURL {
	/**
	 * Constructs the URL.
	 *
	 * @param tid   the ID of the tournament to import to.
	 */
	
	public AImportURL(final ID tid) {
		super("a_import", "tid", tid);
	}
}
