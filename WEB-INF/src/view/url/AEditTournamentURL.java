/**
 * A class for a URL of an "edit tournament" action.
 *
 * @author Kustaa Kangas
 */

package view.url;

import model.ID;

public class AEditTournamentURL extends GoURL {
	/**
	 * Constructs the URL.
	 *
	 * @param tid   the ID of the tournament to edit
	 *              if null, the URL denotes the creation of a new tournament.
	 */
	
	public AEditTournamentURL(final ID tid) {
		super("a_edit_tournament", "tid", tid);
	}
	
	/**
	 * As above, but 'tid' automatically set to null,
	 * denoting the creation of a new tournament.
	 */
	public AEditTournamentURL() {
		this(null);
	}
}
