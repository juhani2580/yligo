/**
 * A class for a redirection to another URL.
 *
 * @author Kustaa Kangas
 */

package view;

import view.url.GoURL;

public class Redirection extends GoResponse {
	private final transient String url;
	
	/**
	 * Constructs the element.
	 *
	 * @param go_url   a Go URL to redirect to.
	 */
	public Redirection(final GoURL go_url) {
		this.url = go_url.toString();
	}
	
	/**
	 * As above, but accepts a String.
	 */
	public Redirection(final String redirect_url) {
		this.url = redirect_url;
	}
	
	/**
	 * Returns the URL of the redirection.
	 */
	public String getUrl() {
		return this.url;
	}
}
