/**
 * A class for a country selection element.
 *
 * @author Kustaa Kangas
 */

package view.element;

import view.xhtml.*;
import model.*;

public class CountrySelect extends Select {
	/**
	 * Constructs the element.
	 * 
	 * @param name
	 *            a value for the element's 'name' attribute.
	 * @param selection
	 *            a preselected country if null, the first element will be
	 *            preselected.
	 */
	public CountrySelect(final String name, final Country selection) {
		super(name);

		for (Country country : Country.getCountryList()) {
			final boolean selected = selection != null
					&& country.getCode().equals(selection.getCode());
			this.add(new SelectOption(country.getCode(), country.getName(),
					selected));
		}
	}
}
