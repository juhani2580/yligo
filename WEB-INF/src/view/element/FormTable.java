/**
 * A class for a table containing labeled form elements.
 *
 * @author Kustaa Kangas
 */

package view.element;

import view.xhtml.*;

public class FormTable extends Table {
	/**
	 * Constructs the element.
	 */
	public FormTable() {
		super(0);
	}
	
	/**
	 * Adds a labeled text field to the table.
	 *
	 * @param label   the label of the field.
	 * @param name    the "name" attribute of the field.
	 * @param value   the "value" attribute of the field.
	 * @return       itself.
	 */
        public FormTable addTextField(final String label, final String name, final String value) {
		this.addTr().addTd(label)
				.addTd(new TextInput(30, name).setAttribute("value", value));
		return this;
        }
	
	/**
	 * As above but with an empty value String.
	 */
        public FormTable addTextField(final String label, final String name) {
		return addTextField(label, name, "");
        }
	
	/**
	 * Adds a generic labeled HTML object to the table.
	 *
	 * @param label   the label of the object.
	 * @param field   the object itself.
	 * @return       itself.
	 */
	public FormTable addField(final String label, final HtmlObject field) {
		this.addTr().addTd(label)
				.addTd(field);
		return this;
	}
}
