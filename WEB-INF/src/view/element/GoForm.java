/**
 * A class for the HTML 'form' element, specialized for the Go system.
 *
 * @author Kustaa Kangas
 */

package view.element;

import view.xhtml.*;
import view.url.*;

public class GoForm extends PostForm {
	/**
	 * Constructs the element.
	 *
	 * @param url   a URL for the 'action' attribute.
	 */
	public GoForm(final GoURL url) {
		super(url.toString());
	}
	
	/**
	 * As above, but accepts a String, which is
	 * automatically converted to a Go URL.
	 */
	public GoForm(final String url) {
		this(new GoURL(url));
	}
}
