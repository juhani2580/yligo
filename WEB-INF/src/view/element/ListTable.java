/**
 * A class for a table for lists.
 *
 * @author Kustaa Kangas
 */

package view.element;

import view.xhtml.*;

public class ListTable extends Table {
	private transient Tr header;
	
	/**
	 * Constructs the element.
	 * A header row is automatically added.
	 */
	public ListTable() {
		super(1);
		this.setClass("list_table");
		this.setAttribute("cellpadding", "2px");
		this.header = this.addTr();
		this.header.setClass("list_head");
	}
	
	/**
	 * Returns the header row of the table.
	 */
	public Tr getHeader() {
		return this.header;
	}
}
