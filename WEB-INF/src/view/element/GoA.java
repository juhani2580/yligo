/**
 * A class for the HTML 'a' element, specialized for the Go system.
 * Accepts GoURL objects in addition to Strings.
 *
 * @author Kustaa Kangas
 */

package view.element;

import view.xhtml.*;
import view.url.*;

public class GoA extends A {
	/**
	 * Constructs the element.
	 *
	 * @param url     a URL for the 'href' attribute.
	 * @param label   contained label text.
	 */
	public GoA(final GoURL url, final String label) {
		super(url.toString(), label);
	}
}
