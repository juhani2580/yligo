/**
 * A class for a response produced by the request handler
 * and sent back to the user. Can be either an HTML page
 * (GoPage.java) or a redirection. Both may contain cookies.
 *
 * @author Kustaa Kangas
 */

package view;

import java.util.ArrayList;
import javax.servlet.http.Cookie;

public abstract class GoResponse {
	private final transient ArrayList<Cookie> cookies;
	
	/**
	 * Constructs the response.
	 */
	public GoResponse() {
		this.cookies = new ArrayList<Cookie>();
	}
	
	/**
	 * Adds a cookie to the response.
	 *
	 * @param cookie   the cookie to add.
	 */
	public void addCookie(final Cookie cookie) {
		this.cookies.add(cookie);
	}
	
	/**
	 * Returns an array of cookies in the response.
	 */
	public Cookie[] getCookies() {
		return this.cookies.toArray(new Cookie[0]);
	}
}
