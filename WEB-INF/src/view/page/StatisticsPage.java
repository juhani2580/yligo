/**
 * A page for tournament statistics.
 * Contains discount usage, players with debts,
 * all entries and all pre-entries.
 *
 * @author Kustaa Kangas
 */

package view.page;

import view.xhtml.*;
import view.element.*;
import view.url.*;
import model.*;

import java.math.BigDecimal;

public class StatisticsPage extends GoPage {
	/**
	 * A table of discounts and their usage.
	 */
	private class DiscountTable extends ListTable {
		/**
		 * Constructs the element.
		 *
		 * @param tournament   the tournament of the discounts.
		 */
		public DiscountTable(final Tournament tournament) {
			this.getHeader().addTh("Discount")
					.addTh("Times used")
					.addTh("Total income");
			
			for(Discount discount : tournament.getDiscounts()) {
				final int times_used = tournament.getNumberOfDiscounts(discount.getId());
				final BigDecimal total_income = discount.getAmount().multiply(new BigDecimal(times_used));
				
				this.addTr().addTd(discount.getName())
						.addTd("" + times_used)
						.addTd(total_income.toString());
			}
		}
	}
	
	/**
	 * A table for entries.
	 */
	private class EntryTable extends ListTable {
		/**
		 * Constructs the element.
		 *
		 * @param entries   the entries to display.
		 * @param pre       if true, displays them as pre-entries.
		 */
		public EntryTable(final Entry[] entries, final boolean pre) {
			Tr tr = this.getHeader();
			tr
					.addTh("Last name")
					.addTh("First name")
					.addTh("Rank")
					.addTh("Country")
					.addTh("Club");
			if(!pre) {
				tr
					.addTh("Total fee")
					.addTh("Paid");
			}
			
			for(Entry entry : entries) {
				tr = this.addTr();
				tr
						.addTd(entry.getLastName())
						.addTd(entry.getFirstName())
						.addTd(entry.getRank().getRepresentation())
						.addTd(entry.getCountry().toString())
						.addTd(entry.getClub());
				if(!pre) {
					tr
						.addTd(entry.getTotalFee().toString())
						.addTd(entry.getPaidSum().toString());
				}
			}
		}
	}
	
	/**
	 * Constructs the page.
	 *
	 * @param tournament   the tournament of the statistics.
	 */
	public StatisticsPage(final Tournament tournament) {
		super(true);
		
		this.getContent()
			.add(new H3("Statistics"))
				.add(new P("Discounts used"))
				.add(new DiscountTable(tournament))
				.add(new P("Players with debts"))
				.add(new EntryTable(tournament.getUnpaidEntries(), false))
				.add(new P("Entries"))
				.add(new EntryTable(tournament.getEntries(), false))
				.add(new P("Pre-entries"))
				.add(new EntryTable(tournament.getPreEntries(), true));
	}
}
