/**
 * A class for the main admin page, which lists all past and present
 * tournaments and actions related to them.
 *
 * @author Kustaa Kangas
 */

package view.page;

import view.xhtml.*;
import view.element.*;
import view.url.*;
import model.*;

public class AdminPage extends GoPage {
	/**
	 * A tournament table element.
	 */
	private class TournamentTable extends ListTable {
		/**
		 * Constructs the element.
		 *
		 * @param tournament_list   an array of tournaments.
		 * @param open              if true, constructs a list of open tournaments,
		 *                          otherwise closed.
		 */
		public TournamentTable(final Tournament[] tournament_list, final boolean open) {
			final Tr header = this.getHeader();
			header
					.addTh("Name")
					.addTh("Date")
					.addTh("File")
					.add(new Th("Actions").setColSpan(open ? 10 : 2));
			
			if(!open) {
				header.addTh("Delete");
			}
			
			for(Tournament tournament : tournament_list) {
				final ID tid = tournament.getId();
				
				final Tr new_tr = this.addTr();
				new_tr
					.addTd(tournament.getName())
					.addTd(tournament.getDate().toString())
					.addTd("" + tournament.getDataFilename());
				
				if(open) {
					new_tr
						.addTd(new GoA(new PRegistrationURL(tid), "Registration"))
						.addTd(new GoA(new PRegistrationURL(tid, true), "Pre-Registration"))
						.addTd(new GoA(new PEditTournamentURL(tid), "Edit"))
						.addTd(new GoA(new PImportURL(tid), "Import"))
						.addTd(new GoA(new PExportURL(tid, false), "Export"))
						.addTd(new GoA(new PExportURL(tid, true), "Pre-export"))
						.addTd(new GoA(new PLogURL(tid), "Log"));
				}
				
				new_tr.addTd(new GoA(new PStatisticsURL(tid), "Statistics"));
				new_tr.addTd(new GoA(new ASetTournamentStateURL(tid, !open), open ? "Close" : "Open"));
				
				if(!open) {
					new_tr.addTd(new CheckboxInput("delete" + tid, false));
				}
			}
		}
	}
	
	/**
	 * Constructs the page.
	 *
	 * @param opens     open tournaments.
	 * @param closeds   closed tournaments.
	 */
	public AdminPage(final Tournament[] opens, final Tournament[] closeds) {
		super(true);
		
		this.getContent().add(new P(new GoA(new PEditTournamentURL(), "Create new tournament")))
				.add(new H3("Open tournaments"))
				.add(new TournamentTable(opens, true))
				.add(new H3("Closed tournaments"))
				.add(new GoForm(new ADeleteTournamentsURL())
					.add(new TournamentTable(closeds, false))
					.add(new P(new SubmitInput("Delete selected"))));
	}
}
