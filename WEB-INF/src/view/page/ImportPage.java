/**
 * A class for the wiki import page.
 * Displays a form to enter wiki markup,
 * which will be parsed by the system.
 *
 * @author Kustaa Kangas
 */

package view.page;

import view.xhtml.*;
import view.element.*;
import view.url.*;
import model.*;

public class ImportPage extends GoPage {
	/**
	 * Constructs the page.
	 *
	 * @param tournament   the tournament to import to
	 */
	public ImportPage(final Tournament tournament) {
		super(true);
		
		this.getContent().add(new H3("Wiki import"))
				.add(new GoForm(new AImportURL(tournament.getId()))
					.add(new Textarea(30, 120, "wiki_markup"))
					.add(new SubmitInput("Import")));
	}
}
