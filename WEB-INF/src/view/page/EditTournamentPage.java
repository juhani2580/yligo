/**
 * A class for a tournament editor page.
 *
 * @author Kustaa Kangas
 */

package view.page;

import view.xhtml.*;
import view.element.*;
import view.url.*;
import model.*;

public class EditTournamentPage extends GoPage {
	/**
	 * A class for tournament data field table.
	 */
	private class FieldTable extends FormTable {
		/**
		 * Constructs the element.
		 *
		 * @param tournament   the tournament to get the field data from.
		 *                     if null, the fields are be empty.
		 */
		public FieldTable(final Tournament tournament) {
			String name = "";
			String base_fee = "";
			Country default_country = null;
			String date = "";
			String html = "";
			boolean pre_open = true;
			
			if(tournament != null) {
				name = tournament.getName();
				base_fee = tournament.getBaseFee().toString();
				default_country = tournament.getDefaultCountry();
				date = tournament.getDate().toString();
				html = tournament.getHtml();
				pre_open = tournament.isPreRegistrationOpen();
			}
			
			this.addTextField("Name", "name", name);
			this.addTextField("Base fee", "base_fee", base_fee);
			this.addField("Default country", new CountrySelect("default_country", default_country));
			this.addTextField("Start date", "date", date);
			this.addField("Free HTML", new Textarea(12, 60, "html", html));
			this.addField("Pre-registration enabled", new CheckboxInput("pre_open", pre_open));
		}
	}
	
	/**
	 * A class for a discount creation, editing and deletion.
	 */
	private class DiscountTable extends ListTable {
		/**
		 * Constructs the element.
		 *
		 * @param tournament   the tournament to get the existing discounts from.
		 *                     if null, the table is empty.
		 */
		public DiscountTable(final Tournament tournament) {
			this.setId("discount_table");
			this.getHeader()
					.addTh("Discount")
					.addTh("Amount")
					.addTh("Is dominant")
					.addTh("Remove");
			
			if(tournament == null) {
				return;
			}
			
			for(Discount discount : tournament.getDiscounts()) {
				final ID did = discount.getId();
				final String name = discount.getName();
				final String amount = discount.getAmount().toString();
				final boolean dominant = discount.isDominant();
				
				this.addTr()
						.addTd(new TextInput(20, "discount_name" + did, name))
						.addTd(new TextInput(5, "discount_amount" + did, amount))
						.addTd(new CheckboxInput("discount_dominant" + did, dominant))
						.addTd(new CheckboxInput("discount_remove" + did));
			}
		}
	}
	
	/**
	 * A class for the tournament creation/editing form.
	 */
	private final class TournamentForm extends GoForm {
		/**
		 * Constructs the element.
		 *
		 * @param tournament   the tournament to edit with the form.
		 *                     if null, the form will create a new tournament.
		 */
		private TournamentForm(final Tournament tournament) {
			super(new AEditTournamentURL((tournament != null) ? tournament.getId() : null));
			
			this.add(new FieldTable(tournament));
			this.add(new P(new ButtonInput("Add discount", "new_discount()")));
			this.add(new DiscountTable(tournament));
			this.add(new Script("js/add_discount.js"));
			this.add(new P(new SubmitInput(tournament == null ? "Create" : "Save")));
		}
	}
	
	/**
	 * Constructs the page.
	 *
	 * @param tournament   the tournament to edit.
	 *                     if null, the page contains a form to create a new tournament.
	 */
	public EditTournamentPage(final Tournament tournament) {
		super(true);
		
		this.getContent()
				.add(new H3(tournament == null ? "Create tournament" : "Edit tournament"))
				.add(new TournamentForm(tournament));
	}
}
