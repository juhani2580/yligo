/**
 * A class for the registration and pre-registration pages.
 * The page can be generated with various settings.
 * For registration, it can contain:
 * - an empty registration form
 * - a form prefilled using data from a pre-entry
 * - an edit form of a previously added entry
 * For pre-registration, it can contain:
 * - an empty pre-registration form
 * - an edit form of a previously added pre-entry
 * It can also contain:
 * - only a single printable table of entries
 *
 * @author Kustaa Kangas
 */

package view.page;

import view.xhtml.*;
import view.element.*;
import view.url.*;
import model.*;
import model.Tournament.EntryComparator.SortBy;

public class RegistrationPage extends GoPage {
	/**
	 * A class for a table to list all entries.
	 */
	private class EntryTable extends ListTable {
		/**
		 * Constructs the element.
		 *
		 * @param tournament   the tournament to get the entries from.
		 * @param pre          if true, creates a table of pre-entries,
		 *                     otherwise uses the final entries.
		 * @param pre_page     if true, displays actions on a preregistration page,
		 *                     otherwise on a final registration page.
		 * @param sort_by      the column to sort by.
		 * @param printable    if true, creates a printable version of the table.
		 */
		public EntryTable(final Tournament tournament, final boolean pre, final boolean pre_page, final SortBy sort_by, final boolean printable) {
			final ID tid = tournament.getId();
			final Entry[] entries = pre ? tournament.getPreEntries(sort_by) : tournament.getEntries(sort_by);
			Tr header = this.getHeader();
			
			header
					.addTh(new GoA(new ASortEntryListURL("name", tid, pre_page, printable), "Last name"))
					.addTh("First name")
					.addTh(new GoA(new ASortEntryListURL("rank", tid, pre_page, printable), "Rank"))
					.addTh("Club");
			
			// The columns for final entries only
			if(!pre) {
				header.addTh("Country");
				
				if(!printable) {
					header
						.addTh("Total fee")
						.addTh("Paid")
						.addTh("Discounts");
				}
			}
			
			if(!printable) {
				header.add(new Th("Info"));
				header.add(new Th(pre && !pre_page ? "Fill" : "Edit"));
			}
			
			// Add entry rows to the table
			for(Entry e : entries) {
				final Tr tr = new Tr()
						.addTd(e.getLastName())
						.addTd(e.getFirstName())
						.addTd(e.getRank().getRepresentation())
						.addTd(e.getClub());
				if(!pre) {
					tr.addTd(e.getCountry().getName());
					
					if(!printable) {
						String discounts_str = "";
						int discount_n = 1;
						boolean first = true;
						
						for(Discount discount : tournament.getDiscounts()) {
							if(e.hasDiscountId(discount.getId())) {
								if(!first) {
									discounts_str += ", ";
								} else {
									first = false;
								}
								discounts_str += discount_n;
							}
							discount_n++;
						}
						
						tr
							.addTd(e.getTotalFee().toString())
							.addTd(e.getPaidSum().toString())
							.addTd(discounts_str);
					}
				}
				
				if(!printable) {
					tr.addTd(e.getInfo());
					
					ID eid = e.getId();
					
					if(pre && !pre_page) {
						tr.addTd(new GoA(new PRegistrationURL(tid, false, eid, false, false), "Fill"));
					} else {
						tr.addTd(new GoA(new PRegistrationURL(tid, pre, eid, true, false), "Edit"));
					}
				}
				
				this.add(tr);
			}
		}
		
		/**
		 * As above but by default final entries on final registration.
		 */
		public EntryTable(final Tournament tournament, final SortBy sort_by, final boolean printable) {
			this(tournament, false, false, sort_by, printable);
		}
	}
	
	/**
	 * A class for a discount selection table.
	 */
	private final class DiscountTable extends Table {
		/**
		 * Constructs the element.
		 *
		 * @param tournament   the tournament to get the available discounts from.
		 * @param entry        the entry to get the selected discounts from
		 *                     if null, all discounts are non-selected.
		 * @param pre          if true, displays pre-registration data,
		 *                     otherwise final registration data.
		 */
		private DiscountTable(final Tournament tournament, final Entry entry, final boolean pre) {
			super(0);
			
			if(pre) {
				this.addTr().add(new Th("Discounts")
					.setColSpan(pre ? 3 : 5)
					.setClass("discount_header"));
			}
			
			int discount_n = 1;
			for(Discount discount : tournament.getDiscounts()) {
				final ID did = discount.getId();
				final boolean checked = (entry == null) ? false : entry.hasDiscountId(did);
				final Tr tr = this.addTr();
				
				if(!pre) {
					tr.addTd(discount_n + ". ");
				}
				
				tr.addTd(discount.getName())
					.addTd(new CheckboxInput("discount" + did, checked)
						.setAttribute("onclick", "update_fee()"))
					.addTd(discount.getAmount().toString() + " eur");
				
				if(!pre) {
					tr.addTd(discount.isDominant() ? "(dom.)" : "");
				}
				
				discount_n++;
			}
			
			if(pre) {
				final String base_fee = tournament.getBaseFee().toString();
				this.addTr().add(new Td("Base fee: " + base_fee).setColSpan(3));
				this.addTr().add(new Td("Your predicted fee: ")
						.setColSpan(3)
						.add(new Span(base_fee).setId("fee_label")));
			}
		}
	}
	
	/**
	 * A class for a rank selection element.
	 */
	private class RankSelect extends Select {
		/**
		 * Constructs the element.
		 *
		 * @param tournament   the tournament to get the ranks from.
		 * @param selection    a preselected rank
		 *                     if null, the first rank in the list is preselected
		 */
		public RankSelect(final Tournament tournament, final Rank selection) {
			super("rank");
			
			for(Rank rank : tournament.getRankList()) {
				String repr = rank.getRepresentation();
				final boolean selected = (rank == selection);
				this.add(new SelectOption(rank.getId(), repr, selected));
			}
		}
	}
	
	/**
	 * A class for a day selection element.
	 */
	private static class DaySelect extends Select {
		/**
		 * Constructs the element.
		 */
		public DaySelect() {
			super("day");
			
			this.add(new SelectOption("--", "--"));
			for(int i = 1; i <= 31; i++) {
				this.add(new SelectOption((i < 10 ? "0" : "") + i, i + "."));
			}
		}
	}
	
	/**
	 * A class for a month selection element.
	 */
	private static class MonthSelect extends Select {
		private static final String[] months = {
			"Jan", "Feb", "Mar", "Apr", "May", "Jun",
   			"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
		
		/**
		 * Constructs the element.
		 */
		public MonthSelect() {
			super("month");
			
			this.add(new SelectOption("--", "--"));
			for(int i = 1; i <= 12; i++) {
				this.add(new SelectOption((i < 10 ? "0" : "") + i, months[i - 1]));
			}
		}
	}
	
	/**
	 * A class for a Javascript code to automatically update
	 * entry fee fields as the discount selection changes.
	 */
	private static class DiscountJavascript extends Script {
		/**
		 * Generates the script as per parameters given to constructor.
		 */
		private static String generate(final Tournament tournament, final boolean pre, final boolean update_on_load) {
			String javascript =
					(update_on_load ? "update_fee();\n" : "") +
					"function update_fee() {\n" +
					"var regular_total = 0;\n" +
					"var dominant_total = 0;\n" +
					"var form = document.getElementById('entry_form');\n";
			
			for(Discount discount : tournament.getDiscounts()) {
				final ID did = discount.getId();
				final String amount = discount.getAmount().toString();
				final boolean dominant = discount.isDominant();
				javascript += "if(form.discount" + did + ".checked) ";
				if(dominant) {
					javascript += "if(dominant_total == 0 || " + amount + " > dominant_total) dominant_total = " + amount + ";\n";
				} else {
					javascript += "regular_total += " + amount + ";\n";
				}
			}
			
			javascript +=
					"var total = " + tournament.getBaseFee() + ";\n" +
					"if(dominant_total != 0) total -= dominant_total; else total -= regular_total;\n" +
					"if(total < 0) total = 0;\n" +
					(pre ? ("var fee_label = document.getElementById('fee_label');\n"
					+ "fee_label.innerHTML = total;\n")
					: ("form.total_fee.value = total;\n" + "form.paid.value = total;\n")) +
					"}\n";
			
			return javascript;
		}
		
		/**
		 * Constructs the element.
		 *
		 * @param tournament       the tournament of the discounts
		 * @param pre              if true, generates the script for pre-registration,
		 *                         otherwise for final registration.
		 * @param update_on_load   if true, the update script will be run on page load.
		 */
		public DiscountJavascript(final Tournament tournament, final boolean pre, final boolean update_on_load) {
			super(new RawText(generate(tournament, pre, update_on_load)));
		}
	}
	
	/**
	 * A Javascript code for including entry data in the code.
	 */
	private static class EntryDataJavascript extends Script {
		/**
		 * Generates the script as per parameters given to constructor.
		 */
		private static String generate(final Tournament tournament) {
			String javascript = "tournament_id = " + tournament.getId() + ";\n";
			javascript += "users = [\n";
			
			for(Entry e : tournament.getPreEntries()) {
				javascript += "{ id: " + e.getId()
						+ ", last_name: '"+ e.getLastName()
						+ "', first_name: '" + e.getFirstName()
						+ "', rank: '" + e.getRank().getRepresentation()
						+ "', club: '" + e.getClub()
						+ "', country: '" + e.getCountry()
						+ "' },\n";
			}
			
			javascript += "]\n";
			return javascript;
		}
		
		/**
		 * Constructs the element.
		 *
		 * @param tournament   the tournament to get the data from.
		 */
		public EntryDataJavascript(final Tournament tournament) {
			super(new RawText(generate(tournament)));
		}
	}
	
	/**
	 * A class for the entry form element.
	 */
	private class EntryForm extends GoForm {
		/**
		 * Constructs the element.
		 *
		 * @param logged       if true, generates the form for administrator.
		 * @param tournament   the tournament of the entry.
		 * @param pre          if true, generates the entry for pre-registration,
		 *                     otherwise for final registration.
		 * @param entry        the (pre-)entry to edit or to fill the form from.
		 * @param edit         if true, creates an form for editing,
		 *                     otherwise for addition of a new entry.
		 */
		public EntryForm(final boolean logged, final Tournament tournament, final boolean pre, final Entry entry, final boolean edit) {
			super(new AEditEntryURL(tournament.getId(), pre, entry != null ? entry.getId() : null, edit));
			this.setId("entry_form");
			
			final boolean pre_fill = !edit && !pre && (entry != null);
			
			String last_name = "";
			String first_name = "";
			Rank rank = null;
			String club = "";
			Country country = tournament.getDefaultCountry();
			String total_fee = tournament.getBaseFee().toString();
			String paid = tournament.getBaseFee().toString();
			String info = "";
			
			if(entry != null) {
				last_name = entry.getLastName();
				first_name = entry.getFirstName();
				rank = entry.getRank();
				club = entry.getClub();
				country = entry.getCountry();
				info = entry.getInfo();
				
				if(!pre) {
					total_fee = entry.getTotalFee().toString();
					paid = entry.getPaidSum().toString();
				}
			}
			
			final Input last_name_box = new TextInput(30, "last_name");
			last_name_box.setAttribute("value", last_name);
			final Input first_name_box = new TextInput(30, "first_name");
			first_name_box.setAttribute("value", first_name);
			final Select rank_box = new RankSelect(tournament, rank);
			rank_box.setId("rank");
			
			if(!edit) {
				last_name_box.setId("last_name");
				last_name_box.setAttribute("onkeyup", "filter_tables()");
				first_name_box.setId("first_name");
				first_name_box.setAttribute("onkeyup", "filter_tables()");
			}
			
			final FormTable table = new FormTable();
			
			table
					.addField("Last name", last_name_box)
					.addField("First name", first_name_box)
					.addField("Rank", rank_box)
					.addTextField("Club", "club", club)
					.addField("Country", new CountrySelect("country", country));
			
			if(!pre) {
				table.addTextField("Total fee", "total_fee", total_fee);
				table.addTextField("Paid", "paid", paid);
			}
			
			table.addTextField("Other info", "info", info);
			
			if(pre && (!edit || !logged)) {
				table.addTr().addTd("Date of birth")
						.add(new Td().add(new DaySelect())
							.add(new MonthSelect()));
			}
			
			if(pre) {
				if(edit) {
					if(!logged) {
						table.addTr().add(new Td("Re-enter the birth date you gave earlier.")
								.setColSpan(2));
					}
				} else {
					table.addTr().add(new Td("The date is required to edit or delete your registration.")
							.setColSpan(2));
				}
			}
			
			this.add(table);
			this.add(new DiscountTable(tournament, entry, pre));
			this.add(new DiscountJavascript(tournament, pre, !(!pre && edit)));
			
			if(!pre) {
				this.add(new Script("js/focus.js"));
				this.add(new Script("js/table_filter.js"));
				this.add(new Script("js/jquery/jquery.js"));
				this.add(new Script("js/jquery/jquery.bgiframe.min.js"));
				this.add(new Script("js/jquery/jquery.autocomplete.js"));
				this.add(new EntryDataJavascript(tournament));
				this.add(new Script("js/pre_suggest.js"));
			}
			
			P form_p = new P(new SubmitInput(edit ? "Save changes" : (pre_fill ? "Confirm" : "Register")));
			if(edit) {
				this.add(new FormTable().addField("Delete entry", new CheckboxInput("delete", false)));
				form_p.add(new GoA(new PRegistrationURL(tournament.getId(), pre), "Return without editing"));
			} else if(pre_fill) {
				form_p.add(new GoA(new PRegistrationURL(tournament.getId()), "Cancel pre-entry confirmation"));
			}
			this.add(form_p);
		}
	}
	
	/**
	 * Constructs the page.
	 *
	 * @param logged       if true, creates the page for administrator.
	 * @param tournament   the tournament of the registration.
	 * @param pre          if true, craetes the page for pre-registration, otherwise for final registration.
	 * @param entry        an entry to prefill the entry form with.
	 * @param edit         if true, displays a form for editing, otherwise for adding a new entry.
	 * @param sort_by      entry list sorting method.
	 * @param printable    if true, displays only a single printable table of entries.
	 */
	public RegistrationPage(final boolean logged, final Tournament tournament, final boolean pre, final Entry entry, final boolean edit, final SortBy sort_by, final boolean printable) {
		super(logged && !printable);
		
		if(!pre) {
			if(entry != null && !edit) {
				this.setOnLoad("rank_focus()");
			} else {
				this.setOnLoad("lastname_focus()");
			}
		}
		
		final Div content = this.getContent();
		
		if(printable) {
			content.add(new EntryTable(tournament, sort_by, true));
			return;
		}
		
		Div left = new Div();
		content.add(left.setClass("left_column"));
		
		left.add(new H3(tournament.getName() + " - "
				+ (pre ? "pre-registration" : "registration")
				+ (edit ? " (editing)" : "")));
		
		if(pre) {
			left.add(new RawText(tournament.getHtml()));
		}
		
		left.add(new EntryForm(logged, tournament, pre, entry, edit));
		
		if(edit) {
			return;
		}
		
		if(logged) {
			left.add("Total income: " + tournament.getTotalPaid() + " / " + tournament.getTotalFees());
			left.add(new P("Pre-entries (" + tournament.getPreEntries().length + ')'));
		}
		
		left.add(new EntryTable(tournament, true, pre, sort_by, printable).setId("pre_entries"));
		
		if(!pre) {
			content.add(new GoA(new PRegistrationURL(tournament.getId(), false, true), "Printable version"));
			content.add(new P("Entries (" + tournament.getEntries().length + ')'));
			content.add(new EntryTable(tournament, sort_by, printable).setId("entries"));
		}
	}
	
	/**
	 * As above but by default an empty form for adding a new (pre-)entry.
	 */
	public RegistrationPage(final boolean logged, final Tournament tournament, final boolean pre, final SortBy sort_by, final boolean printable) {
		this(logged, tournament, pre, null, false, sort_by, printable);
	}
}
