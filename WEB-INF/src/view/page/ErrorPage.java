/**
 * A page for an arbitrary error message, which can be
 * sent to the user for various reasons.
 *
 * @author Kustaa Kangas
 */

package view.page;

import view.xhtml.*;
import view.url.*;
import settings.*;

import java.io.File;

public class ErrorPage extends GoPage {
	/**
	 * Produces a String representation for an exception stack trace,
	 * adding a newline after each method invocation.
	 *
	 * @param exception   the exception to generate the String from.
	 */
	private static String getStackTrace(final Exception exception) {
		String trace = "";
		
		for(StackTraceElement element : exception.getStackTrace()) {
			trace += element.toString() + "\n";
		}
		
		return trace;
	}
	
	/**
	 * Constructs the page with a simple error message.
	 *
	 * @param msg   the error message to display.
	 */
	public ErrorPage(final String msg) {
		super(false);
		
		this.setTitleText("Go Tournaments - Error");
		this.getContent().add(new P(msg));
	}
	
	/**
	 * Constructs the page with a message and an exception stack trace.
	 *
	 * @param msg         the error message to display
	 * @param exception   the exception to display
	 */
	public ErrorPage(final String msg, final Exception exception) {
		this(msg);
		
		this.getContent().add(new P("Stack trace: " + exception.toString()))
				.add(new Pre(getStackTrace(exception)))
				.add(new P("Data directory is: " + Settings.getDataFolder() + File.separator + Settings.CONF_FILE_NAME));
	}
}
