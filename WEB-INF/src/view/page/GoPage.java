/**
 * An abstract base class for all HTML pages of the system.
 * The class constructs the main structure of the HTML document,
 * adding a navigation and a main content block, which can be
 * accessed and modified by a subclass.
 *
 * @author Kustaa Kangas
 */

package view.page;

import view.GoResponse;
import view.xhtml.*;
import view.element.*;
import view.url.*;

public abstract class GoPage extends GoResponse {
	private final transient XHTMLDocument doc;
	private final transient Div content;
	
	/**
	 * Returns the content 'div' element of the page.
	 */
	protected Div getContent() {
		return this.content;
	}
	
	/**
	 * Sets the the title text of the page.
	 *
	 * @param text   the text to set.
	 */
	protected void setTitleText(final String text) {
		this.doc.setTitleText(text);
	}
	
	/**
	 * Sets the onload attribute of 'body'.
	 *
	 * @param onload   the value for the attribute.
	 */
	protected void setOnLoad(final String onload) {
		this.doc.getBody().setAttribute("onload", onload);
	}
	
	/**
	 * Constructs the page.
	 *
	 * @param logged   if true, the viewer of the page is considered
	 *                 an administrator and a navigation bar is
	 *                 included on the page.
	 */
	protected GoPage(final boolean logged) {
		this.doc = new XHTMLDocument();
		this.setTitleText("Go Tournament Registration");
		final Body body = this.doc.getBody();
		
		if(logged) {
			body.add(new Div()
				.add(new GoA(new PAdminURL(), "Main page"))
				.add(new GoForm("a_logout")
					.add(new P(new SubmitInput("Log out")))));
		}
		
		this.content = new Div();
		body.add(this.content);
	}
	
	/**
	 * Returns the XHTML source code of the page.
	 */
	public String getSource() {
		return this.doc.getSource();
	}
	
	/**
	 * As above, but with indentation.
	 *
	 * @param indent_width   the width of the indentation in spaces.
	 */
	public String getSource(final int indent_width) {
		return this.doc.getSource(indent_width);
	}
}
