/**
 * A class for the login page displayed when the user
 * is required to log in to the system. Displays a
 * form with a password field and a submit box.
 *
 * @author Kustaa Kangas
 */

package view.page;

import view.xhtml.*;
import view.element.*;

public class LoginPage extends GoPage {
	/**
	 * Constructs the page.
	 */
	public LoginPage() {
		super(false);
		
		this.getContent().add(new H3("Your request requires admin privileges."))
				.add(new GoForm("a_login")
					.add(new P()
						.add("Enter admin password: ")
						.add(new PasswordInput(20, "password"))
						.add(new SubmitInput("Log in"))));
	}
}
