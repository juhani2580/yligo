/**
 * A class for the HTML 'th' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Th extends TableCell {
	/**
	 * Constructs the element.
	 */
	public Th() {
		super("th");
	}
	
        /**
         * Constructs the element and automatically adds an object to it.
         *
         * @param html   the object to add.
         */
	public Th(final HtmlObject html) {
		this();
		this.add(html);
	}
	
	/**
	 * As above, but accepts a String.
	 */
	public Th(final String text) {
		this();
		this.add(text);
	}
}
