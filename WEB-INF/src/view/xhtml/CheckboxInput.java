/**
 * A subclass for a type 'checkbox' input element (See Input.java).
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class CheckboxInput extends Input {
	/**
	 * Constructs the element.
	 *
	 * @param name   a value for the 'name' attribute.
	 */
	public CheckboxInput(final String name) {
		super(InputType.CHECKBOX, name, "checked");
	}
	
	/**
	 * As above, but with a 'checked' parameter.
	 *
	 * @param checked   if true, sets 'checked' attribute to 'checked'.
	 */
	public CheckboxInput(final String name, final boolean checked) {
		this(name);
		
		if(checked) {
			this.setAttribute("checked", "checked");
		}
	}
}
