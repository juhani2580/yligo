/**
 * A class for the HTML 'pre' element.
 * The element is unindentable.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Pre extends ContainerElement {
	/**
	 * Constructs the element.
	 */
	public Pre() {
		super("pre", false);
	}
	
	/**
	 * Constructs the element and immediately adds text to it.
	 */
	public Pre(final String text) {
		this();
		this.add(text);
	}
}
