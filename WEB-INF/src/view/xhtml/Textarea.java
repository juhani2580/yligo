/**
 * A class for the HTML 'textarea' element.
 * The element is unindentable.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Textarea extends ContainerElement {
	/**
	 * Constructs the element.
	 *
	 * @param rows   the number of rows.
	 * @param cols   the number of columns.
	 */
	public Textarea(final int rows, final int cols) {
		super("textarea", false);
		
		this.setAttribute("rows", Integer.toString(rows));
		this.setAttribute("cols", Integer.toString(cols));
	}
	
	/**
	 * As above, but...
	 *
	 * @param name   a value for the 'name' attribute.
	 */
	public Textarea(final int rows, final int cols, final String name) {
		this(rows, cols);
		this.setAttribute("name", name);
	}
	
	/**
	 * As above, but...
	 *
	 * @param text   a text to add automatically to itself.
	 */
	public Textarea(final int rows, final int cols, final String name, final String text) {
		this(rows, cols, name);
		this.add(text);
	}
}
