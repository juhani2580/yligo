/**
 * A class for the HTML 'title' element.
 * The element is unindentable.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Title extends ContainerElement {
	/**
	 * Constructs the element.
	 *
	 * @param text   the title text, which is added automatically.
	 */
	public Title(final String text) {
		super("title", false);
		this.add(text);
	}
	
	/**
	 * As above, but with an empty title.
	 */
	public Title() {
		this("");
	}
}
