/**
 * A base class for all HTML elements, which may not contain other HTML objects
 * and are thus started and closed with a single tag. Implements the getHtml()
 * methods, which return the single tag, indented appropriately if so specified.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public abstract class EmptyElement extends HtmlElement {
	/**
	 * Constructs the element.
	 *
	 * @param tag_name   the name of the elements's tag.
	 */
	public EmptyElement(final String tag_name) {
		super(tag_name);
	}
	
	/**
	 * Returns the HTML source code of the element.
	 */
	@Override
	public final String getHtml() {
		return this.getStartTag(true);
	}
	
	/**
	 * As above but with indendation.
	 *
	 * @param indent_level   the indendation level used for the element.
	 * @param indent_width   the indendation width for inner elements.
	 *                       (not used for empty elements, required for polymorphism).
	 */
	@Override
	public final String getHtml(final int indent_level, final int indent_width) {
		return HtmlObject.getIndent(indent_level)
				+ this.getHtml()
				+ "\n";
	}
}
