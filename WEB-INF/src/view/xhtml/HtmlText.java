/**
 * A class for plain text, which can be added to an HTML element.
 * The text will be automatically encoded with HTML entities as needed.
 * To bypass the encoding, use RawText.java instead.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class HtmlText extends RawText {
	/**
	 * Constructs an HtmlText object from a String.
	 */
	public HtmlText(final String text) {
		super(text);
	}
	
	/**
	 * Encodes plain text using HTML entities when necessary.
	 *
	 * @param text   the text to encode
	 */
	private static String encode(final String text) {
		return text
				.replace("&", "&amp;")
				.replace("<", "&lt;")
				.replace(">", "&gt;")
				.replace("\"", "&quot;")
				.replace("'", "&apos;");
	}
	
	/**
	 * Returns the HTML source code of the text.
	 * applying the encode() method to it.
	 */
	@Override
	public final String getHtml() {
		return encode(super.getHtml());
	}
}
