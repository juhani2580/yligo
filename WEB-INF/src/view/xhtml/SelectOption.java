/**
 * A class for the HTML 'option' element.
 * The element is unindentable.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class SelectOption extends ContainerElement {
	/**
	 * Constructs the element.
	 *
	 * @param value   a value for the 'value' attribute.
	 */
	public SelectOption(final String value) {
		super("option", false);
		this.setAttribute("value", value);
	}
	
	/**
	 * As above, but...
	 *
	 * @param text   a text to automatically add to itself.
	 */
	public SelectOption(final String value, final String text) {
		this(value);
		this.add(text);
	}
	
	/**
	 * As above, but...
	 *
	 * @param selected   if true, the 'selected' attribute is set to 'selected'.
	 */
	public SelectOption(final String value, final String text, final boolean selected) {
		this(value, text);
		if(selected) {
			this.setAttribute("selected", "selected");
		}
	}
}
