/**
 * A class for the HTML 'a' element.
 * The element is unindetable.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class A extends ContainerElement {
	/**
	 * Constructs the element.
	 *
	 * @param href    a value for the href attribute
	 * @param label   contained label text
	 */
	public A(final String href, final String label) {
		super("a", false);
		this.setAttribute("href", href);
		this.add(label);
	}
}
