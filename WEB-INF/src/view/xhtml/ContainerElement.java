/**
 * A base class for all HTML elements, which may contain other HTML objects.
 * Provides a method to add another HTML object (HtmlObject.java) into itself
 * and implements the getHtml() methods, which return the element's start and
 * close tag with the HTML code of the contained elements between them,
 * indented appropriately if so specified.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

import java.util.ArrayList;

public abstract class ContainerElement extends HtmlElement {
	private final transient ArrayList<HtmlObject> content;	// Contained elements
	private final transient boolean indentable;		// Is indentable flag

	/**
	 * Constructs a ContainerElement.
	 *
	 * @param tag_name        name of the element's tag.
	 * @param is_indentable   'indentable' flag
	 *                        if false, indentation isn't applied to this object.
	 */
	public ContainerElement(final String tag_name, final boolean is_indentable) {
		super(tag_name);
		this.content = new ArrayList<HtmlObject>();
		this.indentable = is_indentable;
	}
	
	/**
	 * As above, but automatically sets 'indentable' to true.
	 */
	public ContainerElement(final String tag_name) {
		this(tag_name, true);
	}
	
	/**
	 * Returns the HTML source code of the contained objects.
	 */
	private String getContent() {
		String cnt = "";

		for (HtmlObject html : this.content)
			cnt += html.getHtml();

		return cnt;
	}
	
	/**
	 * As above, but with indendation.
	 *
	 * @param indent_level   the indentation level used at root level
	 * @param indent_width   ...                        for inner elements.
	 */
	private String getContent(final int indent_level, final int indent_width) {
		String cnt = "";

		for (HtmlObject html : this.content)
			cnt += html.getHtml(indent_level + indent_width, indent_width);

		return cnt;
	}

	/**
	 * Adds an HTML object to this container element.
	 *
	 * @param html   the element to be added.
	 *               may be null, in which case adds nothing.
	 * @return       this object, to allow multiple consecutive calls
	 *               in a single statement, simplifying the caller code.
	 *               subclasses with similar methods should do the same.
	 */
	public final ContainerElement add(final HtmlObject html) {
		if (html != null) {
			this.content.add(html);
		}

		return this;
	}
	
	/**
	 * As above, but accepts a String, which will be automatically
	 * converted to an HtmlText object. (See HtmlText.java).
	 * A null is not allowed here.
	 */
	public final ContainerElement add(final String text) {
		return this.add(new HtmlText(text));
	}
	
	/**
	 * Returns the HTML source code of the container and its inner elements.
	 * The inner elements appear in the code in the order they've been added.
	 */
	@Override
	public final String getHtml() {
		return this.getStartTag(false) + this.getContent() + this.getCloseTag();
	}
	
	/**
	 * As above but with indentation by blocks, that is, the start and close
	 * tags of inner elements will have an indentation one level greater than
	 * the container element. The indentation is applied recursively. If the
	 * element has been constructed with a 'no indentation' flag, indentation
	 * is omitted both from the element itself and its inner elements.
	 *
	 * @param indent_level   the indentation level used for the container itself
	 * @param indent_width   the relative width of each indentation level (in spaces)
	 */
	@Override
	public final String getHtml(final int indent_level, final int indent_width) {
		if (!this.indentable) {
			return this.getStartTag(indent_level, false) + this.getContent()
					+ this.getCloseTag() + "\n";
		}

		return this.getStartTag(indent_level, false) + "\n"
				+ this.getContent(indent_level, indent_width)
				+ this.getCloseTag(indent_level) + "\n";
	}
}
