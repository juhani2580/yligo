/**
 * A class for the HTML 'form' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Form extends ContainerElement {
	/**
	 * An enumeration for possible values of the 'method' attribute.
	 */
	public enum SendMethod { GET, POST }
	
	/**
	 * Constructs the element.
	 *
	 * @param action   a value for the 'action' attribute.
	 * @param method   a value for the 'method' attribute.
	 */
	public Form(final String action, final SendMethod method) {
		super("form");
		this.setAttribute("action", action);
		this.setAttribute("method", (method == SendMethod.GET) ? "get" : "post");
	}
}
