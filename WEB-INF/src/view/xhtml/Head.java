/**
 * The class for the HTML 'head' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Head extends ContainerElement {
	/**
	 * Constructs the element.
	 */
	public Head() {
		super("head");
	}
}
