/**
 * A class for the HTML 'body' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Body extends ContainerElement {
	public Body() {
		super("body");
	}
}
