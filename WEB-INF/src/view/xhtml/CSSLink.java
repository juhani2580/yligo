/**
 * A class for a 'link' element to include CSS style sheets.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class CSSLink extends Link {
	/**
	 * Constructs the element.
	 *
	 * @param href   a value for the 'href' element.
	 */
	public CSSLink(final String href) {
		this.setAttribute("href", href);
		this.setAttribute("rel", "stylesheet");
		this.setAttribute("type", "text/css");
	}
}
