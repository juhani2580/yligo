/**
 * A subclass for a type 'text' input element (See Input.java).
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class TextInput extends Input {
	/**
	 * Constructs the element.
	 *
	 * @param size   a value for the 'size' attribute.
	 * @param name   a value for the 'name' attribute.
	 */
	public TextInput(final int size, final String name) {
		super(InputType.TEXT, name);
		this.setAttribute("size", Integer.toString(size));
	}
	
	/**
	 * As above, but...
	 *
	 * @param text   a text to add automatically to itself.
	 */
	public TextInput(final int size, final String name, final String text) {
		this(size, name);
		this.setAttribute("value", text);
	}
}
