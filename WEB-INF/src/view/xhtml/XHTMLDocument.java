/**
 * A class to provide a simple way to create an XHTML document object.
 * All mandatory elements such as html, head, title, body and doctype
 * declaration are added automatically and can be accessed safely without
 * altering the basic structure of the document.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class XHTMLDocument {
	private static final String DOCTYPE =
		"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">";
	private final transient Body body;
	private transient String title_text;
	
	/**
	 * Constructs a new XHTMLDocument with an empty title and body.
	 */
	public XHTMLDocument() {
		this.body = new Body();
		this.title_text = "";
	}
	
	/**
	 * Returns the documents body element.
	 */
	public Body getBody() {
		return this.body;
	}
	
	/**
	 * Sets the document's title text.
	 */
	public void setTitleText(final String text) {
		this.title_text = text;
	}
	
	/**
	 * Generates and returns the actual element hierarchy.
	 */
	private HtmlObject getRoot() {
		return new Html()
				.add(new Head()
					.add(new Title(this.title_text))
					.add(new Meta()
						.setAttribute("http-equiv", "Content-Type")
						.setAttribute("content", "text/html; charset=utf-8"))
					.add(new CSSLink("style.css"))
					.add(new CSSLink("jquery.autocomplete.css")))
				.add(this.body);
	}
	
	/**
	 * Returns the HTML source code without indentation.
	 */
	public String getSource() {
		return DOCTYPE + "\n\n" + this.getRoot().getHtml();
	}
	
	/**
	 * As above but with indendation.
	 *
	 * @param indent_width   indendation width (number of spaces)
	 */
	public String getSource(final int indent_width) {
		return DOCTYPE + "\n\n" + this.getRoot().getHtml(indent_width);
	}
}
