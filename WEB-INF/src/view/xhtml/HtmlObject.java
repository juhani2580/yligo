/**
 * The abstract base class for all objects that may appear in an HTML document,
 * including text (RawText.java) and various HTML elements (HtmlElement.java).
 * This class requires an inheriting class to implement two methods to get the
 * object's HTML source code as a String, with or without automatic indentation.
 * It also provides tools for its subclasses to create indentation.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

import java.util.Arrays;

public abstract class HtmlObject {
	/**
	 * Returns the object's HTML source code.
	 **/
	public abstract String getHtml();
	
	/**
	 * Returns the object's HTML source code with indentation.
         * The indentation specified is applied recursively to inner elements.
	 *
	 * @param indent_level   the indentation level used at the root level
         * @param indent_width   ...                        for inner elements
         */
	public abstract String getHtml(int indent_level, int indent_width);
	
	/**
	 * As above but with no root level indentation.
	 *
	 * @param indent_width   as above
	 */
	public final String getHtml(final int indent_width) {
		return this.getHtml(0, indent_width);
	}
	
	/**
	 * Returns a String of spaces.
	 *
	 * @param width   the length of the string
	 */
	protected static String getIndent(final int width) {
		final char[] array = new char[width];
		Arrays.fill(array, ' ');
		return new String(array);
	}
	
	/**
	 * Returns the HTML source code (without indendation).
	 */
	@Override
	public final String toString() {
		return this.getHtml();
	}
}
