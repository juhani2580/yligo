/**
 * A class for the HTML 'meta' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Meta extends EmptyElement {
	/**
	 * Constructs the element.
	 */
	public Meta() {
		super("meta");
	}
}
