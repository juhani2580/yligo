/**
 * A subclass for a type 'password' input element (See Input.java).
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class PasswordInput extends Input {
	/**
	 * Constructs the element.
	 *
	 * @param size   a value for the 'size' attribute.
	 * @param name   a value for the 'name' attribute.
	 */
	public PasswordInput(final int size, final String name) {
		super(InputType.PASSWORD, name);
		this.setAttribute("size", Integer.toString(size));
	}
	
	/**
	 * As above, but with a 'text' parameter.
	 *
	 * @param text   a text for the 'value' attribute.
	 */
	public PasswordInput(final int size, final String name, final String text) {
		this(size, name);
		this.setAttribute("value", text);
	}
}
