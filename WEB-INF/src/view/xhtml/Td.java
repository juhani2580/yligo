/**
 * A class for the HTML 'td' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Td extends TableCell {
	/**
	 * Constructs the element.
	 */
	public Td() {
		super("td");
	}
	
	/**
	 * Constructs the element and automatically adds an object to it.
	 *
	 * @param html   the object to add.
	 */
	public Td(final HtmlObject html) {
		this();
		this.add(html);
	}
	
	/**
	 * As above, but accepts a String.
	 */
	public Td(final String text) {
		this();
		this.add(text);
	}
}
