/**
 * A subclass for a type 'submit' input element (See Input.java).
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class SubmitInput extends Input {
	/**
	 * Consturcts the element.
	 *
	 * @param caption   a caption for the element.
	 */
	public SubmitInput(final String caption) {
		super(InputType.SUBMIT, "", caption);
	}
}
