/**
 * A subclass for a type 'button' input element (See Input.java).
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class ButtonInput extends Input {
	/**
	 * Constructs the element.
	 *
	 * @param caption   caption text for the button.
	 */
	public ButtonInput(final String caption) {
		super(InputType.BUTTON, "", caption);
	}
	
	/**
	 * As above but with 'onclick' parameter.
	 *
	 * @param onclick   a value for the 'onclick' attribute.
	 */
	public ButtonInput(final String caption, final String onclick) {
		this(caption);
		this.setAttribute("onclick", onclick);
	}
}
