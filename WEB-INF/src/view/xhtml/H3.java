/**
 * A class for the HTML 'h3' element.
 * The element is unindentable.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class H3 extends ContainerElement {
	/**
	 * Constructs the element.
	 */
	public H3() {
		super("h3", false);
	}
	
	/**
	 * As above but with a text automatically added to it.
	 *
	 * @param text   the text to add.
	 */
	public H3(final String text) {
		this();
		this.add(text);
	}
}
