/**
 * A class for the HTML 'br' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Br extends EmptyElement {
	/**
	 * Costructs the element.
	 */
	public Br() {
		super("br");
	}
}
