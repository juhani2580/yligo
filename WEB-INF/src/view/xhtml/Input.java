/**
 * A class for the HTML 'input' element. Also subclassed by its type attribute.
 * (See TextInput.java, SubmitInput.java, ButtonInput.java, PasswordInput.java
 * and CheckboxInput.java respectively).
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Input extends EmptyElement {
	/**
	 * An enumeration for all valid values of the type attribute.
	 */
	public enum InputType { TEXT, SUBMIT, BUTTON, PASSWORD, CHECKBOX }
	
	/**
	 * Constructs an input element of the specfied type.
	 */
	private static String getTypeText(final InputType type) {
		if(type == InputType.TEXT) {
			return "text";
		} else if(type == InputType.SUBMIT) {
			return "submit";
		} else if(type == InputType.BUTTON) {
			return "button";
		} else if(type == InputType.PASSWORD) {
			return "password";
		}
		return "checkbox";
	}
	
	/**
	 * Constructs the element.
	 *
	 * @param type   the type of the element.
	 * @param name   a value for the 'name' attribute.
	 */
	public Input(final InputType type, final String name) {
		super("input");
		this.setAttribute("type", getTypeText(type));
		this.setName(name);
	}
	
	/**
	 * As above, but with 'value' parameter.
	 *
	 * @param value   a value for the 'value' parameter.
	 */
	public Input(final InputType type, final String name, final String value) {
		this(type, name);
		this.setAttribute("value", value);
	}
}
