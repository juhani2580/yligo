/**
 * A class for the HTML 'span' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Span extends ContainerElement {
	/**
	 * Constructs the element.
	 */
	public Span() {
		super("span");
	}
	
	/**
	 * Constructs the element and immediately adds an object to it.
	 *
	 * @param html   the object to add.
	 */
	public Span(final HtmlObject html) {
		this();
		this.add(html);
	}
	
	/**
	 * As above but accepts a String, which is automatically
	 * converted to a HtmlText object.
	 */
	public Span(final String text) {
		this();
		this.add(text);
	}
}
