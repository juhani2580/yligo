/**
 * A class for the HTML 'div' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Div extends ContainerElement {
	/**
	 * Constructs the element.
	 */
	public Div() {
		super("div");
	}
}
