/**
 * A class for the HTML 'script' element.
 * It refers to javascript by default.
 * The element is unindentable.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Script extends ContainerElement {
	/**
	 * Private constructor.
	 */
	private Script() {
		super("script", false);
		this.setAttribute("type", "text/javascript");
	}
	
	/**
	 * Constructs the element as a reference to an external script.
	 *
	 * @param source   the source of the external script.
	 */
	public Script(final String source) {
		this();
		this.setAttribute("src", source);
	}
	
	/**
	 * Constructs the element as a container of the script.
	 *
	 * @param code   the script itself.
	 */
	public Script(final RawText code) {
		this();
		this.add(new RawText("/* <![CDATA[ */\n"));
		this.add(code);
		this.add(new RawText("/* ]]> */\n"));
	}
}
