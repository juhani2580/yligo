/**
 * A class for the HTML 'html' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Html extends ContainerElement {
	/**
	 * Constructs the element. Automatically adds the
	 * 'xmlns' attribute required by XHTML.
	 */
	public Html() {
		super("html");
		this.setAttribute("xmlns", "http://www.w3.org/1999/xhtml");
	}
}
