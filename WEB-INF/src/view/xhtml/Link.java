/**
 * A class for the HTML 'link' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Link extends EmptyElement {
	/**
	 * Constructs the 'eleent.
	 */
	public Link() {
		super("link");
	}
}
