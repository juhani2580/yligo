/**
 * A class for the HTML 'tr' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Tr extends ContainerElement {
	/**
	 * Constructs the element.
	 */
	public Tr() {
		super("tr");
	}
	
	/**
	 * Adds and returns a 'td' element.
	 */
	public Td addTd() {
		final Td new_td = new Td();
		this.add(new_td);
		return new_td;
	}
	
	/**
	 * As above, but automatically adds an object to the 'td' element.
	 *
	 * @param html   the element to add.
	 */
	public Tr addTd(final HtmlObject html) {
		final Td new_td = this.addTd();
		new_td.add(html);
		return this;
	}
	
	/**
	 * As above, but accepts a String.
	 */
	public Tr addTd(final String text) {
		final Td new_td = this.addTd();
		new_td.add(text);
		return this;
	}
	
	/**
	 * As in addTd(), but for 'th'.
	 */
	public Th addTh() {
		final Th new_th = new Th();
		this.add(new_th);
		return new_th;
	}
	
	/**
	 * As in addTd(), but for 'th'.
	 */
	public Tr addTh(final HtmlObject html) {
		final Th new_th = this.addTh();
		new_th.add(html);
		return this;
	}
	
	/**
	 * As in addTd(), but for 'th'.
	 */
	public Tr addTh(final String text) {
		final Th new_th = this.addTh();
		new_th.add(text);
		return this;
	}
}
