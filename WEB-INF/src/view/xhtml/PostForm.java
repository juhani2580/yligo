/**
 * A class for a form element, which uses POST as a send method.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class PostForm extends Form {
	/**
	 * Constructs the element.
	 *
	 * @param action   a value for the 'action' attribute.
	 */
	public PostForm(final String action) {
		super(action, SendMethod.POST);
	}
}
