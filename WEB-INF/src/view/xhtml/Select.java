/**
 * A class for the HTML 'select' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Select extends ContainerElement {
	/**
	 * Constructs the element.
	 *
	 * @param name   a value for the 'name' element.
	 */
	public Select(final String name) {
		super("select");
		this.setName(name);
	}
}
