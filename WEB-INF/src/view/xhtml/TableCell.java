/**
 * An abstract base class for HTML table cell elements (Td.java, Th.java).
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public abstract class TableCell extends ContainerElement {
	/**
	 * Constructs the element.
	 *
	 * @param tag_name   the name of the element's tag.
	 */
	public TableCell(final String tag_name) {
		super(tag_name, false);
	}
	
	/**
	 * Sets a value for the rowspan attribute.
	 */
	public TableCell setRowSpan(final int span) {
		this.setAttribute("rowspan", Integer.toString(span));
		return this;
	}
	
	/**
	 * Sets a value for the colspan attribute.
	 */
	public TableCell setColSpan(final int span) {
		this.setAttribute("colspan", Integer.toString(span));
		return this;
	}
}
