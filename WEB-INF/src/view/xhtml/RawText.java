/**
 * A class for raw text, which can be added to an HTML document as it is,
 * without any automatic HTML encoding. The syntactical validity of the
 * resulting code is on the user's responsibility.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class RawText extends HtmlObject {
	private final transient String text;
	
	/**
	 * Constructs the object from a String.
	 */
	public RawText(final String raw_text) {
		this.text = raw_text;
	}
	
	/**
	 * Returns the text without encoding.
	 */
	@Override
	public String getHtml() {
		return this.text;
	}
	
	/**
	 * As above but with indentation. (See HtmlObject.java for parameters).
	 */
	@Override
	public String getHtml(final int indent_level, final int ident_width) {
		return HtmlObject.getIndent(indent_level)
				+ this.getHtml()
				+ "\n";
	}
}
