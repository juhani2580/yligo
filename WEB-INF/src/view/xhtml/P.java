/**
 * A class for the HTML 'p' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class P extends ContainerElement {
	/**
	 * Constructs the element.
	 */
	public P() {
		super("p");
	}
	
	/**
	 * Constructs the element and immediately adds an object to it.
	 *
	 * @param html   the object to add.
	 */
	public P(final HtmlObject html) {
		this();
		this.add(html);
	}
	
	/**
	 * As above but accepts a String, which is automatically
	 * converted to a HtmlText object.
	 */
	public P(final String text) {
		this();
		this.add(text);
	}
}
