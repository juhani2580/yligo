/**
 * A base class for all HTML elements (ContainerElement.java, EmptyElement.java).
 * Provides tools for setting the element's attributes and generating start and
 * close tags with or without indentation. All attributes can be set freely but
 * subclasses typically provide shorter methods and/or constructors to set their
 * most commonly used attributes.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

import java.util.ArrayList;

public abstract class HtmlElement extends HtmlObject {
	/**
	 * A private class for HTML element attributes.
	 */
	private class ElementAttribute {
		private final transient String name, value;
		
		public ElementAttribute(final String attribute_name, final String element_value) {
			this.name = attribute_name;
			this.value = element_value;
		}
		
		public String getHtml() {
			return this.name + "=\"" + new HtmlText(this.value) + "\"";
		}
	}
	
	private final transient String tag_name;
	private final transient ArrayList<ElementAttribute> attributes;
	
	/**
	 * Returns the HTML source code the element's attribute list.
	 */
	private String getAttributeList() {
		String list = "";
		
		for(ElementAttribute attr : this.attributes)
			list += ' ' + attr.getHtml();
		
		return list;
	}
	
	/**
	 * Sets the value of an attribute of the element.
	 *
	 * @param attribute_name    the name of the attribute to set.
	 * @param attribute_value   the value to set for the attribute.
	 * @return                  itself, subclasses with similar methods should
	 *                          do the same to simplify the usage.
	 */
	public final HtmlElement setAttribute(final String attribute_name, final String attribute_value) {
		this.attributes.add(new ElementAttribute(attribute_name, attribute_value));
		return this;
	}
	
	/**
	 * Constructs and returns the start tag of the element.
	 *
	 * @param close   if true, the tag is also closed,
	 *                this is required for empty elements.
	 */
	protected final String getStartTag(final boolean close) {
		return '<'
				+ this.tag_name
				+ this.getAttributeList()
				+ (close ? " /" : "")
				+ '>';
	}
	
	/**
	 * As above but with indendation. (See HtmlObject.java for parameters).
	 */
	protected final String getStartTag(final int indent_level, final boolean close) {
		return HtmlObject.getIndent(indent_level) + getStartTag(close);
	}
	
	/**
	 * Constructs and returns the close tag of the element.
	 */
	protected final String getCloseTag() {
		return "</"
				+ this.tag_name
				+ '>';
	}
	
	/**
	 * As above but with indentation. (See HtmlObject.java for parameters).
	 */
	protected final String getCloseTag(final int indent_level) {
		return HtmlObject.getIndent(indent_level) + getCloseTag();
	}
	
	/**
	 * Constructs the element.
	 *
	 * @param element_tag_name   the name of the element's tag.
	 */
	public HtmlElement(final String element_tag_name) {
		this.tag_name = element_tag_name;
		this.attributes = new ArrayList<ElementAttribute>();
	}
	
	/**
	 * A short method for setting the 'name' attribute.
	 *
	 * @param name   a value for 'name'.
	 */
	public final HtmlElement setName(final String name) {
		this.setAttribute("name", name);
		return this;
	}
	
	/**
	 * A short method for setting the 'id' attribute.
	 *
	 * @param id   a value for 'id'.
	 */
	public final HtmlElement setId(final String id) {
		this.setAttribute("id", id);
		return this;
	}
	
	/**
	 * A short method for setting the 'class' attribute.
	 *
	 * @param class_name   a value for 'class'.
	 */
	public final HtmlElement setClass(final String class_name) {
		this.setAttribute("class", class_name);
		return this;
	}
}
