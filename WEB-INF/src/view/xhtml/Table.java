/**
 * A class for the HTML 'table' element.
 *
 * @author Kustaa Kangas
 */

package view.xhtml;

public class Table extends ContainerElement {
	/**
	 * Constructs the element.
	 */
	public Table() {
		super("table");
	}
	
	/**
	 * As above, but...
	 *
	 * @param border   a value for the 'border' attribute.
	 */
	public Table(final int border) {
		this();
		this.setAttribute("border", Integer.toString(border));
	}
	
	/**
	 * Adds and returns a row element.
	 */
	public final Tr addTr() {
		final Tr tr = new Tr();
		this.add(tr);
		return tr;
	}
}
