var ndid = 0;

function new_discount() {
	if(ndid == 20) {
		alert('Cannot add more than 20 discounts at one time.');
		return;
	}

	var table = document.getElementById('discount_table');
	var tr = document.createElement('tr');
	table.appendChild(tr);

	var name_td = document.createElement('td');
	tr.appendChild(name_td);
	var name_input = document.createElement('input');
	name_input.setAttribute('name', 'new_discount_name' + ndid);
	name_input.setAttribute('type', 'text');
	name_input.setAttribute('size', '20');
	name_td.appendChild(name_input);

	var amount_td = document.createElement('td');
	tr.appendChild(amount_td);
	var amount_input = document.createElement('input');
	amount_input.setAttribute('name', 'new_discount_amount' + ndid);
	amount_input.setAttribute('type', 'text');
	amount_input.setAttribute('size', '5');
	amount_td.appendChild(amount_input);

	var dominant_td = document.createElement('td');
	tr.appendChild(dominant_td);
	var dominant_input = document.createElement('input');
	dominant_input.setAttribute('name', 'new_discount_dominant' + ndid);
	dominant_input.setAttribute('type', 'checkbox');
	dominant_input.setAttribute('value', 'checked');
	dominant_td.appendChild(dominant_input);

	var remove_td = document.createElement('td');
	tr.appendChild(remove_td);

	ndid++;
}
