function filter_table(table_name) {
	var lns = document.getElementById('last_name').value.toLowerCase();
	var fns = document.getElementById('first_name').value.toLowerCase();
	var table = document.getElementById(table_name);
	var i;
	for(i = 1; i < table.rows.length; i++) {
		var row = table.rows[i];
		var ln = row.cells[0].innerHTML.toLowerCase();
		var fn = row.cells[1].innerHTML.toLowerCase();
		if(ln.indexOf(lns) == 0 && fn.indexOf(fns) == 0) {
			row.style.display = '';
		} else {
			row.style.display = 'none';
		}
	}
}

function filter_tables() {
	filter_table("pre_entries");
	filter_table("entries");
}
