// presuggestions for pre-registration

$(document)
		.ready(
				function() {

					$("#last_name").autocomplete(
							users,
							{
								selectFirst : false,
								width : 400,
								highlight: false,
								formatItem : function(row, i, max) {
									return row.first_name + " " + row.last_name
											+ " [" + row.rank + ", " + row.club
											+ ", " + row.country + "]";
								},
								formatMatch : function(row, i, max) {
									return row.last_name;
								},
								formatResult : function(row) {
									return row.last_name;
								}
							});

					$("#first_name").autocomplete(
							users,
							{
								selectFirst : false,
								width : 400,
								highlight: false,
								formatItem : function(row, i, max) {
									return row.first_name + " " + row.last_name
											+ " [" + row.rank + ", " + row.club
											+ ", " + row.country + "]";
								},
								formatMatch : function(row, i, max) {
									return row.first_name;
								},
								formatResult : function(row) {
									return row.first_name;
								}
							});

					$('#last_name, #first_name')
							.result(
									function(event, data, formatted) {
										if (data) {
											window.location
													.replace("GoTournaments?req=p_registration&tid="
															+ tournament_id
															+ "&pre=false&eid="
															+ data.id
															+ "&edit=false");
										}
									});

				});
